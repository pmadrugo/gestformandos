﻿namespace WinForms
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using MetroFramework.Forms;
    using Database;
    using System.Threading;

    public partial class FormLoading : MetroForm
    {
        public FormLoading()
        {
            InitializeComponent();
        }

        private void FormLoading_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        /// <summary>
        /// Loads data from text files into the database in memory.
        /// </summary>
        public void LoadData()
        {
            new Thread(new ThreadStart(() =>
            {
                lblLoading.Invoke(new Action(
                    ()=>
                    {
                        lblLoading.Text = $"A carregar avaliações...";
                    })
                );
                Database.Evaluations.LoadFromTextFile();
                Thread.Sleep(1000);

                lblLoading.Invoke(new Action(
                    () =>
                    {
                        lblLoading.Text = $"A carregar estudantes...";
                    })
                );
                Database.Students.LoadFromTextFile();
                Thread.Sleep(1000);

                lblLoading.Invoke(new Action(
                    () =>
                    {
                        lblLoading.Text = $"A carregar turmas...";
                    })
                );
                Database.Classes.LoadFromTextFile();
                Thread.Sleep(1000);

                lblLoading.Invoke(new Action(
                    () =>
                    {
                        lblLoading.Text = $"A carregar UFCDs...";
                    })
                );
                Database.UFCDs.LoadFromTextFile();
                Thread.Sleep(1000);

                lblLoading.Invoke(new Action(
                    () =>
                    {
                        lblLoading.Text = $"A carregar cursos...";
                    })
                );
                Database.Courses.LoadFromTextFile();
                Thread.Sleep(1000);

                lblLoading.Invoke(new Action(
                    () =>
                    {
                        lblLoading.Text = $"A carregar professores...";
                    })
                );
                Database.Professors.LoadFromTextFile();
                Database.Professor_Class.LoadFromTextFile();
                Thread.Sleep(1000);

                lblLoading.Invoke(new Action(
                    () =>
                    {
                        lblLoading.Text = $"A inicializar a aplicação...";
                    })
                );

                this.Invoke(new Action(
                    () =>
                    {
                        this.Close();
                    })
                );
            })).Start();
        }
    }
}
