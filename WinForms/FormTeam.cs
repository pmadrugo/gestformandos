﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Database.TableModels;
using MetroFramework.Controls;
using MetroFramework.Forms;
using WinForms.AddEdit_Forms;

namespace WinForms
{
    using Database;

    public partial class FormTeam : MetroForm
    {
        public Course Course { get; set; }
        public Class Team { get; set; }

        public FormTeam(Course course, Class team)
        {
            this.Course = course;
            this.Team = team;

            InitializeComponent();

            this.Text = $"{Team.Name}";
        }

        private void FormTeam_Load(object sender, EventArgs e)
        {
            this.Activate();

            InitStudentList();
            InitEvaluationList();
            InitProfessor_ClassList();

            metroTabControl.SelectedTab = metroTabPageStudents;
        }

        //----------------------- Students -------------------

        #region Students Page

        /// <summary>
        /// Populates the Student datagridview table with values
        /// </summary>
        private void InitStudentList()
        {
            dataGridViewStudents.Controls.Clear();
            dataGridViewStudents.Rows.Clear();
            dataGridViewStudents.Columns.Clear();

            dataGridViewStudents.AutoGenerateColumns = true;

            dataGridViewStudents.Columns.Add($"ID", $"ID");
            dataGridViewStudents.Columns.Add($"Name", $"Nome");

            if (this.Team.Students.StudentList.Count > 0)
            {
                foreach (Student student in this.Team.Students.StudentList.OrderBy(student => student.Name))
                {
                    dataGridViewStudents.Rows.Add();

                    DataGridViewRow tableRow = dataGridViewStudents.Rows[dataGridViewStudents.Rows.Count - 1];

                    tableRow.Tag = student;

                    tableRow.Cells["ID"].Value = student.ID.ToString();
                    tableRow.Cells["Name"].Value = student.Name;
                }
            }
            

            dataGridViewStudents.AutoGenerateColumns = false;

            SetupStudentTable();
        }

        /// <summary>
        /// Sets up the Students table appearance
        /// </summary>
        private void SetupStudentTable()
        {
            dataGridViewStudents.Columns["ID"].DisplayIndex = 0;
            dataGridViewStudents.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewStudents.Columns["ID"].HeaderText = $"ID";
            dataGridViewStudents.Columns["ID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dataGridViewStudents.Columns["Name"].DisplayIndex = 1;
            dataGridViewStudents.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewStudents.Columns["Name"].HeaderText = $"Nome";
            dataGridViewStudents.Columns["Name"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        /// <summary>
        /// OnClick event for opening the context menu strip to choose how a student will be added
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroTileAdd_Click(object sender, EventArgs e)
        {
            MetroTile btnSender = (MetroTile)sender;

            Point ptLowerLeft = new Point(0, btnSender.Height);
            ptLowerLeft = btnSender.PointToScreen(ptLowerLeft);

            contextMenuStripAddStudent.Show(btnSender, new Point(btnSender.Width - contextMenuStripAddStudent.Width, btnSender.Height));
        }

        /// <summary>
        /// OnClick event for editing an existing student
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroTileEdit_Click(object sender, EventArgs e)
        {
            Student currentStudent = dataGridViewStudents.CurrentRow.Tag as Student;

            if (currentStudent != null)
            {
                FormManageStudent frmManageStudent = new FormManageStudent(this.Team, currentStudent);

                frmManageStudent.ShowDialog();

                InitStudentList();
                InitEvaluationList();
            }
        }

        /// <summary>
        /// OnClick event for deleting an existing student
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroTileDelete_Click(object sender, EventArgs e)
        {
            Student currentStudent = dataGridViewStudents.CurrentRow.Tag as Student;

            if (currentStudent != null)
            {
                DialogResult confirmation = MessageBox.Show(
                           $"Tem a certeza que pretende eliminar {currentStudent.BaseInfo}?",
                           $"Eliminar Turma",
                           MessageBoxButtons.YesNo,
                           MessageBoxIcon.Warning
                       );

                if (confirmation == DialogResult.Yes)
                {
                    switch (this.Team.Students.Delete(currentStudent))
                    {
                        case 0:
                            {
                                InitStudentList();
                                InitEvaluationList();
                            }
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Adding a completely new student
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormManageStudent frmManageStudent = new FormManageStudent(this.Team);

            frmManageStudent.ShowDialog();
            
            InitStudentList();
            InitEvaluationList();

            this.Activate();
        }

        /// <summary>
        /// Adding a student from an existing entry
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addExistingStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormListManager<Student> formListManager = new FormListManager<Student>(this.Team.Students.StudentList, Database.Students.StudentList, $"Estudantes");

            formListManager.ShowDialog();

            Database.Classes.SaveToTextFile();

            InitStudentList();
            InitEvaluationList();

            this.Activate();
        }

        #endregion

        //----------------------- End Students -------------------

        //----------------------- Evaluations -------------------

        #region Evaluations Page

        /// <summary>
        /// Populates the evaluation table with data
        /// </summary>
        private void InitEvaluationList()
        {
            dataGridViewEvaluations.Controls.Clear();
            dataGridViewEvaluations.Rows.Clear();
            dataGridViewEvaluations.Columns.Clear();

            dataGridViewEvaluations.Columns.Add($"ID", $"ID");
            dataGridViewEvaluations.Columns.Add($"Nome", "Nome");

            if (this.Course.UFCDs.UFCDList.Count > 0)
            {
                foreach (UFCD ufcd in this.Course.UFCDs.UFCDList.OrderBy(ufcd => ufcd.ID))
                {
                    DataGridViewColumn tableColumn = new DataGridViewColumn();
                    tableColumn.HeaderText = $"{ufcd.ID}";
                    tableColumn.ToolTipText = $"{ufcd.Name}";
                    tableColumn.Tag = ufcd;
                    tableColumn.CellTemplate = new DataGridViewTextBoxCell();

                    dataGridViewEvaluations.Columns.Add(tableColumn);
                }
            }
            
            if (this.Team.Students.StudentList.Count > 0)
            {
                foreach (Student student in this.Team.Students.StudentList.OrderBy(student => student.Name))
                {
                    dataGridViewEvaluations.Rows.Add();

                    DataGridViewRow tableRow = dataGridViewEvaluations.Rows[dataGridViewEvaluations.Rows.Count-1];
                    tableRow.Tag = student;

                    int colPos = 0;

                    DataGridViewTextBoxCell IDcell = new DataGridViewTextBoxCell()
                    {
                        Value = $"{student.ID}",
                        Tag = student
                    };
                    tableRow.Cells[colPos++] = IDcell;

                    DataGridViewTextBoxCell NameCell = new DataGridViewTextBoxCell()
                    {
                        Value = $"{student.Name}",
                        Tag = student
                    };
                    tableRow.Cells[colPos++] = NameCell;

                    if (this.Course.UFCDs.UFCDList.Count > 0)
                    {
                        foreach (UFCD ufcd in this.Course.UFCDs.UFCDList.OrderBy(ufcd => ufcd.ID))
                        {
                            bool hasValue = false;
                            foreach (Evaluation evaluation in student.Evaluations.EvaluationList)
                            {
                                if (ufcd.ID == evaluation.IDUFCD)
                                {
                                    hasValue = true;
                                    tableRow.Cells[colPos++] = (
                                        new DataGridViewTextBoxCell()
                                        {
                                            Value = $"{evaluation.Grade}",
                                            Tag = evaluation,
                                            ToolTipText = $"{evaluation.Description}"
                                        }
                                    );
                                    break;
                                }
                            }
                            if (!hasValue)
                            {
                                tableRow.Cells[colPos++] = (
                                    new DataGridViewTextBoxCell()
                                    {
                                        Value = $"N/A"
                                    }
                                );
                            }
                        }
                    }
                }
            }
            
            SetupEvaluationTable();
        }

        /// <summary>
        /// Sets the appearance for the Evaluations table
        /// </summary>
        private void SetupEvaluationTable()
        {
            dataGridViewEvaluations.Columns["ID"].DisplayIndex = 0;
            dataGridViewEvaluations.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewEvaluations.Columns["ID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dataGridViewEvaluations.Columns["Nome"].DisplayIndex = 1;
            dataGridViewEvaluations.Columns["Nome"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewEvaluations.Columns["Nome"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            for (int i = 2; i < dataGridViewEvaluations.Columns.Count; i++)
            {
                dataGridViewEvaluations.Columns[i].DisplayIndex = i;
                dataGridViewEvaluations.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                dataGridViewEvaluations.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
        }

        /// <summary>
        /// OnClicking the cell, if there is a matching value, fires up the edit form. If not, fires up the new entry form for the corresponding evaluation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewEvaluations_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }

            DataGridView dataTable = (DataGridView)sender;

            DataGridViewCell selectedCell = dataTable.Rows[e.RowIndex].Cells[e.ColumnIndex];

            if (dataTable.Columns[e.ColumnIndex].Tag is UFCD)
            {
                if (selectedCell.Tag is Evaluation)
                {
                    FormManageEvaluation frmManageEvaluation = new FormManageEvaluation((Evaluation)selectedCell.Tag);

                    frmManageEvaluation.ShowDialog();

                    InitEvaluationList();
                }
                else
                {
                    Student currentStudent = dataTable.Rows[selectedCell.RowIndex].Tag as Student;

                    if (currentStudent != null)
                    {
                        UFCD currentUFCD = dataTable.Columns[selectedCell.ColumnIndex].Tag as UFCD;

                        if (currentUFCD != null)
                        {
                            FormManageEvaluation frmManageEvaluation = new FormManageEvaluation(currentStudent, currentUFCD);

                            frmManageEvaluation.ShowDialog();

                            InitEvaluationList();
                        }
                    }
                }
            }
        }

        #endregion

        //----------------------- End Evaluations -------------------

        //----------------------- Professors ------------------------

        #region Professors Page

        private void InitProfessor_ClassList()
        {
            dataGridViewProfessorClass.Controls.Clear();
            dataGridViewProfessorClass.Rows.Clear();
            dataGridViewProfessorClass.Columns.Clear();

            dataGridViewProfessorClass.Columns.Add($"UFCD", $"UFCD");
            dataGridViewProfessorClass.Columns.Add($"Professor", "Formador");
            
            if (this.Course.UFCDs.UFCDList.Count > 0)
            {
                foreach (UFCD ufcd in this.Course.UFCDs.UFCDList.OrderBy(ufcd => ufcd.ID))
                {
                    dataGridViewProfessorClass.Rows.Add();

                    Professor_Class pclass = Database.Professor_Class.GetAssignedProfessorClass(this.Team, ufcd);

                    DataGridViewRow tableRow = dataGridViewProfessorClass.Rows[dataGridViewProfessorClass.Rows.Count - 1];

                    if (pclass != null)
                    {
                        tableRow.Tag = pclass;
                    }
                    else
                    {
                        tableRow.Tag = ufcd;
                    }

                    int colPos = 0;

                    DataGridViewTextBoxCell UFCDcell = new DataGridViewTextBoxCell()
                    {
                        Value = $"{ufcd.ID}",
                        ToolTipText = $"{ufcd.Name}",
                        Tag = pclass
                    };
                    tableRow.Cells[colPos++] = UFCDcell;

                    DataGridViewTextBoxCell ProfessorCell;

                    if (pclass != null)
                    {
                        ProfessorCell = new DataGridViewTextBoxCell()
                        {
                            Value = $"{pclass.Professor.Name}",
                            ToolTipText = $"{pclass.Professor.ID}",
                            Tag = pclass
                        };
                    }
                    else
                    {
                        ProfessorCell = new DataGridViewTextBoxCell()
                        {
                            Value = $"N/A",
                            ToolTipText = $"Atribua um formador a esta UFCD"
                        };
                    }
                    tableRow.Cells[colPos++] = ProfessorCell;
                }
            }

            SetupProfessor_ClassTable();
        }

        private void SetupProfessor_ClassTable()
        {
            dataGridViewProfessorClass.Columns["UFCD"].DisplayIndex = 0;
            dataGridViewProfessorClass.Columns["UFCD"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewProfessorClass.Columns["UFCD"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dataGridViewProfessorClass.Columns["Professor"].DisplayIndex = 1;
            dataGridViewProfessorClass.Columns["Professor"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewProfessorClass.Columns["Professor"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        /// <summary>
        /// OnClicking the cell, if there is a matching value, fires up the edit form. If not, fires up the new entry form for the corresponding evaluation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewProfessorClass_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }

            DataGridView dataTable = (DataGridView)sender;

            DataGridViewCell selectedCell = dataTable.Rows[e.RowIndex].Cells[e.ColumnIndex];
            
            if (selectedCell.Tag is Professor_Class)
            {
                
                FormManageProfessorClass frmManageProfessorClass = new FormManageProfessorClass((Professor_Class)selectedCell.Tag);

                frmManageProfessorClass.ShowDialog();

                InitProfessor_ClassList();
            }
            else
            {
                
                UFCD currentUFCD = dataTable.Rows[selectedCell.RowIndex].Tag as UFCD;

                if (currentUFCD != null)
                {
                    {
                        FormManageProfessorClass frmManageProfessorClass = new FormManageProfessorClass(this.Team, currentUFCD);

                        frmManageProfessorClass.ShowDialog();

                        InitProfessor_ClassList();
                    }
                }
            }
        }

        private void dataGridViewProfessorClass_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                {
                    if (e.RowIndex < 0)
                    {
                        return;
                    }

                    DataGridView dataTable = (DataGridView)sender;

                    DataGridViewCell selectedCell = dataTable.Rows[e.RowIndex].Cells[e.ColumnIndex];

                    if (selectedCell.Tag is Professor_Class)
                    {
                        Professor_Class pclass = selectedCell.Tag as Professor_Class;

                        if (pclass != null)
                        {
                            DialogResult confirmation = MessageBox.Show(
                               $"Tem a certeza que pretende desassociar {pclass.Professor.BaseInfo}?",
                               $"Desassociar Professor",
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Warning
                            );

                            if (confirmation == DialogResult.Yes)
                            {
                                switch (Database.Professor_Class.Delete(pclass))
                                {
                                    case 0:
                                    {
                                        InitProfessor_ClassList();
                                    }
                                    break;
                                }
                            }
                        }
                    }
                } break;
            }
            
        }
        #endregion

        //----------------------- End Professors --------------------
    }
}
