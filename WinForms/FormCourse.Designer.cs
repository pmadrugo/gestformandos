﻿namespace WinForms
{
    partial class FormCourse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.metroTabControl = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPageTeams = new MetroFramework.Controls.MetroTabPage();
            this.tableLayoutTeams = new System.Windows.Forms.TableLayoutPanel();
            this.metroTabPageUFCDs = new MetroFramework.Controls.MetroTabPage();
            this.dataGridViewUFCDs = new System.Windows.Forms.DataGridView();
            this.metroTileAddTeam = new MetroFramework.Controls.MetroTile();
            this.contextMenuStripUFCD = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editarUFCDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.removerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStripTeam = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemEditTeam = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemRemoveTeam = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.metroTabControl.SuspendLayout();
            this.metroTabPageTeams.SuspendLayout();
            this.metroTabPageUFCDs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUFCDs)).BeginInit();
            this.contextMenuStripUFCD.SuspendLayout();
            this.contextMenuStripTeam.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTabControl
            // 
            this.metroTabControl.Controls.Add(this.metroTabPageTeams);
            this.metroTabControl.Controls.Add(this.metroTabPageUFCDs);
            this.metroTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabControl.Location = new System.Drawing.Point(20, 60);
            this.metroTabControl.Name = "metroTabControl";
            this.metroTabControl.SelectedIndex = 1;
            this.metroTabControl.Size = new System.Drawing.Size(760, 370);
            this.metroTabControl.TabIndex = 4;
            this.metroTabControl.SelectedIndexChanged += new System.EventHandler(this.metroTabControl_SelectedIndexChanged);
            // 
            // metroTabPageTeams
            // 
            this.metroTabPageTeams.Controls.Add(this.tableLayoutTeams);
            this.metroTabPageTeams.HorizontalScrollbarBarColor = true;
            this.metroTabPageTeams.Location = new System.Drawing.Point(4, 35);
            this.metroTabPageTeams.Name = "metroTabPageTeams";
            this.metroTabPageTeams.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.metroTabPageTeams.Size = new System.Drawing.Size(752, 331);
            this.metroTabPageTeams.TabIndex = 0;
            this.metroTabPageTeams.Text = "Turmas";
            this.metroTabPageTeams.VerticalScrollbarBarColor = true;
            // 
            // tableLayoutTeams
            // 
            this.tableLayoutTeams.AutoScroll = true;
            this.tableLayoutTeams.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutTeams.ColumnCount = 1;
            this.tableLayoutTeams.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutTeams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutTeams.Location = new System.Drawing.Point(0, 5);
            this.tableLayoutTeams.Name = "tableLayoutTeams";
            this.tableLayoutTeams.RowCount = 1;
            this.tableLayoutTeams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 370F));
            this.tableLayoutTeams.Size = new System.Drawing.Size(752, 326);
            this.tableLayoutTeams.TabIndex = 5;
            // 
            // metroTabPageUFCDs
            // 
            this.metroTabPageUFCDs.Controls.Add(this.dataGridViewUFCDs);
            this.metroTabPageUFCDs.HorizontalScrollbarBarColor = true;
            this.metroTabPageUFCDs.Location = new System.Drawing.Point(4, 35);
            this.metroTabPageUFCDs.Name = "metroTabPageUFCDs";
            this.metroTabPageUFCDs.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.metroTabPageUFCDs.Size = new System.Drawing.Size(752, 331);
            this.metroTabPageUFCDs.TabIndex = 1;
            this.metroTabPageUFCDs.Text = "UFCDs";
            this.metroTabPageUFCDs.VerticalScrollbarBarColor = true;
            // 
            // dataGridViewUFCDs
            // 
            this.dataGridViewUFCDs.AllowUserToAddRows = false;
            this.dataGridViewUFCDs.AllowUserToDeleteRows = false;
            this.dataGridViewUFCDs.AllowUserToResizeColumns = false;
            this.dataGridViewUFCDs.AllowUserToResizeRows = false;
            this.dataGridViewUFCDs.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewUFCDs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUFCDs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewUFCDs.Location = new System.Drawing.Point(0, 5);
            this.dataGridViewUFCDs.MultiSelect = false;
            this.dataGridViewUFCDs.Name = "dataGridViewUFCDs";
            this.dataGridViewUFCDs.ReadOnly = true;
            this.dataGridViewUFCDs.RowHeadersVisible = false;
            this.dataGridViewUFCDs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewUFCDs.Size = new System.Drawing.Size(752, 326);
            this.dataGridViewUFCDs.TabIndex = 2;
            this.toolTip.SetToolTip(this.dataGridViewUFCDs, "Clique esquerdo para adicionar\r\nClique direito para remover");
            this.dataGridViewUFCDs.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewUFCDs_CellMouseDown);
            this.dataGridViewUFCDs.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridViewUFCDs_MouseDown);
            // 
            // metroTileAddTeam
            // 
            this.metroTileAddTeam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroTileAddTeam.Location = new System.Drawing.Point(704, 60);
            this.metroTileAddTeam.Name = "metroTileAddTeam";
            this.metroTileAddTeam.Size = new System.Drawing.Size(70, 29);
            this.metroTileAddTeam.TabIndex = 5;
            this.metroTileAddTeam.Text = "+";
            this.metroTileAddTeam.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTileAddTeam.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTileAddTeam.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTileAddTeam.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.metroTileAddTeam.Click += new System.EventHandler(this.metroTileAddTeam_Click);
            // 
            // contextMenuStripUFCD
            // 
            this.contextMenuStripUFCD.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editarUFCDToolStripMenuItem,
            this.toolStripSeparator1,
            this.removerToolStripMenuItem});
            this.contextMenuStripUFCD.Name = "contextMenuStripUFCD";
            this.contextMenuStripUFCD.Size = new System.Drawing.Size(122, 54);
            // 
            // editarUFCDToolStripMenuItem
            // 
            this.editarUFCDToolStripMenuItem.Name = "editarUFCDToolStripMenuItem";
            this.editarUFCDToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.editarUFCDToolStripMenuItem.Text = "Editar";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(118, 6);
            // 
            // removerToolStripMenuItem
            // 
            this.removerToolStripMenuItem.Name = "removerToolStripMenuItem";
            this.removerToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.removerToolStripMenuItem.Text = "Remover";
            // 
            // contextMenuStripTeam
            // 
            this.contextMenuStripTeam.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemEditTeam,
            this.toolStripSeparator2,
            this.toolStripMenuItemRemoveTeam});
            this.contextMenuStripTeam.Name = "contextMenuStripUFCD";
            this.contextMenuStripTeam.Size = new System.Drawing.Size(122, 54);
            // 
            // toolStripMenuItemEditTeam
            // 
            this.toolStripMenuItemEditTeam.Name = "toolStripMenuItemEditTeam";
            this.toolStripMenuItemEditTeam.Size = new System.Drawing.Size(121, 22);
            this.toolStripMenuItemEditTeam.Text = "Editar";
            this.toolStripMenuItemEditTeam.Click += new System.EventHandler(this.toolStripMenuItemEditTeam_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(118, 6);
            // 
            // toolStripMenuItemRemoveTeam
            // 
            this.toolStripMenuItemRemoveTeam.Name = "toolStripMenuItemRemoveTeam";
            this.toolStripMenuItemRemoveTeam.Size = new System.Drawing.Size(121, 22);
            this.toolStripMenuItemRemoveTeam.Text = "Remover";
            this.toolStripMenuItemRemoveTeam.Click += new System.EventHandler(this.toolStripMenuItemRemoveTeam_Click);
            // 
            // FormCourse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.metroTileAddTeam);
            this.Controls.Add(this.metroTabControl);
            this.Name = "FormCourse";
            this.Text = "Curso";
            this.Load += new System.EventHandler(this.FormCourse_Load);
            this.metroTabControl.ResumeLayout(false);
            this.metroTabPageTeams.ResumeLayout(false);
            this.metroTabPageUFCDs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUFCDs)).EndInit();
            this.contextMenuStripUFCD.ResumeLayout(false);
            this.contextMenuStripTeam.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl;
        private MetroFramework.Controls.MetroTabPage metroTabPageTeams;
        private System.Windows.Forms.TableLayoutPanel tableLayoutTeams;
        private MetroFramework.Controls.MetroTabPage metroTabPageUFCDs;
        private MetroFramework.Controls.MetroTile metroTileAddTeam;
        private System.Windows.Forms.DataGridView dataGridViewUFCDs;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripUFCD;
        private System.Windows.Forms.ToolStripMenuItem editarUFCDToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem removerToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripTeam;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemEditTeam;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRemoveTeam;
        private System.Windows.Forms.ToolTip toolTip;
    }
}