﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Database.TableModels;
using MetroFramework.Controls;
using MetroFramework.Forms;
using WinForms.AddEdit_Forms;

namespace WinForms
{
    using Database;
    using Database.Common;

    public partial class FormCourse : MetroForm
    {
        public Course Course { get; set; }

        public FormCourse(Course course)
        {
            this.Course = course;

            InitializeComponent();
            
            // Set window title and set it to the first tab
            this.Text = $"{this.Course.BaseInfo}";
            this.metroTabControl.SelectedTab = this.metroTabPageTeams;
        }

        private void FormCourse_Load(object sender, EventArgs e)
        {
            this.Activate();
            
            InitTeamList();
            InitUFCDList();
        }

        private void metroTabControl_SelectedIndexChanged (object sender, EventArgs e)
        {
            if (metroTabControl.SelectedTab == metroTabPageTeams)
            {
                metroTileAddTeam.Enabled = true;
                metroTileAddTeam.Visible = true;
            }
            else
            {
                metroTileAddTeam.Enabled = false;
                metroTileAddTeam.Visible = false;
            }
        }

        // -------------- Team Page ----------------

        #region Team Page

        /// <summary>
        /// Initialize the layout regarding the list of classes
        /// </summary>
        /// <param name="textFilter">
        /// Optional text filter
        /// </param>
        private void InitTeamList(string textFilter = "")
        {
            tableLayoutTeams.Controls.Clear();
            tableLayoutTeams.RowStyles.Clear();
            tableLayoutTeams.RowCount = 0;

            if (string.IsNullOrEmpty(textFilter))
            {
                if (this.Course.Classes.ClassList.Count > 0)
                {
                    foreach (Class team in this.Course.Classes.ClassList.OrderBy(course => course.ID))
                    {
                        tableLayoutTeams.RowCount++;
                        tableLayoutTeams.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
                        tableLayoutTeams.Controls.Add(NewTeamTile(team), 1, tableLayoutTeams.RowCount - 1);
                        tableLayoutTeams.Controls[tableLayoutTeams.Controls.Count - 1].ContextMenuStrip = contextMenuStripTeam;
                    }
                }
            }
            else
            {
                string[] keywords = textFilter.Split(' ');

                if (this.Course.Classes.ClassList.Count > 0)
                {
                    foreach (Class team in this.Course.Classes.ClassList.OrderBy(course => course.ID))
                    {
                        foreach (string keyword in keywords)
                        {
                            if (team.BaseInfo.ToLower().Contains(keyword.ToLower()))
                            {
                                tableLayoutTeams.RowCount++;
                                tableLayoutTeams.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
                                tableLayoutTeams.Controls.Add(NewTeamTile(team), 1, tableLayoutTeams.RowCount - 1);
                                tableLayoutTeams.Controls[tableLayoutTeams.Controls.Count - 1].ContextMenuStrip = contextMenuStripTeam;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Prepares a MetroTile object for insertion into the Classes table.
        /// </summary>
        /// <param name="team">Class belonging to the MetroTile</param>
        /// <returns>MetroTile regarding the class</returns>
        private MetroTile NewTeamTile(Class team)
        {
            MetroTile teamButton = new MetroTile()
            {
                Size = new Size(300, 50),
                Font = new Font("MS Sans Serif", 11),
                Text = $"{team.Name}",
                Dock = DockStyle.Top,
                Tag = team,
                Height = 50
            };
            teamButton.Click += TeamButton_Click;

            return teamButton;
        }

        /// <summary>
        /// MetroTile OnClick event, opening the specific class form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TeamButton_Click(object sender, EventArgs e)
        {
            MetroTile teamButton = (MetroTile)sender;

            Class team = teamButton.Tag as Class;

            if (team != null)
            {
                this.Hide();
                FormTeam teamForm = new FormTeam(this.Course, team);
                teamForm.ShowDialog();
                InitTeamList();
                this.Show();
                this.Activate();
            }
        }

        /// <summary>
        /// Context menu strip action to edit the current class
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemEditTeam_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            if (menuItem != null)
            {
                ContextMenuStrip menu = (ContextMenuStrip)menuItem.Owner;

                if (menu != null)
                {
                    Control selectedTeam = menu.SourceControl;

                    if (selectedTeam != null)
                    {
                        FormManageTeam formManageCourse = new FormManageTeam(this.Course, (Class)(selectedTeam.Tag));
                        
                        if (formManageCourse.ShowDialog() == DialogResult.OK)
                        {
                            InitTeamList();
                        }
                        this.Activate();
                    }
                }
            }
        }

        /// <summary>
        /// Context menu strip action to remove the current class
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemRemoveTeam_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            if (menuItem != null)
            {
                ContextMenuStrip menu = (ContextMenuStrip)menuItem.Owner;

                if (menu != null)
                {
                    Control selectedTeam = menu.SourceControl;

                    if (selectedTeam != null)
                    {
                        Class selectedValue = (Class)selectedTeam.Tag;

                        DialogResult confirmation = MessageBox.Show(
                            $"Tem a certeza que pretende eliminar {selectedValue.BaseInfo}?",
                            $"Eliminar Turma",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Warning
                        );

                        if (confirmation == DialogResult.Yes)
                        {
                            switch (this.Course.Classes.Delete(selectedValue))
                            {
                                case 0:
                                {
                                    InitTeamList();
                                    
                                } break;

                                case -2:
                                {
                                    MessageBox.Show($"A turma possui alunos associados.");
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Button to insert a new class
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroTileAddTeam_Click(object sender, EventArgs e)
        {
            FormManageTeam formManageCourse = new FormManageTeam(this.Course);
            
            formManageCourse.ShowDialog();

            InitTeamList();

            this.Activate();
        }

        #endregion

        // -------------- End Team Page ----------------

        // --------------- UFCD Page -------------------

        #region UFCD Page

        /// <summary>
        /// Populates the UFCD data grid view table
        /// </summary>
        private void InitUFCDList()
        {
            dataGridViewUFCDs.Rows.Clear();
            dataGridViewUFCDs.Columns.Clear();
            dataGridViewUFCDs.Controls.Clear();

            dataGridViewUFCDs.Columns.Add($"ID", $"ID");
            dataGridViewUFCDs.Columns.Add($"Name", $"Nome");
            dataGridViewUFCDs.Columns.Add($"Duration", $"Duração");
            dataGridViewUFCDs.Columns.Add($"Credits", $"Créditos");
            dataGridViewUFCDs.Columns.Add($"Description", $"Descrição");

            if (this.Course.UFCDs.UFCDList.Count > 0)
            {
                foreach (UFCD ufcd in this.Course.UFCDs.UFCDList.OrderBy(ufcd => ufcd.ID))
                {
                    dataGridViewUFCDs.Rows.Add();

                    DataGridViewRow tableRow = dataGridViewUFCDs.Rows[dataGridViewUFCDs.Rows.Count-1];
                    tableRow.Tag = ufcd;

                    int colPos = 0;

                    DataGridViewTextBoxCell IDcell = new DataGridViewTextBoxCell()
                    {
                        Value = $"{ufcd.ID}",
                        Tag = ufcd
                    };
                    tableRow.Cells[colPos++] = IDcell;

                    DataGridViewTextBoxCell NameCell = new DataGridViewTextBoxCell()
                    {
                        Value = $"{ufcd.Name}",
                        Tag = ufcd
                    };
                    tableRow.Cells[colPos++] = NameCell;

                    DataGridViewTextBoxCell DurationCell = new DataGridViewTextBoxCell()
                    {
                        Value = $"{ufcd.Duration}",
                        Tag = ufcd
                    };
                    tableRow.Cells[colPos++] = DurationCell;

                    DataGridViewTextBoxCell CreditsCell = new DataGridViewTextBoxCell()
                    {
                        Value = $"{ufcd.Credits}",
                        Tag = ufcd
                    };
                    tableRow.Cells[colPos++] = CreditsCell;

                    DataGridViewTextBoxCell DescriptionCell = new DataGridViewTextBoxCell()
                    {
                        Value = $"{ufcd.Description}",
                        Tag = ufcd
                    };
                    tableRow.Cells[colPos++] = DescriptionCell;
                }
            }
            
            SetupUFCDTable();
        }

        /// <summary>
        /// Sets up the UFCD datagridview table appearance
        /// </summary>
        private void SetupUFCDTable()
        {
            dataGridViewUFCDs.Columns["ID"].DisplayIndex = 0;
            dataGridViewUFCDs.Columns["ID"].Visible = true;
            dataGridViewUFCDs.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUFCDs.Columns["ID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dataGridViewUFCDs.Columns["Name"].DisplayIndex = 1;
            dataGridViewUFCDs.Columns["Name"].Visible = true;
            dataGridViewUFCDs.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUFCDs.Columns["Name"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dataGridViewUFCDs.Columns["Duration"].DisplayIndex = 2;
            dataGridViewUFCDs.Columns["Duration"].Visible = true;
            dataGridViewUFCDs.Columns["Duration"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUFCDs.Columns["Duration"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dataGridViewUFCDs.Columns["Credits"].DisplayIndex = 3;
            dataGridViewUFCDs.Columns["Credits"].Visible = true;
            dataGridViewUFCDs.Columns["Credits"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUFCDs.Columns["Credits"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dataGridViewUFCDs.Columns["Description"].DisplayIndex = 4;
            dataGridViewUFCDs.Columns["Description"].Visible = true;
            dataGridViewUFCDs.Columns["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewUFCDs.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        /// <summary>
        /// On Right Clicking the cell, ask if the user wasnt to desassociate the UFCD from the class
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewUFCDs_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    {
                        if (e.RowIndex < 0)
                        {
                            return;
                        }

                        UFCD selectedUFCD = dataGridViewUFCDs.Rows[e.RowIndex].Tag as UFCD;

                        if (selectedUFCD != null)
                        {
                            DialogResult confirmation = MessageBox.Show(
                               $"Tem a certeza que pretende desassociar {selectedUFCD.BaseInfo}?",
                               $"Desassociar UFCD",
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Warning
                           );

                                if (confirmation == DialogResult.Yes)
                                {
                                    switch (this.Course.UFCDs.Delete(selectedUFCD))
                                    {
                                        case 0:
                                            {
                                                InitUFCDList();
                                            }
                                            break;
                                    }
                                }
                            }
                    }
                    break;
            }
        }

        /// <summary>
        /// On left clicking the UFCD listings, ask what UFCD the user wants to add to the class
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewUFCDs_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    {
                        if(e.Y <= dataGridViewUFCDs.ColumnHeadersHeight)
                        {
                            return;
                        }

                        FormListManager<UFCD> frmListManager = new FormListManager<UFCD>(this.Course.UFCDs.UFCDList, Database.UFCDs.UFCDList, $"Adicione a UFCD pretendida");
                        
                        
                        if (frmListManager.ShowDialog() == DialogResult.OK)
                        {
                            Database.Courses.SaveToTextFile();
                        }

                        InitUFCDList();
                    }
                    break;
            }
        }

        #endregion

        // ------------ End UFCD Page ------------------
    }
}
