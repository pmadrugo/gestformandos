﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Database.Common;
using MetroFramework.Forms;

namespace WinForms
{
    public partial class FormListManager<T> : MetroForm
    {
        public object FilledList { get; set; }
        public List<T> SourceList { get; set; }

        string _WindowTitle;
        string _DisplayMember;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="FilledList">List currently being checked</param>
        /// <param name="SourceList">Source List being checked against</param>
        /// <param name="WindowTitle">Window Title for the dialog</param>
        /// <param name="DisplayMember">What property to use for display</param>
        public FormListManager(object FilledList, List<T> SourceList, string WindowTitle, string DisplayMember)
        {
            this.SourceList = SourceList;
            this.FilledList = FilledList;

            InitializeComponent();

            this._WindowTitle = WindowTitle;
            this._DisplayMember = DisplayMember;

            this.Text = $"{WindowTitle}";

            FilterList();
        }

        /// <summary>
        /// Default constructor; DisplayMember is assumed to be BaseInfo property
        /// </summary>
        /// <param name="FilledList">List currently being checked</param>
        /// <param name="SourceList">Source List being checked against</param>
        /// <param name="WindowTitle">Window Title for the dialog</param>
        public FormListManager(object FilledList, List<T> SourceList, string WindowTitle) 
            : this(FilledList, SourceList, WindowTitle, $"BaseInfo") { }

        private void FormListManager_Load(object sender, EventArgs e)
        {
            metroTextBoxFilter.Focus();
        }

        /// <summary>
        /// Filters the list based on text being inserted
        /// </summary>
        /// <param name="textFilter">Filter keywords</param>
        private void FilterList (string textFilter = "")
        {
            listBoxList.DataSource = null;
            listBoxList.Items.Clear();
            
            IList filledList = (IList)this.FilledList;
            
            string[] keywords = textFilter.ToLower().Split(' ');
                
            foreach (T item in this.SourceList.OrderBy(item => item.ToString()))
            {
                if (filledList.Contains(item))
                {
                    continue;
                }

                if (keywords.Length > 0)
                {
                    bool addItem = false;

                    IData itemData = item as IData;
                    foreach (string keyword in keywords)
                    {
                        if (itemData.BaseInfo.ToLower().Contains(keyword))
                        {
                            addItem = true;
                            break;
                        }
                    }

                    if (addItem)
                    {
                        listBoxList.Items.Add(item);
                    }
                    continue;
                }
                listBoxList.Items.Add(item);
            }
            listBoxList.DisplayMember = this._DisplayMember;
        }

        /// <summary>
        /// On writing filter text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroTextBoxFilter_TextChanged(object sender, EventArgs e)
        {
            FilterList(metroTextBoxFilter.Text);
            if (this.listBoxList.Items.Count > 0)
            {
                this.listBoxList.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// On pressing Enter, assume selected value as the entry to select
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormListManager_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (this.listBoxList.SelectedItem != null)
                {
                    IList filledList = (IList)this.FilledList;

                    filledList.Add((T)this.listBoxList.SelectedItem);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }
        
        /// <summary>
        /// On double click the entry, assume double clicked value as the entry to select
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = this.listBoxList.IndexFromPoint(e.Location);
            if (index > -1)
            {
                IList filledList = (IList)this.FilledList;

                filledList.Add((T)this.listBoxList.Items[index]);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}
