﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using Database.TableModels;

namespace WinForms.Common
{
    using Database;
    using Database.Common;

    public partial class UCProfile : MetroUserControl
    {
        public Profile Profile { get; set; }

        public UCProfile()
        {
            InitializeComponent();

            this.dateTimePickerBirthDate.MaxDate = DateTime.Today.AddYears(-15);

            SetupGenderList();
            SetupIDTypeList();

            this.metroTextBoxID.ReadOnly = true;
        }

        /// <summary>
        /// Sets the ID for the profile entry, sent by the parent form/controller
        /// </summary>
        /// <param name="ID"></param>
        public void SetIDFieldValue(uint ID)
        {
            metroTextBoxID.Text = ID.ToString();
        }

        /// <summary>
        /// Populates controls with the values of the entry to edit
        /// </summary>
        public void PopulateView()
        {
            this.metroTextBoxID.Text = this.Profile.ID.ToString();
            this.metroTextBoxBirthplace.Text = this.Profile.Birthplace;
            this.metroTextBoxCitizenship.Text = this.Profile.Citizenship;
            this.metroTextBoxDocumentNumber.Text = this.Profile.ID_Document_Number;
            this.metroTextBoxName.Text = this.Profile.Name;
            this.metroTextBoxNationality.Text = this.Profile.Nationality;
            this.metroTextBoxNIF.Text = this.Profile.NIF.ToString();

            this.metroComboBoxGender.SelectedIndex = (int)this.Profile.Gender;
            this.metroComboBoxIDType.SelectedIndex = (int)this.Profile.ID_Document_Type;

            this.dateTimePickerBirthDate.Value = this.Profile.BirthDate;
            this.dateTimePickerEmissionDate.Value = this.Profile.ID_Document_EmissionDate;
            this.dateTimePickerExpirationDate.Value = this.Profile.ID_Document_ExpirationDate;
        }

        /// <summary>
        /// Sets the comboboxes up on entry insertion
        /// </summary>
        public void PopulateInsertView()
        {
            this.metroComboBoxGender.SelectedIndex = 0;
            this.metroComboBoxIDType.SelectedIndex = 0;
        }

        /// <summary>
        /// Sets the Gender combobox with the three default entries
        /// </summary>
        private void SetupGenderList()
        {
            metroComboBoxGender.Items.Clear();
            metroComboBoxGender.Items.Add($"N/A");
            metroComboBoxGender.Items.Add($"Masculino");
            metroComboBoxGender.Items.Add($"Feminino");
        }

        /// <summary>
        /// Sets the ID Type combobox with the six default entries
        /// </summary>
        private void SetupIDTypeList()
        {
            metroComboBoxIDType.Items.Clear();
            metroComboBoxIDType.Items.Add($"Bilhete de Identidade");
            metroComboBoxIDType.Items.Add($"Cartão de Cidadão");
            metroComboBoxIDType.Items.Add($"Militar");
            metroComboBoxIDType.Items.Add($"Passaporte");
            metroComboBoxIDType.Items.Add($"Título de Residência");
            metroComboBoxIDType.Items.Add($"Outro");
        }

        /// <summary>
        /// Loads data from controls into the temporary entry in memory
        /// </summary>
        public Profile LoadData()
        {
            if (!ValidateForm())
            {
                return null;
            }

            Profile profileData = new Profile()
            {
                ID = Convert.ToUInt32(metroTextBoxID.Text),
                Name = metroTextBoxName.Text,
                ID_Document_Number = metroTextBoxDocumentNumber.Text,
                ID_Document_EmissionDate = dateTimePickerEmissionDate.Value,
                ID_Document_ExpirationDate = dateTimePickerExpirationDate.Value,
                ID_Document_Type = (IDType)metroComboBoxIDType.SelectedIndex,
                BirthDate = dateTimePickerBirthDate.Value,
                Birthplace = metroTextBoxBirthplace.Text,
                Citizenship = metroTextBoxCitizenship.Text,
                Gender = (Gender)metroComboBoxGender.SelectedIndex,
                Nationality = metroTextBoxNationality.Text,
                NIF = Convert.ToUInt32(metroTextBoxNIF.Text)
            };

            return profileData;
        }

        /// <summary>
        /// Checks if everything in the form is valid according to the required entry parameters
        /// </summary>
        /// <returns>
        /// True if everything checks up
        /// False if a value is incorrect
        /// </returns>
        private bool ValidateForm()
        {
            if (string.IsNullOrEmpty(metroTextBoxID.Text))
            {
                MessageBox.Show($"Este perfil necessita de um ID.");
                return false;
            }

            uint tempID;
            if (!uint.TryParse(metroTextBoxID.Text, out tempID))
            {
                MessageBox.Show($"O ID tem de ser numérico.");
                return false;
            }

            if (string.IsNullOrEmpty(metroTextBoxName.Text))
            {
                MessageBox.Show($"Este perfil tem de ter um nome atríbuido.");
                return false;
            }
            
            if (string.IsNullOrEmpty(metroTextBoxDocumentNumber.Text))
            {
                MessageBox.Show($"O perfil necessita de ter o número do seu documento preenchido.");
                return false;
            }

            uint tempDocumentNumber;
            if (!uint.TryParse(metroTextBoxDocumentNumber.Text, out tempDocumentNumber))
            {
                MessageBox.Show($"O documento de identificação tem de ser valor numérico.");
                return false;
            }

            if (string.IsNullOrEmpty(metroTextBoxNIF.Text))
            {
                MessageBox.Show($"O NIF é de preenchimento obrigatório");
                return false;
            }

            uint NIFtemp;
            if (!uint.TryParse(metroTextBoxNIF.Text, out NIFtemp))
            {
                MessageBox.Show($"O NIF tem de ser numérico!");
                return false;
            }

            if (metroTextBoxNIF.Text.Length != 9)
            {
                MessageBox.Show($"O NIF tem de ter 9 digitos.");
                return false;
            }
            if (dateTimePickerEmissionDate.Value >= dateTimePickerExpirationDate.Value)
            {
                MessageBox.Show($"A data de emissão não pode ser igual ou superior de validade.");
                return false;
            }

            return true;
        }
    }
}
