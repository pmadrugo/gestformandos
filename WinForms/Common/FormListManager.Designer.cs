﻿namespace WinForms
{
    partial class FormListManager<T>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainerText_List = new System.Windows.Forms.SplitContainer();
            this.metroTextBoxFilter = new MetroFramework.Controls.MetroTextBox();
            this.listBoxList = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerText_List)).BeginInit();
            this.splitContainerText_List.Panel1.SuspendLayout();
            this.splitContainerText_List.Panel2.SuspendLayout();
            this.splitContainerText_List.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerText_List
            // 
            this.splitContainerText_List.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerText_List.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainerText_List.IsSplitterFixed = true;
            this.splitContainerText_List.Location = new System.Drawing.Point(20, 60);
            this.splitContainerText_List.Name = "splitContainerText_List";
            this.splitContainerText_List.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerText_List.Panel1
            // 
            this.splitContainerText_List.Panel1.Controls.Add(this.metroTextBoxFilter);
            // 
            // splitContainerText_List.Panel2
            // 
            this.splitContainerText_List.Panel2.Controls.Add(this.listBoxList);
            this.splitContainerText_List.Size = new System.Drawing.Size(360, 220);
            this.splitContainerText_List.SplitterDistance = 25;
            this.splitContainerText_List.TabIndex = 1;
            // 
            // metroTextBoxFilter
            // 
            this.metroTextBoxFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTextBoxFilter.Location = new System.Drawing.Point(0, 0);
            this.metroTextBoxFilter.Name = "metroTextBoxFilter";
            this.metroTextBoxFilter.Size = new System.Drawing.Size(360, 25);
            this.metroTextBoxFilter.TabIndex = 0;
            this.metroTextBoxFilter.TextChanged += new System.EventHandler(this.metroTextBoxFilter_TextChanged);
            // 
            // listBoxList
            // 
            this.listBoxList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxList.FormattingEnabled = true;
            this.listBoxList.Location = new System.Drawing.Point(0, 0);
            this.listBoxList.Name = "listBoxList";
            this.listBoxList.Size = new System.Drawing.Size(360, 191);
            this.listBoxList.TabIndex = 1;
            this.listBoxList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxList_MouseDoubleClick);
            // 
            // FormListManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Drawing.MetroBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(400, 300);
            this.Controls.Add(this.splitContainerText_List);
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(400, 300);
            this.MinimumSize = new System.Drawing.Size(400, 300);
            this.Name = "FormListManager";
            this.Resizable = false;
            this.Load += new System.EventHandler(this.FormListManager_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormListManager_KeyDown);
            this.splitContainerText_List.Panel1.ResumeLayout(false);
            this.splitContainerText_List.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerText_List)).EndInit();
            this.splitContainerText_List.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainerText_List;
        private System.Windows.Forms.ListBox listBoxList;
        private MetroFramework.Controls.MetroTextBox metroTextBoxFilter;
    }
}