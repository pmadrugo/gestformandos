﻿namespace WinForms.Common
{
    partial class UCProfile
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxID = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBoxGender = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.dateTimePickerBirthDate = new System.Windows.Forms.DateTimePicker();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBoxIDType = new MetroFramework.Controls.MetroComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxDocumentNumber = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.dateTimePickerEmissionDate = new System.Windows.Forms.DateTimePicker();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.dateTimePickerExpirationDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxNIF = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxNationality = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxCitizenship = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxBirthplace = new MetroFramework.Controls.MetroTextBox();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(10, 15);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(21, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "ID";
            // 
            // metroTextBoxID
            // 
            this.metroTextBoxID.Location = new System.Drawing.Point(37, 13);
            this.metroTextBoxID.Name = "metroTextBoxID";
            this.metroTextBoxID.Size = new System.Drawing.Size(75, 23);
            this.metroTextBoxID.TabIndex = 0;
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(124, 15);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(46, 19);
            this.metroLabel2.TabIndex = 0;
            this.metroLabel2.Text = "Nome";
            // 
            // metroTextBoxName
            // 
            this.metroTextBoxName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroTextBoxName.Location = new System.Drawing.Point(176, 13);
            this.metroTextBoxName.Name = "metroTextBoxName";
            this.metroTextBoxName.Size = new System.Drawing.Size(540, 23);
            this.metroTextBoxName.TabIndex = 1;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(10, 57);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(52, 19);
            this.metroLabel3.TabIndex = 0;
            this.metroLabel3.Text = "Género";
            // 
            // metroComboBoxGender
            // 
            this.metroComboBoxGender.FormattingEnabled = true;
            this.metroComboBoxGender.ItemHeight = 23;
            this.metroComboBoxGender.Location = new System.Drawing.Point(68, 53);
            this.metroComboBoxGender.Name = "metroComboBoxGender";
            this.metroComboBoxGender.Size = new System.Drawing.Size(136, 29);
            this.metroComboBoxGender.TabIndex = 2;
            // 
            // metroLabel4
            // 
            this.metroLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(392, 57);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(128, 19);
            this.metroLabel4.TabIndex = 0;
            this.metroLabel4.Text = "Data de Nascimento";
            // 
            // dateTimePickerBirthDate
            // 
            this.dateTimePickerBirthDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerBirthDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.dateTimePickerBirthDate.Location = new System.Drawing.Point(526, 55);
            this.dateTimePickerBirthDate.Name = "dateTimePickerBirthDate";
            this.dateTimePickerBirthDate.Size = new System.Drawing.Size(190, 23);
            this.dateTimePickerBirthDate.TabIndex = 3;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(10, 123);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(146, 19);
            this.metroLabel5.TabIndex = 0;
            this.metroLabel5.Text = "Tipo de Documentação";
            // 
            // metroComboBoxIDType
            // 
            this.metroComboBoxIDType.FormattingEnabled = true;
            this.metroComboBoxIDType.ItemHeight = 23;
            this.metroComboBoxIDType.Location = new System.Drawing.Point(162, 118);
            this.metroComboBoxIDType.Name = "metroComboBoxIDType";
            this.metroComboBoxIDType.Size = new System.Drawing.Size(186, 29);
            this.metroComboBoxIDType.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label1.Location = new System.Drawing.Point(10, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(706, 1);
            this.label1.TabIndex = 4;
            // 
            // metroLabel6
            // 
            this.metroLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(371, 123);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(150, 19);
            this.metroLabel6.TabIndex = 0;
            this.metroLabel6.Text = "Número do Documento";
            // 
            // metroTextBoxDocumentNumber
            // 
            this.metroTextBoxDocumentNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroTextBoxDocumentNumber.Location = new System.Drawing.Point(534, 123);
            this.metroTextBoxDocumentNumber.MaxLength = 15;
            this.metroTextBoxDocumentNumber.Name = "metroTextBoxDocumentNumber";
            this.metroTextBoxDocumentNumber.Size = new System.Drawing.Size(182, 23);
            this.metroTextBoxDocumentNumber.TabIndex = 5;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(11, 167);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(106, 19);
            this.metroLabel7.TabIndex = 0;
            this.metroLabel7.Text = "Data de Emissão";
            // 
            // dateTimePickerEmissionDate
            // 
            this.dateTimePickerEmissionDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.dateTimePickerEmissionDate.Location = new System.Drawing.Point(124, 166);
            this.dateTimePickerEmissionDate.Name = "dateTimePickerEmissionDate";
            this.dateTimePickerEmissionDate.Size = new System.Drawing.Size(190, 23);
            this.dateTimePickerEmissionDate.TabIndex = 6;
            // 
            // metroLabel8
            // 
            this.metroLabel8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(413, 168);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(108, 19);
            this.metroLabel8.TabIndex = 0;
            this.metroLabel8.Text = "Data de Validade";
            // 
            // dateTimePickerExpirationDate
            // 
            this.dateTimePickerExpirationDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerExpirationDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.dateTimePickerExpirationDate.Location = new System.Drawing.Point(526, 166);
            this.dateTimePickerExpirationDate.Name = "dateTimePickerExpirationDate";
            this.dateTimePickerExpirationDate.Size = new System.Drawing.Size(190, 23);
            this.dateTimePickerExpirationDate.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label2.Location = new System.Drawing.Point(12, 214);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(706, 1);
            this.label2.TabIndex = 5;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(10, 233);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(29, 19);
            this.metroLabel9.TabIndex = 0;
            this.metroLabel9.Text = "NIF";
            // 
            // metroTextBoxNIF
            // 
            this.metroTextBoxNIF.Location = new System.Drawing.Point(101, 231);
            this.metroTextBoxNIF.MaxLength = 9;
            this.metroTextBoxNIF.Name = "metroTextBoxNIF";
            this.metroTextBoxNIF.Size = new System.Drawing.Size(213, 23);
            this.metroTextBoxNIF.TabIndex = 8;
            // 
            // metroLabel10
            // 
            this.metroLabel10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(406, 231);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(93, 19);
            this.metroLabel10.TabIndex = 0;
            this.metroLabel10.Text = "Nacionalidade";
            // 
            // metroTextBoxNationality
            // 
            this.metroTextBoxNationality.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroTextBoxNationality.Location = new System.Drawing.Point(505, 229);
            this.metroTextBoxNationality.Name = "metroTextBoxNationality";
            this.metroTextBoxNationality.Size = new System.Drawing.Size(213, 23);
            this.metroTextBoxNationality.TabIndex = 9;
            // 
            // metroLabel11
            // 
            this.metroLabel11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(406, 271);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(68, 19);
            this.metroLabel11.TabIndex = 0;
            this.metroLabel11.Text = "Cidadania";
            // 
            // metroTextBoxCitizenship
            // 
            this.metroTextBoxCitizenship.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroTextBoxCitizenship.Location = new System.Drawing.Point(505, 269);
            this.metroTextBoxCitizenship.Name = "metroTextBoxCitizenship";
            this.metroTextBoxCitizenship.Size = new System.Drawing.Size(213, 23);
            this.metroTextBoxCitizenship.TabIndex = 11;
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(10, 273);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(85, 19);
            this.metroLabel12.TabIndex = 0;
            this.metroLabel12.Text = "Naturalidade";
            // 
            // metroTextBoxBirthplace
            // 
            this.metroTextBoxBirthplace.Location = new System.Drawing.Point(101, 271);
            this.metroTextBoxBirthplace.Name = "metroTextBoxBirthplace";
            this.metroTextBoxBirthplace.Size = new System.Drawing.Size(213, 23);
            this.metroTextBoxBirthplace.TabIndex = 10;
            // 
            // UCProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePickerExpirationDate);
            this.Controls.Add(this.dateTimePickerEmissionDate);
            this.Controls.Add(this.dateTimePickerBirthDate);
            this.Controls.Add(this.metroComboBoxIDType);
            this.Controls.Add(this.metroComboBoxGender);
            this.Controls.Add(this.metroTextBoxDocumentNumber);
            this.Controls.Add(this.metroTextBoxName);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroTextBoxCitizenship);
            this.Controls.Add(this.metroTextBoxNationality);
            this.Controls.Add(this.metroTextBoxBirthplace);
            this.Controls.Add(this.metroTextBoxNIF);
            this.Controls.Add(this.metroTextBoxID);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel12);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel1);
            this.Name = "UCProfile";
            this.Size = new System.Drawing.Size(731, 340);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox metroTextBoxID;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox metroTextBoxName;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroComboBox metroComboBoxGender;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.DateTimePicker dateTimePickerBirthDate;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroComboBox metroComboBoxIDType;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox metroTextBoxDocumentNumber;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private System.Windows.Forms.DateTimePicker dateTimePickerEmissionDate;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private System.Windows.Forms.DateTimePicker dateTimePickerExpirationDate;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroTextBox metroTextBoxNIF;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroTextBox metroTextBoxNationality;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroTextBox metroTextBoxCitizenship;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroTextBox metroTextBoxBirthplace;
    }
}
