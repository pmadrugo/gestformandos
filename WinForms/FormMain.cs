﻿namespace WinForms
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Database.TableModels;
    using Database;
    using MetroFramework.Forms;
    using MetroFramework.Controls;
    using WinForms.AddEdit_Forms;

    public partial class FormMain : MetroForm
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            this.Activate();

            InitCourseList();
            InitUFCDList();
            InitProfessorList();

            this.metroTabControl.SelectedTab = metroTabCursos;
        }

        private void metroTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.metroTabControl.SelectedIndex == 0)
            {
                metroTileAddCourse.Enabled = true;
                metroTileAddCourse.Visible = true;
            }
            else
            {
                metroTileAddCourse.Enabled = false;
                metroTileAddCourse.Visible = false;
            }
        }

        // ------------------ Course Page -----------------

        #region Course Page

        /// <summary>
        /// Prepares the table showing the courses currently added
        /// </summary>
        /// <param name="textFilter">Optional text filter</param>
        private void InitCourseList (string textFilter = "")
        {
            tableLayoutCourses.Controls.Clear();
            tableLayoutCourses.RowStyles.Clear();
            tableLayoutCourses.RowCount = 0;

            if (string.IsNullOrEmpty(textFilter))
            {
                if (Database.Courses.CourseList.Count > 0)
                {
                    foreach (Course course in Database.Courses.CourseList.OrderBy(course => course.ID))
                    {
                        tableLayoutCourses.RowCount++;
                        tableLayoutCourses.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
                        tableLayoutCourses.Controls.Add(NewCourseTile(course), 1, tableLayoutCourses.RowCount - 1);
                        tableLayoutCourses.Controls[tableLayoutCourses.Controls.Count - 1].ContextMenuStrip = contextMenuStripCourse;
                    }
                }
            }
            else
            {
                string[] keywords = textFilter.Split(' ');
                if (Database.Courses.CourseList.Count > 0)
                {
                    foreach (Course course in Database.Courses.CourseList.OrderBy(course => course.ID))
                    {
                        foreach (string keyword in keywords)
                        {
                            if (course.BaseInfo.ToLower().Contains(keyword.ToLower()))
                            {
                                tableLayoutCourses.RowCount++;
                                tableLayoutCourses.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
                                tableLayoutCourses.Controls.Add(NewCourseTile(course), 1, tableLayoutCourses.RowCount - 1);
                                tableLayoutCourses.Controls[tableLayoutCourses.Controls.Count - 1].ContextMenuStrip = contextMenuStripCourse;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Prepares the MetroTile object regarding the Course
        /// </summary>
        /// <param name="course">Course belonging to the MetroTile</param>
        /// <returns></returns>
        private MetroTile NewCourseTile(Course course)
        {
            MetroTile courseButton = new MetroTile()
            {
                Size = new Size(300, 50),
                Font = new Font("MS Sans Serif", 11),
                Text = $"{course.ID} - {course.Name}",
                Dock = DockStyle.Top,
                Tag = course,
                Height = 50
            };
            courseButton.Click += CourseButton_Click;
            this.toolTip.SetToolTip(courseButton, $"{course.Description}");

            return courseButton;
        }

        /// <summary>
        /// OnClick event for the MetroTile action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CourseButton_Click(object sender, EventArgs e)
        {
            MetroTile courseButton = (MetroTile)sender;

            Course course = courseButton.Tag as Course;

            if (course != null)
            {
                this.Hide();
                FormCourse courseForm = new FormCourse(course);
                courseForm.ShowDialog();
                InitCourseList();
                this.Show();
                this.Activate();
            }
        }

        /// <summary>
        /// Insert new course
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroTileAddCourse_Click(object sender, EventArgs e)
        {
            FormManageCourse formManageCourse = new FormManageCourse();
            
            if (formManageCourse.ShowDialog() == DialogResult.OK)
            {
                InitCourseList();
            }

            this.Activate();
        }

        /// <summary>
        /// Context menu action to edit the selected Course
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemEditCourse_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            if (menuItem != null)
            {
                ContextMenuStrip menu = (ContextMenuStrip)menuItem.Owner;

                if (menu != null)
                {
                    Control selectedCourse = menu.SourceControl;

                    if (selectedCourse != null)
                    {
                        FormManageCourse formManageCourse = new FormManageCourse((Course)(selectedCourse.Tag));
                        
                        if (formManageCourse.ShowDialog() == DialogResult.OK)
                        {
                            InitCourseList();
                        }

                        this.Activate();
                    }
                }
            }
        }

        /// <summary>
        /// Context menu action to remove the selected course
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemRemoveCourse_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            if (menuItem != null)
            {
                ContextMenuStrip menu = (ContextMenuStrip)menuItem.Owner;

                if (menu != null)
                {
                    Control selectedCourse = menu.SourceControl;

                    if (selectedCourse != null)
                    {
                        Course selectedValue = (Course)selectedCourse.Tag;

                        DialogResult confirmation = MessageBox.Show(
                            $"Tem a certeza que pretende eliminar {selectedValue.BaseInfo}?",
                            $"Eliminar Curso",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Warning
                        );

                        if (confirmation == DialogResult.Yes)
                        {
                            switch (Database.Courses.Delete(selectedValue))
                            {
                                case 0:
                                {
                                    MessageBox.Show($"Curso apagado com sucesso.");
                                    InitCourseList();
                                } break;
                                case -2:
                                {
                                    MessageBox.Show($"O curso possui turmas associadas.");
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }

        #endregion

        // --------- End Course Page ---------

        // --------- UFCD Page --------------

        #region UFCD Page

        /// <summary>
        /// Populates the UFCD datagridview with the available UFCDs sorted by ID
        /// </summary>
        private void InitUFCDList ()
        {
            dataGridViewUFCDs.AutoGenerateColumns = true;

            dataGridViewUFCDs.Columns.Clear();
            dataGridViewUFCDs.Rows.Clear();
            dataGridViewUFCDs.Controls.Clear();

            dataGridViewUFCDs.Columns.Add($"ID", $"ID");
            dataGridViewUFCDs.Columns.Add($"Name", $"Nome");
            dataGridViewUFCDs.Columns.Add($"Duration", $"Duração");
            dataGridViewUFCDs.Columns.Add($"Credits", $"Créditos");
            dataGridViewUFCDs.Columns.Add($"Description", $"Descrição");

            if (Database.UFCDs.UFCDList.Count > 0)
            {
                foreach (UFCD ufcd in Database.UFCDs.UFCDList.OrderBy(ufcd => ufcd.ID))
                {
                    dataGridViewUFCDs.Rows.Add();

                    DataGridViewRow tableRow = dataGridViewUFCDs.Rows[dataGridViewUFCDs.Rows.Count-1];

                    tableRow.Tag = ufcd;

                    tableRow.Cells["ID"].Value = ufcd.ID.ToString();
                    tableRow.Cells["Name"].Value = ufcd.Name;
                    tableRow.Cells["Duration"].Value = ufcd.Duration;
                    tableRow.Cells["Credits"].Value = ufcd.Credits;
                    tableRow.Cells["Description"].Value = ufcd.Description;
                }
            }

            dataGridViewUFCDs.AutoGenerateColumns = false;

            SetupUFCDTable();
        }

        /// <summary>
        /// Sets up the UFCD table appearance
        /// </summary>
        private void SetupUFCDTable()
        {
            dataGridViewUFCDs.Columns["ID"].DisplayIndex = 0;
            dataGridViewUFCDs.Columns["ID"].Visible = true;
            dataGridViewUFCDs.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUFCDs.Columns["ID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dataGridViewUFCDs.Columns["Name"].DisplayIndex = 1;
            dataGridViewUFCDs.Columns["Name"].Visible = true;
            dataGridViewUFCDs.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUFCDs.Columns["Name"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewUFCDs.Columns["Name"].HeaderText = $"Nome";

            dataGridViewUFCDs.Columns["Duration"].DisplayIndex = 2;
            dataGridViewUFCDs.Columns["Duration"].Visible = true;
            dataGridViewUFCDs.Columns["Duration"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUFCDs.Columns["Duration"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewUFCDs.Columns["Duration"].HeaderText = $"Duração";

            dataGridViewUFCDs.Columns["Credits"].DisplayIndex = 3;
            dataGridViewUFCDs.Columns["Credits"].Visible = true;
            dataGridViewUFCDs.Columns["Credits"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUFCDs.Columns["Credits"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewUFCDs.Columns["Credits"].HeaderText = $"Créditos";

            dataGridViewUFCDs.Columns["Description"].DisplayIndex = 4;
            dataGridViewUFCDs.Columns["Description"].Visible = true;
            dataGridViewUFCDs.Columns["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewUFCDs.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewUFCDs.Columns["Description"].HeaderText = $"Descrição";
        }

        /// <summary>
        /// Click event for the button to add a new UFCD
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddUFCD(object sender, EventArgs e)
        {
            FormManageUFCD frmManageUFCD = new FormManageUFCD();

            frmManageUFCD.ShowDialog();

            InitUFCDList();
        }

        /// <summary>
        /// Click event to edit an existing UFCD
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditUFCD(object sender, EventArgs e)
        {
            UFCD currentUFCD = dataGridViewUFCDs.CurrentRow.Tag as UFCD;

            if (currentUFCD != null)
            {
                FormManageUFCD frmManageUFCD = new FormManageUFCD(currentUFCD);

                frmManageUFCD.ShowDialog();

                InitUFCDList();
            }
        }

        /// <summary>
        /// Click event to delete an existing UFCD
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteUFCD(object sender, EventArgs e)
        {
            UFCD currentUFCD = dataGridViewUFCDs.CurrentRow.Tag as UFCD;

            if (currentUFCD != null)
            {
                DialogResult confirmDelete = MessageBox.Show(
                    $"Deseja apagar a UFCD {currentUFCD.BaseInfo}?",
                    $"Apagar UFCD",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning
                );

                if (confirmDelete == DialogResult.Yes)
                {
                    switch (Database.UFCDs.Delete(currentUFCD))
                    {
                        case 0:
                        {
                            InitUFCDList();
                        } break;
                    }
                }
            }
        }

        #endregion

        // --------- End UFCD Page -----------

        // --------- Professor Page -----------

        #region Professor Page

        /// <summary>
        /// Populates the datagridview table with information regarding professors
        /// </summary>
        private void InitProfessorList ()
        {
            dataGridViewProfessors.AutoGenerateColumns = true;

            dataGridViewProfessors.Columns.Clear();
            dataGridViewProfessors.Rows.Clear();
            dataGridViewProfessors.Controls.Clear();

            dataGridViewProfessors.Columns.Add($"ID", $"ID");
            dataGridViewProfessors.Columns.Add($"Name", $"Nome");

            if (Database.Professors.ProfessorList.Count > 0)
            {
                foreach (Professor professor in Database.Professors.ProfessorList.OrderBy(professor => professor.Name))
                {
                    dataGridViewProfessors.Rows.Add();

                    DataGridViewRow tableRow = dataGridViewProfessors.Rows[dataGridViewProfessors.Rows.Count - 1];

                    tableRow.Tag = professor;

                    tableRow.Cells["ID"].Value = professor.ID.ToString();
                    tableRow.Cells["Name"].Value = professor.Name;
                }
            }
            
            dataGridViewProfessors.AutoGenerateColumns = false;

            SetupProfessorTable();
        }

        /// <summary>
        /// Sets up the Professor table appearance
        /// </summary>
        private void SetupProfessorTable()
        {
            dataGridViewProfessors.Columns["ID"].DisplayIndex = 0;
            dataGridViewProfessors.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewProfessors.Columns["ID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dataGridViewProfessors.Columns["Name"].DisplayIndex = 1;
            dataGridViewProfessors.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewProfessors.Columns["Name"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        /// <summary>
        /// Click event for adding a professor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroTileAddProfessor_Click(object sender, EventArgs e)
        {
            FormManageProfessor frmManageProfessor = new FormManageProfessor();

            frmManageProfessor.ShowDialog();
            InitProfessorList();
        }

        /// <summary>
        /// Click event for editing an existing professor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroTileEditProfessor_Click(object sender, EventArgs e)
        {
            Professor selectedProfessor = dataGridViewProfessors.CurrentRow.Tag as Professor;

            if (selectedProfessor != null)
            {
                FormManageProfessor frmManageProfessor = new FormManageProfessor(selectedProfessor);

                frmManageProfessor.ShowDialog();
                InitProfessorList();
            }
        }

        /// <summary>
        /// Click event for removing an existing professor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroTileDeleteProfessor_Click(object sender, EventArgs e)
        {
            Professor selectedProfessor = dataGridViewProfessors.CurrentRow.Tag as Professor;

            if (selectedProfessor != null)
            {
                DialogResult confirmation = MessageBox.Show(
                           $"Tem a certeza que pretende remover {selectedProfessor.BaseInfo}?",
                           $"Remover Professor",
                           MessageBoxButtons.YesNo,
                           MessageBoxIcon.Warning
                       );

                if (confirmation == DialogResult.Yes)
                {
                    switch (Database.Professors.Delete(selectedProfessor))
                    {
                        case 0:
                            {
                                InitProfessorList();
                            }
                            break;
                        case -2:
                        {
                            MessageBox.Show($"O professor seleccionado possui UFCDs associadas. Desassocie as primeiro.");
                        } break;
                        case -3:
                        {
                            MessageBox.Show($"O professor seleccionado possui turmas associadas. Desassocie as primeiro.");
                        }
                        break;
                    }
                }
            }
        }

        #endregion

        // --------- End Professor Page -------

        private void btnAbout_Click(object sender, EventArgs e)
        {
            AboutBox about = new AboutBox();
            about.ShowDialog();
        }
    }
}
