﻿namespace WinForms
{
    partial class FormTeam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewStudents = new System.Windows.Forms.DataGridView();
            this.splitContainerTable_Buttons = new System.Windows.Forms.SplitContainer();
            this.splitContainerAddEdit_Delete = new System.Windows.Forms.SplitContainer();
            this.splitContainerAdd_Edit = new System.Windows.Forms.SplitContainer();
            this.metroTileAdd = new MetroFramework.Controls.MetroTile();
            this.metroTileEdit = new MetroFramework.Controls.MetroTile();
            this.metroTileDelete = new MetroFramework.Controls.MetroTile();
            this.metroTabControl = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPageStudents = new MetroFramework.Controls.MetroTabPage();
            this.metroTabPageEvaluations = new MetroFramework.Controls.MetroTabPage();
            this.dataGridViewEvaluations = new System.Windows.Forms.DataGridView();
            this.metroTabPageProfessors = new MetroFramework.Controls.MetroTabPage();
            this.dataGridViewProfessorClass = new System.Windows.Forms.DataGridView();
            this.contextMenuStripAddStudent = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addExistingStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerTable_Buttons)).BeginInit();
            this.splitContainerTable_Buttons.Panel1.SuspendLayout();
            this.splitContainerTable_Buttons.Panel2.SuspendLayout();
            this.splitContainerTable_Buttons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerAddEdit_Delete)).BeginInit();
            this.splitContainerAddEdit_Delete.Panel1.SuspendLayout();
            this.splitContainerAddEdit_Delete.Panel2.SuspendLayout();
            this.splitContainerAddEdit_Delete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerAdd_Edit)).BeginInit();
            this.splitContainerAdd_Edit.Panel1.SuspendLayout();
            this.splitContainerAdd_Edit.Panel2.SuspendLayout();
            this.splitContainerAdd_Edit.SuspendLayout();
            this.metroTabControl.SuspendLayout();
            this.metroTabPageStudents.SuspendLayout();
            this.metroTabPageEvaluations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEvaluations)).BeginInit();
            this.metroTabPageProfessors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProfessorClass)).BeginInit();
            this.contextMenuStripAddStudent.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewStudents
            // 
            this.dataGridViewStudents.AllowUserToAddRows = false;
            this.dataGridViewStudents.AllowUserToDeleteRows = false;
            this.dataGridViewStudents.AllowUserToResizeColumns = false;
            this.dataGridViewStudents.AllowUserToResizeRows = false;
            this.dataGridViewStudents.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStudents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewStudents.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewStudents.MultiSelect = false;
            this.dataGridViewStudents.Name = "dataGridViewStudents";
            this.dataGridViewStudents.ReadOnly = true;
            this.dataGridViewStudents.RowHeadersVisible = false;
            this.dataGridViewStudents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewStudents.Size = new System.Drawing.Size(677, 326);
            this.dataGridViewStudents.TabIndex = 0;
            // 
            // splitContainerTable_Buttons
            // 
            this.splitContainerTable_Buttons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerTable_Buttons.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainerTable_Buttons.IsSplitterFixed = true;
            this.splitContainerTable_Buttons.Location = new System.Drawing.Point(0, 5);
            this.splitContainerTable_Buttons.Name = "splitContainerTable_Buttons";
            // 
            // splitContainerTable_Buttons.Panel1
            // 
            this.splitContainerTable_Buttons.Panel1.Controls.Add(this.dataGridViewStudents);
            // 
            // splitContainerTable_Buttons.Panel2
            // 
            this.splitContainerTable_Buttons.Panel2.Controls.Add(this.splitContainerAddEdit_Delete);
            this.splitContainerTable_Buttons.Size = new System.Drawing.Size(752, 326);
            this.splitContainerTable_Buttons.SplitterDistance = 677;
            this.splitContainerTable_Buttons.TabIndex = 1;
            // 
            // splitContainerAddEdit_Delete
            // 
            this.splitContainerAddEdit_Delete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerAddEdit_Delete.Location = new System.Drawing.Point(0, 0);
            this.splitContainerAddEdit_Delete.Name = "splitContainerAddEdit_Delete";
            this.splitContainerAddEdit_Delete.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerAddEdit_Delete.Panel1
            // 
            this.splitContainerAddEdit_Delete.Panel1.Controls.Add(this.splitContainerAdd_Edit);
            // 
            // splitContainerAddEdit_Delete.Panel2
            // 
            this.splitContainerAddEdit_Delete.Panel2.Controls.Add(this.metroTileDelete);
            this.splitContainerAddEdit_Delete.Size = new System.Drawing.Size(71, 326);
            this.splitContainerAddEdit_Delete.SplitterDistance = 214;
            this.splitContainerAddEdit_Delete.TabIndex = 0;
            // 
            // splitContainerAdd_Edit
            // 
            this.splitContainerAdd_Edit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerAdd_Edit.Location = new System.Drawing.Point(0, 0);
            this.splitContainerAdd_Edit.Name = "splitContainerAdd_Edit";
            this.splitContainerAdd_Edit.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerAdd_Edit.Panel1
            // 
            this.splitContainerAdd_Edit.Panel1.Controls.Add(this.metroTileAdd);
            // 
            // splitContainerAdd_Edit.Panel2
            // 
            this.splitContainerAdd_Edit.Panel2.Controls.Add(this.metroTileEdit);
            this.splitContainerAdd_Edit.Size = new System.Drawing.Size(71, 214);
            this.splitContainerAdd_Edit.SplitterDistance = 99;
            this.splitContainerAdd_Edit.TabIndex = 0;
            // 
            // metroTileAdd
            // 
            this.metroTileAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTileAdd.Location = new System.Drawing.Point(0, 0);
            this.metroTileAdd.Name = "metroTileAdd";
            this.metroTileAdd.Size = new System.Drawing.Size(71, 99);
            this.metroTileAdd.TabIndex = 0;
            this.metroTileAdd.Text = "Adicionar";
            this.metroTileAdd.Click += new System.EventHandler(this.metroTileAdd_Click);
            // 
            // metroTileEdit
            // 
            this.metroTileEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTileEdit.Location = new System.Drawing.Point(0, 0);
            this.metroTileEdit.Name = "metroTileEdit";
            this.metroTileEdit.Size = new System.Drawing.Size(71, 111);
            this.metroTileEdit.TabIndex = 0;
            this.metroTileEdit.Text = "Editar";
            this.metroTileEdit.Click += new System.EventHandler(this.metroTileEdit_Click);
            // 
            // metroTileDelete
            // 
            this.metroTileDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTileDelete.Location = new System.Drawing.Point(0, 0);
            this.metroTileDelete.Name = "metroTileDelete";
            this.metroTileDelete.Size = new System.Drawing.Size(71, 108);
            this.metroTileDelete.TabIndex = 0;
            this.metroTileDelete.Text = "Remover";
            this.metroTileDelete.Click += new System.EventHandler(this.metroTileDelete_Click);
            // 
            // metroTabControl
            // 
            this.metroTabControl.Controls.Add(this.metroTabPageStudents);
            this.metroTabControl.Controls.Add(this.metroTabPageProfessors);
            this.metroTabControl.Controls.Add(this.metroTabPageEvaluations);
            this.metroTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabControl.Location = new System.Drawing.Point(20, 60);
            this.metroTabControl.Name = "metroTabControl";
            this.metroTabControl.SelectedIndex = 2;
            this.metroTabControl.Size = new System.Drawing.Size(760, 370);
            this.metroTabControl.TabIndex = 2;
            // 
            // metroTabPageStudents
            // 
            this.metroTabPageStudents.Controls.Add(this.splitContainerTable_Buttons);
            this.metroTabPageStudents.HorizontalScrollbarBarColor = true;
            this.metroTabPageStudents.Location = new System.Drawing.Point(4, 35);
            this.metroTabPageStudents.Name = "metroTabPageStudents";
            this.metroTabPageStudents.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.metroTabPageStudents.Size = new System.Drawing.Size(752, 331);
            this.metroTabPageStudents.TabIndex = 0;
            this.metroTabPageStudents.Text = "Estudantes";
            this.metroTabPageStudents.VerticalScrollbarBarColor = true;
            // 
            // metroTabPageEvaluations
            // 
            this.metroTabPageEvaluations.Controls.Add(this.dataGridViewEvaluations);
            this.metroTabPageEvaluations.HorizontalScrollbarBarColor = true;
            this.metroTabPageEvaluations.Location = new System.Drawing.Point(4, 35);
            this.metroTabPageEvaluations.Name = "metroTabPageEvaluations";
            this.metroTabPageEvaluations.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.metroTabPageEvaluations.Size = new System.Drawing.Size(752, 331);
            this.metroTabPageEvaluations.TabIndex = 1;
            this.metroTabPageEvaluations.Text = "Avaliações";
            this.metroTabPageEvaluations.VerticalScrollbarBarColor = true;
            // 
            // dataGridViewEvaluations
            // 
            this.dataGridViewEvaluations.AllowUserToAddRows = false;
            this.dataGridViewEvaluations.AllowUserToDeleteRows = false;
            this.dataGridViewEvaluations.AllowUserToResizeColumns = false;
            this.dataGridViewEvaluations.AllowUserToResizeRows = false;
            this.dataGridViewEvaluations.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewEvaluations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEvaluations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewEvaluations.Location = new System.Drawing.Point(0, 5);
            this.dataGridViewEvaluations.MultiSelect = false;
            this.dataGridViewEvaluations.Name = "dataGridViewEvaluations";
            this.dataGridViewEvaluations.ReadOnly = true;
            this.dataGridViewEvaluations.RowHeadersVisible = false;
            this.dataGridViewEvaluations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEvaluations.Size = new System.Drawing.Size(752, 326);
            this.dataGridViewEvaluations.TabIndex = 2;
            this.dataGridViewEvaluations.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewEvaluations_CellClick);
            // 
            // metroTabPageProfessors
            // 
            this.metroTabPageProfessors.Controls.Add(this.dataGridViewProfessorClass);
            this.metroTabPageProfessors.HorizontalScrollbarBarColor = true;
            this.metroTabPageProfessors.Location = new System.Drawing.Point(4, 35);
            this.metroTabPageProfessors.Name = "metroTabPageProfessors";
            this.metroTabPageProfessors.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.metroTabPageProfessors.Size = new System.Drawing.Size(752, 331);
            this.metroTabPageProfessors.TabIndex = 2;
            this.metroTabPageProfessors.Text = "Formadores";
            this.metroTabPageProfessors.VerticalScrollbarBarColor = true;
            // 
            // dataGridViewProfessorClass
            // 
            this.dataGridViewProfessorClass.AllowUserToAddRows = false;
            this.dataGridViewProfessorClass.AllowUserToDeleteRows = false;
            this.dataGridViewProfessorClass.AllowUserToResizeColumns = false;
            this.dataGridViewProfessorClass.AllowUserToResizeRows = false;
            this.dataGridViewProfessorClass.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewProfessorClass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProfessorClass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewProfessorClass.Location = new System.Drawing.Point(0, 5);
            this.dataGridViewProfessorClass.MultiSelect = false;
            this.dataGridViewProfessorClass.Name = "dataGridViewProfessorClass";
            this.dataGridViewProfessorClass.ReadOnly = true;
            this.dataGridViewProfessorClass.RowHeadersVisible = false;
            this.dataGridViewProfessorClass.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewProfessorClass.Size = new System.Drawing.Size(752, 326);
            this.dataGridViewProfessorClass.TabIndex = 3;
            this.dataGridViewProfessorClass.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewProfessorClass_CellClick);
            this.dataGridViewProfessorClass.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewProfessorClass_CellMouseDown);
            // 
            // contextMenuStripAddStudent
            // 
            this.contextMenuStripAddStudent.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newStudentToolStripMenuItem,
            this.addExistingStudentToolStripMenuItem});
            this.contextMenuStripAddStudent.Name = "contextMenuStripAddStudent";
            this.contextMenuStripAddStudent.Size = new System.Drawing.Size(184, 48);
            // 
            // newStudentToolStripMenuItem
            // 
            this.newStudentToolStripMenuItem.Name = "newStudentToolStripMenuItem";
            this.newStudentToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.newStudentToolStripMenuItem.Text = "Novo Aluno";
            this.newStudentToolStripMenuItem.Click += new System.EventHandler(this.newStudentToolStripMenuItem_Click);
            // 
            // addExistingStudentToolStripMenuItem
            // 
            this.addExistingStudentToolStripMenuItem.Name = "addExistingStudentToolStripMenuItem";
            this.addExistingStudentToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.addExistingStudentToolStripMenuItem.Text = "Adicionar Existente...";
            this.addExistingStudentToolStripMenuItem.Click += new System.EventHandler(this.addExistingStudentToolStripMenuItem_Click);
            // 
            // FormTeam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.metroTabControl);
            this.Name = "FormTeam";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "FormTeam";
            this.Load += new System.EventHandler(this.FormTeam_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).EndInit();
            this.splitContainerTable_Buttons.Panel1.ResumeLayout(false);
            this.splitContainerTable_Buttons.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerTable_Buttons)).EndInit();
            this.splitContainerTable_Buttons.ResumeLayout(false);
            this.splitContainerAddEdit_Delete.Panel1.ResumeLayout(false);
            this.splitContainerAddEdit_Delete.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerAddEdit_Delete)).EndInit();
            this.splitContainerAddEdit_Delete.ResumeLayout(false);
            this.splitContainerAdd_Edit.Panel1.ResumeLayout(false);
            this.splitContainerAdd_Edit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerAdd_Edit)).EndInit();
            this.splitContainerAdd_Edit.ResumeLayout(false);
            this.metroTabControl.ResumeLayout(false);
            this.metroTabPageStudents.ResumeLayout(false);
            this.metroTabPageEvaluations.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEvaluations)).EndInit();
            this.metroTabPageProfessors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProfessorClass)).EndInit();
            this.contextMenuStripAddStudent.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewStudents;
        private System.Windows.Forms.SplitContainer splitContainerTable_Buttons;
        private System.Windows.Forms.SplitContainer splitContainerAdd_Edit;
        private System.Windows.Forms.SplitContainer splitContainerAddEdit_Delete;
        private MetroFramework.Controls.MetroTile metroTileAdd;
        private MetroFramework.Controls.MetroTile metroTileEdit;
        private MetroFramework.Controls.MetroTile metroTileDelete;
        private MetroFramework.Controls.MetroTabControl metroTabControl;
        private MetroFramework.Controls.MetroTabPage metroTabPageStudents;
        private MetroFramework.Controls.MetroTabPage metroTabPageEvaluations;
        private System.Windows.Forms.DataGridView dataGridViewEvaluations;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripAddStudent;
        private System.Windows.Forms.ToolStripMenuItem newStudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addExistingStudentToolStripMenuItem;
        private MetroFramework.Controls.MetroTabPage metroTabPageProfessors;
        private System.Windows.Forms.DataGridView dataGridViewProfessorClass;
    }
}