﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Database.TableModels;
using MetroFramework.Forms;


namespace WinForms.AddEdit_Forms
{
    using Database;
    using Database.Common;

    public partial class FormManageTeam : MetroForm
    {
        public Course ParentCourse { get; set; }

        public Class NewTeam { get; set; }
        public Class OldTeam { get; set; }
        EventHandler _FunctionToPerform;

        /// <summary>
        /// Edit class
        /// </summary>
        /// <param name="parentCourse">Course to which the class belongs to</param>
        /// <param name="team">Class to edit</param>
        public FormManageTeam(Course parentCourse, Class team) : this(parentCourse)
        {
            this.OldTeam = team;

            Core<Course>.CopyValues(this.OldTeam, this.NewTeam);

            this.Text = $"{this.OldTeam.BaseInfo}";

            PopulateView();

            _FunctionToPerform = this.EditButton;

            metroTextBoxID.ReadOnly = true;
        }

        /// <summary>
        /// Insert new class
        /// </summary>
        /// <param name="parentCourse">Course to which the class will belong to</param>
        public FormManageTeam(Course parentCourse)
        {
            InitializeComponent();

            this.ParentCourse = parentCourse;
            this.NewTeam = new Class();

            this.metroTextBoxID.Text = $"{Database.Classes.GetNewID()}";

            _FunctionToPerform = this.InsertButton;

            metroTextBoxID.ReadOnly = true;
        }

        private void FormManageCourse_Load(object sender, EventArgs e)
        {
            this.Activate();
            
            this.metroButtonSave.Click += _FunctionToPerform;
        }

        /// <summary>
        /// Populates controls with the values of the entry to edit
        /// </summary>
        private void PopulateView()
        {
            metroTextBoxID.Text = this.NewTeam.ID.ToString();
            metroTextBoxName.Text = this.NewTeam.Name;
            dateTimePickerStartDate.Value = this.NewTeam.StartDate;
            dateTimePickerEndDate.Value = this.NewTeam.EndDate;

            InitLists();
        }

        private void InitLists()
        {

        }

        /// <summary>
        /// Loads data from controls into the temporary entry in memory
        /// </summary>
        private void LoadData()
        {
            this.NewTeam.ID = Convert.ToUInt32(metroTextBoxID.Text);
            this.NewTeam.Name = metroTextBoxName.Text;
            this.NewTeam.StartDate = dateTimePickerStartDate.Value;
            this.NewTeam.EndDate = dateTimePickerEndDate.Value;
        }

        private void metroButtonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        // --- Save Button

        /// <summary>
        /// Insert entry button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InsertButton(object sender, EventArgs e)
        {
            if (!ValidateForm())
            {
                return;
            }

            LoadData();

            switch (this.ParentCourse.Classes.Add(NewTeam))
            {
                case 0:
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                } break;
            }
        }

        /// <summary>
        /// Edit entry button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditButton(object sender, EventArgs e)
        {
            if (!ValidateForm())
            {
                return;
            }

            LoadData();

            switch (this.ParentCourse.Classes.Update(this.OldTeam, this.NewTeam))
            {
                case 0:
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                } break;
            }
        }

        // --- End Save Buttons


        /// <summary>
        /// Checks if everything in the form is valid according to the required entry parameters
        /// </summary>
        /// <returns>
        /// True if everything checks up
        /// False if a value is incorrect
        /// </returns>
        private bool ValidateForm()
        {
            if (string.IsNullOrEmpty(metroTextBoxID.Text))
            {
                MessageBox.Show($"A turma precisa de ter um ID! Verifique o em http://www.anqep.gov.pt/default.aspx");
                return false;
            }
            uint IDtemp;
            if (!uint.TryParse(metroTextBoxID.Text, out IDtemp))
            {
                MessageBox.Show($"É necessário que o ID seja numérico!");
                return false;
            }
            if (string.IsNullOrEmpty(metroTextBoxName.Text))
            {
                MessageBox.Show($"A turma deve possuir um nome.");
                return false;
            }
            if (dateTimePickerStartDate.Value >= dateTimePickerEndDate.Value)
            {
                MessageBox.Show($"A turma não pode terminar antes de iniciar!");
                return false;
            }
            return true;
        }
    }
}
