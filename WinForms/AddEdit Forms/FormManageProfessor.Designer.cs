﻿using WinForms.Common;

namespace WinForms.AddEdit_Forms
{
    partial class FormManageProfessor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ucProfile = new WinForms.Common.UCProfile();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dataGridViewUFCDs = new System.Windows.Forms.DataGridView();
            this.metroButtonSave = new MetroFramework.Controls.MetroButton();
            this.metroButtonCancel = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUFCDs)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(20, 60);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ucProfile);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(760, 502);
            this.splitContainer1.SplitterDistance = 302;
            this.splitContainer1.TabIndex = 0;
            // 
            // ucProfile
            // 
            this.ucProfile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucProfile.Location = new System.Drawing.Point(0, 0);
            this.ucProfile.Name = "ucProfile";
            this.ucProfile.Profile = null;
            this.ucProfile.Size = new System.Drawing.Size(760, 302);
            this.ucProfile.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridViewUFCDs);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.metroButtonSave);
            this.splitContainer2.Panel2.Controls.Add(this.metroButtonCancel);
            this.splitContainer2.Size = new System.Drawing.Size(760, 196);
            this.splitContainer2.SplitterDistance = 149;
            this.splitContainer2.TabIndex = 9;
            // 
            // dataGridViewUFCDs
            // 
            this.dataGridViewUFCDs.AllowUserToAddRows = false;
            this.dataGridViewUFCDs.AllowUserToDeleteRows = false;
            this.dataGridViewUFCDs.AllowUserToResizeColumns = false;
            this.dataGridViewUFCDs.AllowUserToResizeRows = false;
            this.dataGridViewUFCDs.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewUFCDs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUFCDs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewUFCDs.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewUFCDs.MultiSelect = false;
            this.dataGridViewUFCDs.Name = "dataGridViewUFCDs";
            this.dataGridViewUFCDs.ReadOnly = true;
            this.dataGridViewUFCDs.RowHeadersVisible = false;
            this.dataGridViewUFCDs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewUFCDs.Size = new System.Drawing.Size(760, 149);
            this.dataGridViewUFCDs.TabIndex = 3;
            this.dataGridViewUFCDs.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewUFCDs_CellMouseDown);
            this.dataGridViewUFCDs.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridViewUFCDs_MouseDown);
            // 
            // metroButtonSave
            // 
            this.metroButtonSave.Location = new System.Drawing.Point(3, 6);
            this.metroButtonSave.Name = "metroButtonSave";
            this.metroButtonSave.Size = new System.Drawing.Size(121, 34);
            this.metroButtonSave.TabIndex = 7;
            this.metroButtonSave.Text = "Guardar";
            // 
            // metroButtonCancel
            // 
            this.metroButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButtonCancel.Location = new System.Drawing.Point(636, 5);
            this.metroButtonCancel.Name = "metroButtonCancel";
            this.metroButtonCancel.Size = new System.Drawing.Size(121, 34);
            this.metroButtonCancel.TabIndex = 8;
            this.metroButtonCancel.Text = "Cancelar";
            this.metroButtonCancel.Click += new System.EventHandler(this.metroButtonCancel_Click);
            // 
            // FormManageProfessor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Drawing.MetroBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(800, 582);
            this.Controls.Add(this.splitContainer1);
            this.MaximumSize = new System.Drawing.Size(800, 582);
            this.MinimumSize = new System.Drawing.Size(800, 582);
            this.Name = "FormManageProfessor";
            this.Resizable = false;
            this.Text = "Novo Professor";
            this.Load += new System.EventHandler(this.FormManageProfessor_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUFCDs)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private UCProfile ucProfile;
        private MetroFramework.Controls.MetroButton metroButtonCancel;
        private MetroFramework.Controls.MetroButton metroButtonSave;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView dataGridViewUFCDs;
    }
}