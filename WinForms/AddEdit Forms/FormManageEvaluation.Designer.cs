﻿namespace WinForms.AddEdit_Forms
{
    partial class FormManageEvaluation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numericUpDownGrade = new System.Windows.Forms.NumericUpDown();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxDescription = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonCancel = new MetroFramework.Controls.MetroButton();
            this.metroButtonSave = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGrade)).BeginInit();
            this.SuspendLayout();
            // 
            // numericUpDownGrade
            // 
            this.numericUpDownGrade.BackColor = System.Drawing.SystemColors.Control;
            this.numericUpDownGrade.DecimalPlaces = 2;
            this.numericUpDownGrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.numericUpDownGrade.Location = new System.Drawing.Point(113, 88);
            this.numericUpDownGrade.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownGrade.Name = "numericUpDownGrade";
            this.numericUpDownGrade.Size = new System.Drawing.Size(131, 23);
            this.numericUpDownGrade.TabIndex = 0;
            this.numericUpDownGrade.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(67, 89);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(38, 19);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "Nota";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(67, 129);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(65, 19);
            this.metroLabel2.TabIndex = 1;
            this.metroLabel2.Text = "Descrição";
            // 
            // metroTextBoxDescription
            // 
            this.metroTextBoxDescription.Location = new System.Drawing.Point(138, 129);
            this.metroTextBoxDescription.Multiline = true;
            this.metroTextBoxDescription.Name = "metroTextBoxDescription";
            this.metroTextBoxDescription.Size = new System.Drawing.Size(379, 71);
            this.metroTextBoxDescription.TabIndex = 2;
            // 
            // metroButtonCancel
            // 
            this.metroButtonCancel.Location = new System.Drawing.Point(492, 242);
            this.metroButtonCancel.Name = "metroButtonCancel";
            this.metroButtonCancel.Size = new System.Drawing.Size(121, 34);
            this.metroButtonCancel.TabIndex = 13;
            this.metroButtonCancel.Text = "Cancelar";
            this.metroButtonCancel.Click += new System.EventHandler(this.metroButtonCancel_Click);
            // 
            // metroButtonSave
            // 
            this.metroButtonSave.Location = new System.Drawing.Point(23, 242);
            this.metroButtonSave.Name = "metroButtonSave";
            this.metroButtonSave.Size = new System.Drawing.Size(121, 34);
            this.metroButtonSave.TabIndex = 12;
            this.metroButtonSave.Text = "Guardar";
            // 
            // FormManageEvaluation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Drawing.MetroBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(636, 299);
            this.Controls.Add(this.metroButtonCancel);
            this.Controls.Add(this.metroButtonSave);
            this.Controls.Add(this.metroTextBoxDescription);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.numericUpDownGrade);
            this.MaximumSize = new System.Drawing.Size(636, 299);
            this.MinimumSize = new System.Drawing.Size(636, 299);
            this.Name = "FormManageEvaluation";
            this.Resizable = false;
            this.Text = "Nova Avaliação";
            this.Load += new System.EventHandler(this.FormManageEvaluation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGrade)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numericUpDownGrade;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox metroTextBoxDescription;
        private MetroFramework.Controls.MetroButton metroButtonCancel;
        private MetroFramework.Controls.MetroButton metroButtonSave;
    }
}