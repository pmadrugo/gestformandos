﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Database.TableModels;
using MetroFramework.Forms;

namespace WinForms.AddEdit_Forms
{
    using Database;
    using Database.Common;

    public partial class FormManageProfessor : MetroForm
    {
        public Class ParentTeam { get; set; }

        public Professor OldProfessor { get; set; }
        public Professor NewProfessor { get; set; }

        EventHandler _FunctionToPerform;

        /// <summary>
        /// Edit Professor
        /// </summary>
        /// <param name="professor"></param>
        public FormManageProfessor(Professor professor) : this()
        {
            this.OldProfessor = professor;

            Core<Professor>.CopyValues(this.OldProfessor, this.NewProfessor);

            this.ucProfile.Profile = this.NewProfessor;
            this.ucProfile.PopulateView();

            this.Text = $"{professor.BaseInfo}";

            _FunctionToPerform = EditProfessor;
        }
        /// <summary>
        /// New professor
        /// </summary>
        public FormManageProfessor()
        {
            this.ParentTeam = ParentTeam;
            this.NewProfessor = new Professor();

            InitializeComponent();

            _FunctionToPerform = AddProfessor;

            
        }

        private void FormManageProfessor_Load(object sender, EventArgs e)
        {
            if (this.OldProfessor == null)
            {
                this.ucProfile.PopulateInsertView();
                this.ucProfile.SetIDFieldValue(Database.Professors.GetNewID());
            }

            this.InitUFCDList();
            metroButtonSave.Click += _FunctionToPerform;

            this.Activate();
        }

        /// <summary>
        /// Cancel button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroButtonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Loads data from controls into the temporary entry in memory
        /// </summary>
        private void LoadData(Profile Profile)
        {
            this.NewProfessor.ID = Profile.ID;
            this.NewProfessor.Name = Profile.Name;
            this.NewProfessor.ID_Document_EmissionDate = Profile.ID_Document_EmissionDate;
            this.NewProfessor.ID_Document_ExpirationDate = Profile.ID_Document_ExpirationDate;
            this.NewProfessor.ID_Document_Number = Profile.ID_Document_Number;
            this.NewProfessor.ID_Document_Type = Profile.ID_Document_Type;
            this.NewProfessor.Nationality = Profile.Nationality;
            this.NewProfessor.NIF = Profile.NIF;
            this.NewProfessor.BirthDate = Profile.BirthDate;
            this.NewProfessor.Birthplace = Profile.Birthplace;
            this.NewProfessor.Citizenship = Profile.Citizenship;
            this.NewProfessor.Gender = Profile.Gender;
        }

        // --- Save Button

        /// <summary>
        /// Insert entry button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddProfessor(object sender, EventArgs e)
        {
            Profile loadedProfile = this.ucProfile.LoadData();

            if (loadedProfile == null)
            {
                return;
            }

            LoadData(loadedProfile);

            switch (Database.Professors.Add(NewProfessor))
            {
                case 0:
                {
                    MessageBox.Show($"Professor adicionado com sucesso!");
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                } break;
            }
        }

        /// <summary>
        /// Edit entry button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditProfessor(object sender, EventArgs e)
        {
            Profile loadedProfile = this.ucProfile.LoadData();

            if (loadedProfile == null)
            {
                return;
            }

            LoadData(loadedProfile);

            switch (Database.Professors.Update(this.OldProfessor, this.NewProfessor))
            {
                case 0:
                {
                    MessageBox.Show($"Professor editado com sucesso!");
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                } break;
                case -1:
                {
                    MessageBox.Show($"Já existe um professor com esse NIF!");
                    return;
                }
                break;
            }
        }

        // ----- UFCD Section

        #region UFCD Section

        /// <summary>
        /// Populates the UFCD data grid view table
        /// </summary>
        private void InitUFCDList()
        {
            dataGridViewUFCDs.Rows.Clear();
            dataGridViewUFCDs.Columns.Clear();
            dataGridViewUFCDs.Controls.Clear();

            dataGridViewUFCDs.Columns.Add($"ID", $"ID");
            dataGridViewUFCDs.Columns.Add($"Name", $"Nome");
            dataGridViewUFCDs.Columns.Add($"Duration", $"Duração");
            dataGridViewUFCDs.Columns.Add($"Credits", $"Créditos");
            dataGridViewUFCDs.Columns.Add($"Description", $"Descrição");

            if (this.NewProfessor.UFCDs.UFCDList.Count > 0)
            {
                foreach (UFCD ufcd in this.NewProfessor.UFCDs.UFCDList.OrderBy(ufcd => ufcd.ID))
                {
                    dataGridViewUFCDs.Rows.Add();

                    DataGridViewRow tableRow = dataGridViewUFCDs.Rows[dataGridViewUFCDs.Rows.Count - 1];
                    tableRow.Tag = ufcd;

                    int colPos = 0;

                    DataGridViewTextBoxCell IDcell = new DataGridViewTextBoxCell()
                    {
                        Value = $"{ufcd.ID}",
                        Tag = ufcd
                    };
                    tableRow.Cells[colPos++] = IDcell;

                    DataGridViewTextBoxCell NameCell = new DataGridViewTextBoxCell()
                    {
                        Value = $"{ufcd.Name}",
                        Tag = ufcd
                    };
                    tableRow.Cells[colPos++] = NameCell;

                    DataGridViewTextBoxCell DurationCell = new DataGridViewTextBoxCell()
                    {
                        Value = $"{ufcd.Duration}",
                        Tag = ufcd
                    };
                    tableRow.Cells[colPos++] = DurationCell;

                    DataGridViewTextBoxCell CreditsCell = new DataGridViewTextBoxCell()
                    {
                        Value = $"{ufcd.Credits}",
                        Tag = ufcd
                    };
                    tableRow.Cells[colPos++] = CreditsCell;

                    DataGridViewTextBoxCell DescriptionCell = new DataGridViewTextBoxCell()
                    {
                        Value = $"{ufcd.Description}",
                        Tag = ufcd
                    };
                    tableRow.Cells[colPos++] = DescriptionCell;
                }
            }

            SetupUFCDTable();
        }

        /// <summary>
        /// Sets up the UFCD datagridview table appearance
        /// </summary>
        private void SetupUFCDTable()
        {
            dataGridViewUFCDs.Columns["ID"].DisplayIndex = 0;
            dataGridViewUFCDs.Columns["ID"].Visible = true;
            dataGridViewUFCDs.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUFCDs.Columns["ID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dataGridViewUFCDs.Columns["Name"].DisplayIndex = 1;
            dataGridViewUFCDs.Columns["Name"].Visible = true;
            dataGridViewUFCDs.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUFCDs.Columns["Name"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dataGridViewUFCDs.Columns["Duration"].DisplayIndex = 2;
            dataGridViewUFCDs.Columns["Duration"].Visible = true;
            dataGridViewUFCDs.Columns["Duration"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUFCDs.Columns["Duration"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dataGridViewUFCDs.Columns["Credits"].DisplayIndex = 3;
            dataGridViewUFCDs.Columns["Credits"].Visible = true;
            dataGridViewUFCDs.Columns["Credits"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUFCDs.Columns["Credits"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dataGridViewUFCDs.Columns["Description"].DisplayIndex = 4;
            dataGridViewUFCDs.Columns["Description"].Visible = true;
            dataGridViewUFCDs.Columns["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewUFCDs.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        /// <summary>
        /// On Right Clicking the cell, ask if the user wasnt to desassociate the UFCD from the class
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewUFCDs_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    {
                        UFCD selectedUFCD = dataGridViewUFCDs.Rows[e.RowIndex].Tag as UFCD;

                        if (selectedUFCD != null)
                        {
                            DialogResult confirmation = MessageBox.Show(
                               $"Tem a certeza que pretende desassociar {selectedUFCD.BaseInfo}?",
                               $"Desassociar UFCD",
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Warning
                           );

                            if (confirmation == DialogResult.Yes)
                            {
                                this.NewProfessor.UFCDs.UFCDList.Remove(selectedUFCD);

                                InitUFCDList();
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// On left clicking the UFCD listings, ask what UFCD the user wants to add to the class
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewUFCDs_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    {
                        FormListManager<UFCD> frmListManager = new FormListManager<UFCD>(this.NewProfessor.UFCDs.UFCDList, Database.UFCDs.UFCDList, $"Adicione a UFCD pretendida");

                        if (frmListManager.ShowDialog() == DialogResult.OK)
                        {
                            Database.Courses.SaveToTextFile();
                        }

                        InitUFCDList();
                    }
                    break;
            }
        }

        #endregion  

        // ----- End UFCD Section

    }
}
