﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Database.TableModels;
using MetroFramework.Forms;

namespace WinForms.AddEdit_Forms
{
    using Database;
    using Database.Common;

    public partial class FormManageUFCD : MetroForm
    {
        public UFCD NewUFCD { get; set; }
        public UFCD OldUFCD { get; set; }
        EventHandler _FunctionToPerform;

        /// <summary>
        /// Edit Course
        /// </summary>
        /// <param name="course">Course to Edit</param>
        public FormManageUFCD(UFCD ufcd) : this()
        {
            this.OldUFCD = ufcd;

            Core<UFCD>.CopyValues(this.OldUFCD, this.NewUFCD);

            this.Text = $"{this.OldUFCD.BaseInfo}";

            PopulateView();

            _FunctionToPerform = this.EditButton;

            this.metroTextBoxID.ReadOnly = true;
        }

        /// <summary>
        /// Inserts a new course
        /// </summary>
        public FormManageUFCD()
        {
            InitializeComponent();

            this.NewUFCD = new UFCD();

            this.metroTextBoxID.Text = $"{Database.UFCDs.GetNewID()}";

            _FunctionToPerform = this.InsertButton;
        }

        private void FormManageUFCD_Load(object sender, EventArgs e)
        {
            this.Activate();

            this.metroButtonSave.Click += _FunctionToPerform;
        }

        /// <summary>
        /// Populates controls with the values of the entry to edit
        /// </summary>
        private void PopulateView()
        {
            metroTextBoxID.Text = this.NewUFCD.ID.ToString();
            metroTextBoxDescription.Text = this.NewUFCD.Description;
            metroTextBoxName.Text = this.NewUFCD.Name;
            numericUpDownCredits.Value = Convert.ToDecimal(this.NewUFCD.Credits);
            numericUpDownDuration.Value = Convert.ToDecimal(this.NewUFCD.Duration);
        }

        /// <summary>
        /// Loads data from controls into the temporary entry in memory
        /// </summary>
        private void LoadData()
        {
            this.NewUFCD.ID = Convert.ToUInt32(metroTextBoxID.Text);
            this.NewUFCD.Description = metroTextBoxDescription.Text;
            this.NewUFCD.Name = metroTextBoxName.Text;

            this.NewUFCD.Duration = Convert.ToDouble(numericUpDownDuration.Value);
            this.NewUFCD.Credits = Convert.ToDouble(numericUpDownCredits.Value);
        }

        // --- Save button

        /// <summary>
        /// Edit entry button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditButton(object sender, EventArgs e)
        {
            if (!ValidateForm())
            {
                return;
            }

            LoadData();

            switch (Database.UFCDs.Update(this.OldUFCD, this.NewUFCD))
            {
                case 0:
                    {
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    } break;
            }
        }

        /// <summary>
        /// Insert entry button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InsertButton(object sender, EventArgs e)
        {
            if (!ValidateForm())
            {
                return;
            }

            LoadData();

            switch (Database.UFCDs.Add(this.NewUFCD))
            {
                case 0:
                    {
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    } break;
            }
        }

        // --- End save button

        /// <summary>
        /// Checks if everything in the form is valid according to the required entry parameters
        /// </summary>
        /// <returns>
        /// True if everything checks up
        /// False if a value is incorrect
        /// </returns>
        private bool ValidateForm()
        {
            if (string.IsNullOrEmpty(metroTextBoxID.Text))
            {
                MessageBox.Show($"A UFCD tem de possuir um ID");
                return false;
            }
            uint IDtemp;
            if (!uint.TryParse(metroTextBoxID.Text, out IDtemp))
            {
                MessageBox.Show($"O ID tem de ser numérico");
                return false;
            }
            if (string.IsNullOrEmpty(metroTextBoxName.Text))
            {
                MessageBox.Show($"A UFCD tem de ter um nome.");
                return false;
            }
            if (numericUpDownCredits.Value <= 0)
            {
                MessageBox.Show($"A UFCD tem de valer alguns créditos.");
                return false;
            }
            if (numericUpDownDuration.Value <= 0)
            {
                MessageBox.Show($"A UFCD tem de ter uma duração.");
                return false;
            }
            return true;
        }

        /// <summary>
        /// Cancel button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroButtonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
