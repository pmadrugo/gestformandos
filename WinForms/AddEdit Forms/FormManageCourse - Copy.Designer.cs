﻿namespace WinForms.AddEdit_Forms
{
    partial class FormManageCourse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.metroTextBoxID = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxName = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxDescription = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.listBoxClasses = new System.Windows.Forms.ListBox();
            this.listBoxUFCDs = new System.Windows.Forms.ListBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.metroButtonSave = new MetroFramework.Controls.MetroButton();
            this.metroButtonCancel = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroTextBoxID
            // 
            this.metroTextBoxID.Location = new System.Drawing.Point(76, 75);
            this.metroTextBoxID.Name = "metroTextBoxID";
            this.metroTextBoxID.Size = new System.Drawing.Size(81, 23);
            this.metroTextBoxID.TabIndex = 0;
            this.metroTextBoxID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(49, 76);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(21, 19);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "ID";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(163, 77);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(46, 19);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Nome";
            // 
            // metroTextBoxName
            // 
            this.metroTextBoxName.Location = new System.Drawing.Point(215, 75);
            this.metroTextBoxName.Name = "metroTextBoxName";
            this.metroTextBoxName.Size = new System.Drawing.Size(514, 23);
            this.metroTextBoxName.TabIndex = 1;
            // 
            // metroTextBoxDescription
            // 
            this.metroTextBoxDescription.Location = new System.Drawing.Point(129, 110);
            this.metroTextBoxDescription.Multiline = true;
            this.metroTextBoxDescription.Name = "metroTextBoxDescription";
            this.metroTextBoxDescription.Size = new System.Drawing.Size(600, 75);
            this.metroTextBoxDescription.TabIndex = 2;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(49, 111);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(65, 19);
            this.metroLabel3.TabIndex = 2;
            this.metroLabel3.Text = "Descrição";
            // 
            // listBoxClasses
            // 
            this.listBoxClasses.BackColor = System.Drawing.Color.White;
            this.listBoxClasses.ColumnWidth = 150;
            this.listBoxClasses.FormattingEnabled = true;
            this.listBoxClasses.Location = new System.Drawing.Point(49, 202);
            this.listBoxClasses.Name = "listBoxClasses";
            this.listBoxClasses.Size = new System.Drawing.Size(300, 147);
            this.listBoxClasses.TabIndex = 3;
            this.toolTip.SetToolTip(this.listBoxClasses, "Clique Esquerdo: Adicionar\r\nClique Direito: Remover");
            this.listBoxClasses.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBoxClasses_MouseDown);
            // 
            // listBoxUFCDs
            // 
            this.listBoxUFCDs.BackColor = System.Drawing.Color.White;
            this.listBoxUFCDs.FormattingEnabled = true;
            this.listBoxUFCDs.Location = new System.Drawing.Point(429, 202);
            this.listBoxUFCDs.Name = "listBoxUFCDs";
            this.listBoxUFCDs.Size = new System.Drawing.Size(300, 147);
            this.listBoxUFCDs.TabIndex = 4;
            this.toolTip.SetToolTip(this.listBoxUFCDs, "Clique Esquerdo: Adicionar");
            this.listBoxUFCDs.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBoxUFCDs_MouseDown);
            // 
            // metroButtonSave
            // 
            this.metroButtonSave.Location = new System.Drawing.Point(24, 393);
            this.metroButtonSave.Name = "metroButtonSave";
            this.metroButtonSave.Size = new System.Drawing.Size(121, 34);
            this.metroButtonSave.TabIndex = 5;
            this.metroButtonSave.Text = "Guardar";
            // 
            // metroButtonCancel
            // 
            this.metroButtonCancel.Location = new System.Drawing.Point(656, 393);
            this.metroButtonCancel.Name = "metroButtonCancel";
            this.metroButtonCancel.Size = new System.Drawing.Size(121, 34);
            this.metroButtonCancel.TabIndex = 6;
            this.metroButtonCancel.Text = "Cancelar";
            this.metroButtonCancel.Click += new System.EventHandler(this.metroButtonCancel_Click);
            // 
            // FormManageCourse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.metroButtonCancel);
            this.Controls.Add(this.metroButtonSave);
            this.Controls.Add(this.listBoxUFCDs);
            this.Controls.Add(this.listBoxClasses);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroTextBoxDescription);
            this.Controls.Add(this.metroTextBoxName);
            this.Controls.Add(this.metroTextBoxID);
            this.Name = "FormManageCourse";
            this.Text = "Novo Curso";
            this.Load += new System.EventHandler(this.FormManageCourse_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox metroTextBoxID;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox metroTextBoxName;
        private MetroFramework.Controls.MetroTextBox metroTextBoxDescription;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private System.Windows.Forms.ListBox listBoxClasses;
        private System.Windows.Forms.ListBox listBoxUFCDs;
        private System.Windows.Forms.ToolTip toolTip;
        private MetroFramework.Controls.MetroButton metroButtonSave;
        private MetroFramework.Controls.MetroButton metroButtonCancel;
    }
}