﻿namespace WinForms.AddEdit_Forms
{
    partial class FormManageUFCD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxDescription = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxName = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxID = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.numericUpDownDuration = new System.Windows.Forms.NumericUpDown();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.numericUpDownCredits = new System.Windows.Forms.NumericUpDown();
            this.metroButtonCancel = new MetroFramework.Controls.MetroButton();
            this.metroButtonSave = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCredits)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(49, 150);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(65, 19);
            this.metroLabel3.TabIndex = 6;
            this.metroLabel3.Text = "Descrição";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(163, 80);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(46, 19);
            this.metroLabel2.TabIndex = 7;
            this.metroLabel2.Text = "Nome";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(49, 79);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(21, 19);
            this.metroLabel1.TabIndex = 4;
            this.metroLabel1.Text = "ID";
            // 
            // metroTextBoxDescription
            // 
            this.metroTextBoxDescription.Location = new System.Drawing.Point(129, 149);
            this.metroTextBoxDescription.Multiline = true;
            this.metroTextBoxDescription.Name = "metroTextBoxDescription";
            this.metroTextBoxDescription.Size = new System.Drawing.Size(600, 75);
            this.metroTextBoxDescription.TabIndex = 4;
            // 
            // metroTextBoxName
            // 
            this.metroTextBoxName.Location = new System.Drawing.Point(215, 78);
            this.metroTextBoxName.Name = "metroTextBoxName";
            this.metroTextBoxName.Size = new System.Drawing.Size(514, 23);
            this.metroTextBoxName.TabIndex = 1;
            // 
            // metroTextBoxID
            // 
            this.metroTextBoxID.Location = new System.Drawing.Point(76, 78);
            this.metroTextBoxID.Name = "metroTextBoxID";
            this.metroTextBoxID.Size = new System.Drawing.Size(81, 23);
            this.metroTextBoxID.TabIndex = 0;
            this.metroTextBoxID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(49, 115);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(58, 19);
            this.metroLabel4.TabIndex = 4;
            this.metroLabel4.Text = "Duração";
            // 
            // numericUpDownDuration
            // 
            this.numericUpDownDuration.BackColor = System.Drawing.SystemColors.Control;
            this.numericUpDownDuration.DecimalPlaces = 2;
            this.numericUpDownDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.numericUpDownDuration.Location = new System.Drawing.Point(114, 115);
            this.numericUpDownDuration.Name = "numericUpDownDuration";
            this.numericUpDownDuration.Size = new System.Drawing.Size(95, 23);
            this.numericUpDownDuration.TabIndex = 2;
            this.numericUpDownDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(207, 115);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(16, 19);
            this.metroLabel5.TabIndex = 4;
            this.metroLabel5.Text = "h";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(555, 115);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(58, 19);
            this.metroLabel6.TabIndex = 4;
            this.metroLabel6.Text = "Créditos";
            // 
            // numericUpDownCredits
            // 
            this.numericUpDownCredits.BackColor = System.Drawing.SystemColors.Control;
            this.numericUpDownCredits.DecimalPlaces = 2;
            this.numericUpDownCredits.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.numericUpDownCredits.Location = new System.Drawing.Point(620, 115);
            this.numericUpDownCredits.Name = "numericUpDownCredits";
            this.numericUpDownCredits.Size = new System.Drawing.Size(109, 23);
            this.numericUpDownCredits.TabIndex = 3;
            this.numericUpDownCredits.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // metroButtonCancel
            // 
            this.metroButtonCancel.Location = new System.Drawing.Point(655, 263);
            this.metroButtonCancel.Name = "metroButtonCancel";
            this.metroButtonCancel.Size = new System.Drawing.Size(121, 34);
            this.metroButtonCancel.TabIndex = 11;
            this.metroButtonCancel.Text = "Cancelar";
            this.metroButtonCancel.Click += new System.EventHandler(this.metroButtonCancel_Click);
            // 
            // metroButtonSave
            // 
            this.metroButtonSave.Location = new System.Drawing.Point(23, 263);
            this.metroButtonSave.Name = "metroButtonSave";
            this.metroButtonSave.Size = new System.Drawing.Size(121, 34);
            this.metroButtonSave.TabIndex = 10;
            this.metroButtonSave.Text = "Guardar";
            // 
            // FormManageUFCD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Drawing.MetroBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(800, 313);
            this.Controls.Add(this.metroButtonCancel);
            this.Controls.Add(this.metroButtonSave);
            this.Controls.Add(this.numericUpDownCredits);
            this.Controls.Add(this.numericUpDownDuration);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroTextBoxDescription);
            this.Controls.Add(this.metroTextBoxName);
            this.Controls.Add(this.metroTextBoxID);
            this.MaximumSize = new System.Drawing.Size(800, 313);
            this.MinimumSize = new System.Drawing.Size(800, 313);
            this.Name = "FormManageUFCD";
            this.Resizable = false;
            this.Text = "Nova UFCD";
            this.Load += new System.EventHandler(this.FormManageUFCD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCredits)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox metroTextBoxDescription;
        private MetroFramework.Controls.MetroTextBox metroTextBoxName;
        private MetroFramework.Controls.MetroTextBox metroTextBoxID;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.NumericUpDown numericUpDownDuration;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private System.Windows.Forms.NumericUpDown numericUpDownCredits;
        private MetroFramework.Controls.MetroButton metroButtonCancel;
        private MetroFramework.Controls.MetroButton metroButtonSave;
    }
}