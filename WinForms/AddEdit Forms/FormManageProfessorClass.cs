﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MetroFramework.Forms;

namespace WinForms.AddEdit_Forms
{
    using Database.Common;
    using Database.TableModels;
    using Database;

    public partial class FormManageProfessorClass : MetroForm
    {
        public bool EditMode { get; set; }

        public Class Team { get; set; }
        public UFCD UFCD { get; set; }

        public Professor_Class OldPClass { get; set; }
        public Professor_Class NewPClass { get; set; }

        public List<Professor> AvailableProfessors { get; set; }

        /// <summary>
        /// Edit an existing association
        /// </summary>
        /// <param name="pclass">Existing association</param>
        public FormManageProfessorClass(Professor_Class pclass) : this()
        {
            this.OldPClass = pclass;
            this.UFCD = pclass.UFCD;

            this.AvailableProfessors = Database.Professor_Class.GetAvailableProfessors(this.OldPClass.UFCD);

            Core<Professor_Class>.CopyValues(this.OldPClass, this.NewPClass);

            this.EditMode = true;

            PopulateView();
        }

        /// <summary>
        /// Insert a new association
        /// </summary>
        /// <param name="team">Class to which you want toa ssociate the professor</param>
        /// <param name="ufcd">UFCD to which you want to associate the professor</param>
        public FormManageProfessorClass(Class team, UFCD ufcd) : this()
        {
            this.Team = team;
            this.UFCD = ufcd;

            this.AvailableProfessors = Database.Professor_Class.GetAvailableProfessors(this.UFCD);

            this.EditMode = false;

            PopulateView();
        }

        /// <summary>
        /// Common constructor
        /// </summary>
        private FormManageProfessorClass()
        {
            InitializeComponent();

            this.NewPClass = new Professor_Class();
        }

        /// <summary>
        /// Populates view based on filter in textbox
        /// </summary>
        private void PopulateView()
        {
            listBoxList.DataSource = null;
            listBoxList.Items.Clear();

            List<Professor> listAvailableProfessors = this.AvailableProfessors;

            string[] keywords = metroTextBoxFilter.Text.ToLower().Split(' ');

            foreach(Professor professor in Database.Professor_Class.GetAvailableProfessors(this.UFCD))
            {
                if (keywords.Length > 0)
                {
                    bool addItem = false;

                    foreach (string keyword in keywords)
                    {
                        if (professor.BaseInfo.ToLower().Contains(keyword))
                        {
                            addItem = true;
                            break;
                        }
                    }

                    if (addItem)
                    {
                        listBoxList.Items.Add(professor);
                    }
                    continue;
                }
                listBoxList.Items.Add(professor);
            }
            listBoxList.DisplayMember = $"Name";
        }

        /// <summary>
        /// On writing filter text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroTextBoxFilter_TextChanged(object sender, EventArgs e)
        {
            PopulateView();
            if (this.listBoxList.Items.Count > 0)
            {
                this.listBoxList.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// On pressing Enter, assume selected value as the entry to select
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormManageProfessorClass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (this.listBoxList.SelectedItem != null)
                {
                    EnterData((Professor)(this.listBoxList.SelectedItem));

                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }

        /// <summary>
        /// On double click the entry, assume double clicked value as the entry to select
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = this.listBoxList.IndexFromPoint(e.Location);
            if (index > -1)
            {
                EnterData((Professor)(this.listBoxList.Items[index]));

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        /// <summary>
        /// Confirm data entry
        /// </summary>
        /// <param name="professor"></param>
        private void EnterData(Professor professor)
        {
            if (this.EditMode)
            {
                this.NewPClass.Professor = professor;

                Database.Professor_Class.Update(this.OldPClass, this.NewPClass);
            }
            else
            {
                Database.Professor_Class.Add(
                    new Professor_Class()
                    {
                        ID = Database.Professor_Class.GetNewID(),
                        Professor = professor,
                        Team = this.Team,
                        UFCD = this.UFCD
                    }
                );
            }
        }
    }
}
