﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Database.TableModels;
using MetroFramework.Forms;


namespace WinForms.AddEdit_Forms
{
    using Database;
    using Database.Common;

    public partial class FormManageCourse : MetroForm
    {
        public Course NewCourse { get; set; }
        public Course OldCourse { get; set; }
        EventHandler _FunctionToPerform;

        /// <summary>
        /// Edit Course
        /// </summary>
        /// <param name="course">Course to Edit</param>
        public FormManageCourse(Course course) : this()
        {
            this.OldCourse = course;

            Core<Course>.CopyValues(this.OldCourse, this.NewCourse);

            this.Text = $"{this.OldCourse.BaseInfo}";

            PopulateView();

            _FunctionToPerform = this.EditButton;

            metroTextBoxID.ReadOnly = true;
        }

        /// <summary>
        /// Inserts a new course
        /// </summary>
        public FormManageCourse()
        {
            InitializeComponent();

            this.NewCourse = new Course();

            this.metroTextBoxID.Text = $"{Database.Courses.GetNewID()}";

            _FunctionToPerform = this.InsertButton;
        }

        private void FormManageCourse_Load(object sender, EventArgs e)
        {
            this.Activate();
            
            this.metroButtonSave.Click += _FunctionToPerform;
        }

        /// <summary>
        /// Populates controls with the values of the entry to edit
        /// </summary>
        private void PopulateView()
        {
            metroTextBoxID.Text = this.NewCourse.ID.ToString();
            metroTextBoxDescription.Text = this.NewCourse.Description;
            metroTextBoxName.Text = this.NewCourse.Name;

            InitLists();
        }

        private void InitLists()
        {
            
        }

        /// <summary>
        /// Loads data from controls into the temporary entry in memory
        /// </summary>
        private void LoadData()
        {
            this.NewCourse.ID = Convert.ToUInt32(metroTextBoxID.Text);
            this.NewCourse.Description = metroTextBoxDescription.Text;
            this.NewCourse.Name = metroTextBoxName.Text;
        }

        private void metroButtonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        // --- Save Button

        /// <summary>
        /// Insert entry button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InsertButton(object sender, EventArgs e)
        {
            if (!ValidateForm())
            {
                return;
            }

            LoadData();

            switch (Database.Courses.Add(this.NewCourse))
            {
                case 0:
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                } break;
            }
        }

        /// <summary>
        /// Edit entry button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditButton(object sender, EventArgs e)
        {
            if (!ValidateForm())
            {
                return;
            }

            LoadData();

            switch (Database.Courses.Update(this.OldCourse, this.NewCourse))
            {
                case 0:
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                } break;
            }
        }

        // --- End Save Buttons

        /// <summary>
        /// Checks if everything in the form is valid according to the required entry parameters
        /// </summary>
        /// <returns>
        /// True if everything checks up
        /// False if a value is incorrect
        /// </returns>
        private bool ValidateForm()
        {
            if (string.IsNullOrEmpty(metroTextBoxID.Text))
            {
                MessageBox.Show($"O curso precisa de ter um ID! Verifique o em http://www.anqep.gov.pt/default.aspx");
                return false;
            }
            uint IDtemp;
            if (!uint.TryParse(metroTextBoxID.Text, out IDtemp))
            {
                MessageBox.Show($"É necessário que o ID seja numérico! Verifique o em http://www.anqep.gov.pt/default.aspx");
                return false;
            }
            if (string.IsNullOrEmpty(metroTextBoxName.Text))
            {
                MessageBox.Show($"O curso deve possuir um nome.");
                return false;
            }
            return true;
        }
    }
}
