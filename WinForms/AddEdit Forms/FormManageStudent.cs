﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Database.TableModels;
using MetroFramework.Forms;

namespace WinForms.AddEdit_Forms
{
    using Database;
    using Database.Common;

    public partial class FormManageStudent : MetroForm
    {
        public Class ParentTeam { get; set; }

        public Student OldStudent { get; set; }
        public Student NewStudent { get; set; }

        EventHandler _FunctionToPerform;

        /// <summary>
        /// Edit existing student
        /// </summary>
        /// <param name="ParentTeam">Class to which the student belongs</param>
        /// <param name="student">Student to edit</param>
        public FormManageStudent(Class ParentTeam, Student student) : this(ParentTeam)
        {
            this.OldStudent = student;

            Core<Student>.CopyValues(this.OldStudent, this.NewStudent);

            this.ucProfile.Profile = this.NewStudent;
            this.ucProfile.PopulateView();

            this.Text = $"{student.BaseInfo}";

            _FunctionToPerform = EditStudent;
        }
        /// <summary>
        /// Insert new student
        /// </summary>
        /// <param name="ParentTeam"></param>
        public FormManageStudent(Class ParentTeam)
        {
            this.ParentTeam = ParentTeam;
            this.NewStudent = new Student();

            InitializeComponent();

            _FunctionToPerform = AddStudent;
        }

        private void FormManageStudent_Load(object sender, EventArgs e)
        {
            if (this.OldStudent == null)
            {
                this.ucProfile.PopulateInsertView();
                this.ucProfile.SetIDFieldValue(Database.Students.GetNewID());
            }

            metroButtonSave.Click += _FunctionToPerform;

            this.Activate();
        }

        /// <summary>
        /// Cancel button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroButtonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Loads data from controls into the temporary entry in memory
        /// </summary>
        private void LoadData(Profile Profile)
        {
            this.NewStudent.ID = Profile.ID;
            this.NewStudent.Name = Profile.Name;
            this.NewStudent.ID_Document_EmissionDate = Profile.ID_Document_EmissionDate;
            this.NewStudent.ID_Document_ExpirationDate = Profile.ID_Document_ExpirationDate;
            this.NewStudent.ID_Document_Number = Profile.ID_Document_Number;
            this.NewStudent.ID_Document_Type = Profile.ID_Document_Type;
            this.NewStudent.Nationality = Profile.Nationality;
            this.NewStudent.NIF = Profile.NIF;
            this.NewStudent.BirthDate = Profile.BirthDate;
            this.NewStudent.Birthplace = Profile.Birthplace;
            this.NewStudent.Citizenship = Profile.Citizenship;
            this.NewStudent.Gender = Profile.Gender;
        }

        // --- Save Button

        /// <summary>
        /// Insert entry button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddStudent(object sender, EventArgs e)
        {
            Profile loadedProfile = this.ucProfile.LoadData();

            if (loadedProfile == null)
            {
                return;
            }

            LoadData(loadedProfile);
            
            switch (this.ParentTeam.Students.Add(NewStudent))
            {
                case 0:
                {
                    MessageBox.Show($"Aluno adicionado com sucesso!");
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                } break;
                case -1:
                {
                    MessageBox.Show($"Já existe um aluno com esse NIF!");
                    return;
                } break;
            }
        }

        /// <summary>
        /// Edit entry button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditStudent(object sender, EventArgs e)
        {
            Profile loadedProfile = this.ucProfile.LoadData();

            if (loadedProfile == null)
            {
                return;
            }

            LoadData(loadedProfile);

            switch (this.ParentTeam.Students.Update(OldStudent, NewStudent))
            {
                case 0:
                {
                    MessageBox.Show($"Aluno editado com sucesso!");
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                } break;
            }
        }

        // --- End Save Button
    }
}
