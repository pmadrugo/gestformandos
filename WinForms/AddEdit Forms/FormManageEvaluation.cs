﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Database.TableModels;
using MetroFramework.Forms;

namespace WinForms.AddEdit_Forms
{
    using Database.Common;
    using Database;

    public partial class FormManageEvaluation : MetroForm
    {
        public Student Student { get; set; }
        public UFCD UFCD { get; set; }

        public Evaluation OldEvaluation { get; set; }
        public Evaluation NewEvaluation { get; set; }

        /// <summary>
        /// Insert new evaluation
        /// </summary>
        /// <param name="student">Student to which the evaluation belongs</param>
        /// <param name="ufcd">UFCD to which the evaluation belongs</param>
        public FormManageEvaluation(Student student, UFCD ufcd)
        {
            InitializeComponent();

            this.NewEvaluation = new Evaluation();

            this.NewEvaluation.ID = Database.Evaluations.GetNewID();
            this.NewEvaluation.IDStudent = student.ID;
            this.NewEvaluation.IDUFCD = ufcd.ID;

            this.Student = student;
            this.UFCD = ufcd;

            this.metroButtonSave.Click += new EventHandler(AddEvaluation);
        }

        /// <summary>
        /// Edit existing evaluation
        /// </summary>
        /// <param name="evaluation">Evaluation to edit</param>
        public FormManageEvaluation(Evaluation evaluation)
        {
            InitializeComponent();

            this.OldEvaluation = evaluation;
            this.NewEvaluation = new Evaluation();

            Core<Evaluation>.CopyValues(this.OldEvaluation, this.NewEvaluation);

            this.Text = $"Rever avaliação";

            PopulateView();

            this.metroButtonSave.Click += new EventHandler(EditEvaluation);
        }

        private void FormManageEvaluation_Load(object sender, EventArgs e)
        {
            this.Activate();
        }

        /// <summary>
        /// Populates controls with the values of the entry to edit
        /// </summary>
        private void PopulateView()
        {
            metroTextBoxDescription.Text = this.NewEvaluation.Description;
            numericUpDownGrade.Value = Convert.ToDecimal(this.NewEvaluation.Grade);
        }

        /// <summary>
        /// Loads data from controls into the temporary entry in memory
        /// </summary>
        private void LoadData()
        {
            this.NewEvaluation.Description = this.metroTextBoxDescription.Text;
            this.NewEvaluation.Grade = Convert.ToDouble(numericUpDownGrade.Value);
        }

        // --- Save Button

        /// <summary>
        /// Insert entry button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddEvaluation(object sender, EventArgs e)
        {
            LoadData();

            switch (Database.Evaluations.Add(this.NewEvaluation))
            {
                case 0:
                    {
                        this.Student.Evaluations.Add(this.NewEvaluation);
                        this.UFCD.Evaluations.Add(this.NewEvaluation);

                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    } break;
            }
        }

        /// <summary>
        /// Edit entry button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditEvaluation(object sender, EventArgs e)
        {
            LoadData();

            switch (Database.Evaluations.Update(this.OldEvaluation, this.NewEvaluation))
            {
                case 0:
                    {
                        this.DialogResult = DialogResult.OK;
                    } break;
            }
        }

        /// <summary>
        /// Cancel button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void metroButtonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
