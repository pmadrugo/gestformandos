﻿namespace WinForms
{
    partial class FormLoading
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroProgSpinner = new MetroFramework.Controls.MetroProgressSpinner();
            this.lblLoading = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // metroProgSpinner
            // 
            this.metroProgSpinner.Location = new System.Drawing.Point(333, 219);
            this.metroProgSpinner.Maximum = 100;
            this.metroProgSpinner.Name = "metroProgSpinner";
            this.metroProgSpinner.Size = new System.Drawing.Size(103, 107);
            this.metroProgSpinner.TabIndex = 0;
            // 
            // lblLoading
            // 
            this.lblLoading.Location = new System.Drawing.Point(23, 329);
            this.lblLoading.Name = "lblLoading";
            this.lblLoading.Size = new System.Drawing.Size(754, 101);
            this.lblLoading.TabIndex = 1;
            this.lblLoading.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // FormLoading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ControlBox = false;
            this.Controls.Add(this.lblLoading);
            this.Controls.Add(this.metroProgSpinner);
            this.MaximizeBox = false;
            this.Name = "FormLoading";
            this.Resizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "A carregar ficheiros...";
            this.Load += new System.EventHandler(this.FormLoading_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroProgressSpinner metroProgSpinner;
        private MetroFramework.Controls.MetroLabel lblLoading;
    }
}

