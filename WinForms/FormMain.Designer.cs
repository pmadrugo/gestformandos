﻿namespace WinForms
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.metroTabControl = new MetroFramework.Controls.MetroTabControl();
            this.metroTabCursos = new MetroFramework.Controls.MetroTabPage();
            this.tableLayoutCourses = new System.Windows.Forms.TableLayoutPanel();
            this.metroTabProfessors = new MetroFramework.Controls.MetroTabPage();
            this.splitContainerProfessorTable_Buttons = new System.Windows.Forms.SplitContainer();
            this.dataGridViewProfessors = new System.Windows.Forms.DataGridView();
            this.splitContainerProfessorAddEdit_Delete = new System.Windows.Forms.SplitContainer();
            this.splitContainerProfessorAdd_Edit = new System.Windows.Forms.SplitContainer();
            this.metroTileAddProfessor = new MetroFramework.Controls.MetroTile();
            this.metroTileEditProfessor = new MetroFramework.Controls.MetroTile();
            this.metroTileDeleteProfessor = new MetroFramework.Controls.MetroTile();
            this.metroTabUFCDs = new MetroFramework.Controls.MetroTabPage();
            this.splitContainerUFCDTable_Buttons = new System.Windows.Forms.SplitContainer();
            this.dataGridViewUFCDs = new System.Windows.Forms.DataGridView();
            this.splitContainerUFCDAddEdit_Delete = new System.Windows.Forms.SplitContainer();
            this.splitContainerUFCDAdd_Edit = new System.Windows.Forms.SplitContainer();
            this.metroTileAddUFCD = new MetroFramework.Controls.MetroTile();
            this.metroTileEditUFCD = new MetroFramework.Controls.MetroTile();
            this.metroTileDeleteUFCD = new MetroFramework.Controls.MetroTile();
            this.metroTileAddCourse = new MetroFramework.Controls.MetroTile();
            this.contextMenuStripCourse = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemEditCourse = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemRemoveCourse = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAbout = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.metroTabControl.SuspendLayout();
            this.metroTabCursos.SuspendLayout();
            this.metroTabProfessors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerProfessorTable_Buttons)).BeginInit();
            this.splitContainerProfessorTable_Buttons.Panel1.SuspendLayout();
            this.splitContainerProfessorTable_Buttons.Panel2.SuspendLayout();
            this.splitContainerProfessorTable_Buttons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProfessors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerProfessorAddEdit_Delete)).BeginInit();
            this.splitContainerProfessorAddEdit_Delete.Panel1.SuspendLayout();
            this.splitContainerProfessorAddEdit_Delete.Panel2.SuspendLayout();
            this.splitContainerProfessorAddEdit_Delete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerProfessorAdd_Edit)).BeginInit();
            this.splitContainerProfessorAdd_Edit.Panel1.SuspendLayout();
            this.splitContainerProfessorAdd_Edit.Panel2.SuspendLayout();
            this.splitContainerProfessorAdd_Edit.SuspendLayout();
            this.metroTabUFCDs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerUFCDTable_Buttons)).BeginInit();
            this.splitContainerUFCDTable_Buttons.Panel1.SuspendLayout();
            this.splitContainerUFCDTable_Buttons.Panel2.SuspendLayout();
            this.splitContainerUFCDTable_Buttons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUFCDs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerUFCDAddEdit_Delete)).BeginInit();
            this.splitContainerUFCDAddEdit_Delete.Panel1.SuspendLayout();
            this.splitContainerUFCDAddEdit_Delete.Panel2.SuspendLayout();
            this.splitContainerUFCDAddEdit_Delete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerUFCDAdd_Edit)).BeginInit();
            this.splitContainerUFCDAdd_Edit.Panel1.SuspendLayout();
            this.splitContainerUFCDAdd_Edit.Panel2.SuspendLayout();
            this.splitContainerUFCDAdd_Edit.SuspendLayout();
            this.contextMenuStripCourse.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTabControl
            // 
            this.metroTabControl.Controls.Add(this.metroTabCursos);
            this.metroTabControl.Controls.Add(this.metroTabProfessors);
            this.metroTabControl.Controls.Add(this.metroTabUFCDs);
            this.metroTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabControl.ItemSize = new System.Drawing.Size(70, 31);
            this.metroTabControl.Location = new System.Drawing.Point(20, 60);
            this.metroTabControl.Name = "metroTabControl";
            this.metroTabControl.SelectedIndex = 0;
            this.metroTabControl.Size = new System.Drawing.Size(760, 370);
            this.metroTabControl.TabIndex = 0;
            this.metroTabControl.SelectedIndexChanged += new System.EventHandler(this.metroTabControl_SelectedIndexChanged);
            // 
            // metroTabCursos
            // 
            this.metroTabCursos.Controls.Add(this.tableLayoutCourses);
            this.metroTabCursos.HorizontalScrollbarBarColor = true;
            this.metroTabCursos.Location = new System.Drawing.Point(4, 35);
            this.metroTabCursos.Name = "metroTabCursos";
            this.metroTabCursos.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.metroTabCursos.Size = new System.Drawing.Size(752, 331);
            this.metroTabCursos.TabIndex = 0;
            this.metroTabCursos.Text = "Cursos";
            this.metroTabCursos.VerticalScrollbarBarColor = true;
            // 
            // tableLayoutCourses
            // 
            this.tableLayoutCourses.AutoScroll = true;
            this.tableLayoutCourses.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutCourses.ColumnCount = 1;
            this.tableLayoutCourses.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutCourses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutCourses.Location = new System.Drawing.Point(0, 5);
            this.tableLayoutCourses.Name = "tableLayoutCourses";
            this.tableLayoutCourses.RowCount = 1;
            this.tableLayoutCourses.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 370F));
            this.tableLayoutCourses.Size = new System.Drawing.Size(752, 326);
            this.tableLayoutCourses.TabIndex = 2;
            // 
            // metroTabProfessors
            // 
            this.metroTabProfessors.Controls.Add(this.splitContainerProfessorTable_Buttons);
            this.metroTabProfessors.HorizontalScrollbarBarColor = true;
            this.metroTabProfessors.Location = new System.Drawing.Point(4, 35);
            this.metroTabProfessors.Name = "metroTabProfessors";
            this.metroTabProfessors.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.metroTabProfessors.Size = new System.Drawing.Size(752, 331);
            this.metroTabProfessors.TabIndex = 1;
            this.metroTabProfessors.Text = "Formadores";
            this.metroTabProfessors.VerticalScrollbarBarColor = true;
            // 
            // splitContainerProfessorTable_Buttons
            // 
            this.splitContainerProfessorTable_Buttons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerProfessorTable_Buttons.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainerProfessorTable_Buttons.IsSplitterFixed = true;
            this.splitContainerProfessorTable_Buttons.Location = new System.Drawing.Point(0, 5);
            this.splitContainerProfessorTable_Buttons.Name = "splitContainerProfessorTable_Buttons";
            // 
            // splitContainerProfessorTable_Buttons.Panel1
            // 
            this.splitContainerProfessorTable_Buttons.Panel1.Controls.Add(this.dataGridViewProfessors);
            // 
            // splitContainerProfessorTable_Buttons.Panel2
            // 
            this.splitContainerProfessorTable_Buttons.Panel2.Controls.Add(this.splitContainerProfessorAddEdit_Delete);
            this.splitContainerProfessorTable_Buttons.Size = new System.Drawing.Size(752, 326);
            this.splitContainerProfessorTable_Buttons.SplitterDistance = 677;
            this.splitContainerProfessorTable_Buttons.TabIndex = 3;
            // 
            // dataGridViewProfessors
            // 
            this.dataGridViewProfessors.AllowUserToAddRows = false;
            this.dataGridViewProfessors.AllowUserToDeleteRows = false;
            this.dataGridViewProfessors.AllowUserToResizeColumns = false;
            this.dataGridViewProfessors.AllowUserToResizeRows = false;
            this.dataGridViewProfessors.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewProfessors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProfessors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewProfessors.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewProfessors.MultiSelect = false;
            this.dataGridViewProfessors.Name = "dataGridViewProfessors";
            this.dataGridViewProfessors.ReadOnly = true;
            this.dataGridViewProfessors.RowHeadersVisible = false;
            this.dataGridViewProfessors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewProfessors.Size = new System.Drawing.Size(677, 326);
            this.dataGridViewProfessors.TabIndex = 0;
            // 
            // splitContainerProfessorAddEdit_Delete
            // 
            this.splitContainerProfessorAddEdit_Delete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerProfessorAddEdit_Delete.Location = new System.Drawing.Point(0, 0);
            this.splitContainerProfessorAddEdit_Delete.Name = "splitContainerProfessorAddEdit_Delete";
            this.splitContainerProfessorAddEdit_Delete.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerProfessorAddEdit_Delete.Panel1
            // 
            this.splitContainerProfessorAddEdit_Delete.Panel1.Controls.Add(this.splitContainerProfessorAdd_Edit);
            // 
            // splitContainerProfessorAddEdit_Delete.Panel2
            // 
            this.splitContainerProfessorAddEdit_Delete.Panel2.Controls.Add(this.metroTileDeleteProfessor);
            this.splitContainerProfessorAddEdit_Delete.Size = new System.Drawing.Size(71, 326);
            this.splitContainerProfessorAddEdit_Delete.SplitterDistance = 214;
            this.splitContainerProfessorAddEdit_Delete.TabIndex = 0;
            // 
            // splitContainerProfessorAdd_Edit
            // 
            this.splitContainerProfessorAdd_Edit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerProfessorAdd_Edit.Location = new System.Drawing.Point(0, 0);
            this.splitContainerProfessorAdd_Edit.Name = "splitContainerProfessorAdd_Edit";
            this.splitContainerProfessorAdd_Edit.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerProfessorAdd_Edit.Panel1
            // 
            this.splitContainerProfessorAdd_Edit.Panel1.Controls.Add(this.metroTileAddProfessor);
            // 
            // splitContainerProfessorAdd_Edit.Panel2
            // 
            this.splitContainerProfessorAdd_Edit.Panel2.Controls.Add(this.metroTileEditProfessor);
            this.splitContainerProfessorAdd_Edit.Size = new System.Drawing.Size(71, 214);
            this.splitContainerProfessorAdd_Edit.SplitterDistance = 99;
            this.splitContainerProfessorAdd_Edit.TabIndex = 0;
            // 
            // metroTileAddProfessor
            // 
            this.metroTileAddProfessor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTileAddProfessor.Location = new System.Drawing.Point(0, 0);
            this.metroTileAddProfessor.Name = "metroTileAddProfessor";
            this.metroTileAddProfessor.Size = new System.Drawing.Size(71, 99);
            this.metroTileAddProfessor.TabIndex = 0;
            this.metroTileAddProfessor.Text = "Adicionar";
            this.metroTileAddProfessor.Click += new System.EventHandler(this.metroTileAddProfessor_Click);
            // 
            // metroTileEditProfessor
            // 
            this.metroTileEditProfessor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTileEditProfessor.Location = new System.Drawing.Point(0, 0);
            this.metroTileEditProfessor.Name = "metroTileEditProfessor";
            this.metroTileEditProfessor.Size = new System.Drawing.Size(71, 111);
            this.metroTileEditProfessor.TabIndex = 0;
            this.metroTileEditProfessor.Text = "Editar";
            this.metroTileEditProfessor.Click += new System.EventHandler(this.metroTileEditProfessor_Click);
            // 
            // metroTileDeleteProfessor
            // 
            this.metroTileDeleteProfessor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTileDeleteProfessor.Location = new System.Drawing.Point(0, 0);
            this.metroTileDeleteProfessor.Name = "metroTileDeleteProfessor";
            this.metroTileDeleteProfessor.Size = new System.Drawing.Size(71, 108);
            this.metroTileDeleteProfessor.TabIndex = 0;
            this.metroTileDeleteProfessor.Text = "Remover";
            this.metroTileDeleteProfessor.Click += new System.EventHandler(this.metroTileDeleteProfessor_Click);
            // 
            // metroTabUFCDs
            // 
            this.metroTabUFCDs.Controls.Add(this.splitContainerUFCDTable_Buttons);
            this.metroTabUFCDs.HorizontalScrollbarBarColor = true;
            this.metroTabUFCDs.Location = new System.Drawing.Point(4, 35);
            this.metroTabUFCDs.Name = "metroTabUFCDs";
            this.metroTabUFCDs.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.metroTabUFCDs.Size = new System.Drawing.Size(752, 331);
            this.metroTabUFCDs.TabIndex = 2;
            this.metroTabUFCDs.Text = "UFCDs";
            this.metroTabUFCDs.VerticalScrollbarBarColor = true;
            // 
            // splitContainerUFCDTable_Buttons
            // 
            this.splitContainerUFCDTable_Buttons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerUFCDTable_Buttons.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainerUFCDTable_Buttons.IsSplitterFixed = true;
            this.splitContainerUFCDTable_Buttons.Location = new System.Drawing.Point(0, 5);
            this.splitContainerUFCDTable_Buttons.Name = "splitContainerUFCDTable_Buttons";
            // 
            // splitContainerUFCDTable_Buttons.Panel1
            // 
            this.splitContainerUFCDTable_Buttons.Panel1.Controls.Add(this.dataGridViewUFCDs);
            // 
            // splitContainerUFCDTable_Buttons.Panel2
            // 
            this.splitContainerUFCDTable_Buttons.Panel2.Controls.Add(this.splitContainerUFCDAddEdit_Delete);
            this.splitContainerUFCDTable_Buttons.Size = new System.Drawing.Size(752, 326);
            this.splitContainerUFCDTable_Buttons.SplitterDistance = 677;
            this.splitContainerUFCDTable_Buttons.TabIndex = 2;
            // 
            // dataGridViewUFCDs
            // 
            this.dataGridViewUFCDs.AllowUserToAddRows = false;
            this.dataGridViewUFCDs.AllowUserToDeleteRows = false;
            this.dataGridViewUFCDs.AllowUserToResizeColumns = false;
            this.dataGridViewUFCDs.AllowUserToResizeRows = false;
            this.dataGridViewUFCDs.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewUFCDs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUFCDs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewUFCDs.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewUFCDs.MultiSelect = false;
            this.dataGridViewUFCDs.Name = "dataGridViewUFCDs";
            this.dataGridViewUFCDs.ReadOnly = true;
            this.dataGridViewUFCDs.RowHeadersVisible = false;
            this.dataGridViewUFCDs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewUFCDs.Size = new System.Drawing.Size(677, 326);
            this.dataGridViewUFCDs.TabIndex = 0;
            // 
            // splitContainerUFCDAddEdit_Delete
            // 
            this.splitContainerUFCDAddEdit_Delete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerUFCDAddEdit_Delete.Location = new System.Drawing.Point(0, 0);
            this.splitContainerUFCDAddEdit_Delete.Name = "splitContainerUFCDAddEdit_Delete";
            this.splitContainerUFCDAddEdit_Delete.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerUFCDAddEdit_Delete.Panel1
            // 
            this.splitContainerUFCDAddEdit_Delete.Panel1.Controls.Add(this.splitContainerUFCDAdd_Edit);
            // 
            // splitContainerUFCDAddEdit_Delete.Panel2
            // 
            this.splitContainerUFCDAddEdit_Delete.Panel2.Controls.Add(this.metroTileDeleteUFCD);
            this.splitContainerUFCDAddEdit_Delete.Size = new System.Drawing.Size(71, 326);
            this.splitContainerUFCDAddEdit_Delete.SplitterDistance = 214;
            this.splitContainerUFCDAddEdit_Delete.TabIndex = 0;
            // 
            // splitContainerUFCDAdd_Edit
            // 
            this.splitContainerUFCDAdd_Edit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerUFCDAdd_Edit.Location = new System.Drawing.Point(0, 0);
            this.splitContainerUFCDAdd_Edit.Name = "splitContainerUFCDAdd_Edit";
            this.splitContainerUFCDAdd_Edit.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerUFCDAdd_Edit.Panel1
            // 
            this.splitContainerUFCDAdd_Edit.Panel1.Controls.Add(this.metroTileAddUFCD);
            // 
            // splitContainerUFCDAdd_Edit.Panel2
            // 
            this.splitContainerUFCDAdd_Edit.Panel2.Controls.Add(this.metroTileEditUFCD);
            this.splitContainerUFCDAdd_Edit.Size = new System.Drawing.Size(71, 214);
            this.splitContainerUFCDAdd_Edit.SplitterDistance = 99;
            this.splitContainerUFCDAdd_Edit.TabIndex = 0;
            // 
            // metroTileAddUFCD
            // 
            this.metroTileAddUFCD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTileAddUFCD.Location = new System.Drawing.Point(0, 0);
            this.metroTileAddUFCD.Name = "metroTileAddUFCD";
            this.metroTileAddUFCD.Size = new System.Drawing.Size(71, 99);
            this.metroTileAddUFCD.TabIndex = 0;
            this.metroTileAddUFCD.Text = "Adicionar";
            this.metroTileAddUFCD.Click += new System.EventHandler(this.AddUFCD);
            // 
            // metroTileEditUFCD
            // 
            this.metroTileEditUFCD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTileEditUFCD.Location = new System.Drawing.Point(0, 0);
            this.metroTileEditUFCD.Name = "metroTileEditUFCD";
            this.metroTileEditUFCD.Size = new System.Drawing.Size(71, 111);
            this.metroTileEditUFCD.TabIndex = 0;
            this.metroTileEditUFCD.Text = "Editar";
            this.metroTileEditUFCD.Click += new System.EventHandler(this.EditUFCD);
            // 
            // metroTileDeleteUFCD
            // 
            this.metroTileDeleteUFCD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTileDeleteUFCD.Location = new System.Drawing.Point(0, 0);
            this.metroTileDeleteUFCD.Name = "metroTileDeleteUFCD";
            this.metroTileDeleteUFCD.Size = new System.Drawing.Size(71, 108);
            this.metroTileDeleteUFCD.TabIndex = 0;
            this.metroTileDeleteUFCD.Text = "Remover";
            this.metroTileDeleteUFCD.Click += new System.EventHandler(this.DeleteUFCD);
            // 
            // metroTileAddCourse
            // 
            this.metroTileAddCourse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroTileAddCourse.Location = new System.Drawing.Point(703, 60);
            this.metroTileAddCourse.Name = "metroTileAddCourse";
            this.metroTileAddCourse.Size = new System.Drawing.Size(70, 29);
            this.metroTileAddCourse.TabIndex = 1;
            this.metroTileAddCourse.Text = "+";
            this.metroTileAddCourse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTileAddCourse.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTileAddCourse.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTileAddCourse.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.metroTileAddCourse.Click += new System.EventHandler(this.metroTileAddCourse_Click);
            // 
            // contextMenuStripCourse
            // 
            this.contextMenuStripCourse.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemEditCourse,
            this.toolStripSeparator2,
            this.toolStripMenuItemRemoveCourse});
            this.contextMenuStripCourse.Name = "contextMenuStripUFCD";
            this.contextMenuStripCourse.Size = new System.Drawing.Size(122, 54);
            // 
            // toolStripMenuItemEditCourse
            // 
            this.toolStripMenuItemEditCourse.Name = "toolStripMenuItemEditCourse";
            this.toolStripMenuItemEditCourse.Size = new System.Drawing.Size(121, 22);
            this.toolStripMenuItemEditCourse.Text = "Editar";
            this.toolStripMenuItemEditCourse.Click += new System.EventHandler(this.toolStripMenuItemEditCourse_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(118, 6);
            // 
            // toolStripMenuItemRemoveCourse
            // 
            this.toolStripMenuItemRemoveCourse.Name = "toolStripMenuItemRemoveCourse";
            this.toolStripMenuItemRemoveCourse.Size = new System.Drawing.Size(121, 22);
            this.toolStripMenuItemRemoveCourse.Text = "Remover";
            this.toolStripMenuItemRemoveCourse.Click += new System.EventHandler(this.toolStripMenuItemRemoveCourse_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.BackColor = System.Drawing.Color.Transparent;
            this.btnAbout.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAbout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbout.Location = new System.Drawing.Point(695, 5);
            this.btnAbout.Margin = new System.Windows.Forms.Padding(0);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(26, 21);
            this.btnAbout.TabIndex = 2;
            this.btnAbout.Text = "?";
            this.btnAbout.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAbout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnAbout.UseVisualStyleBackColor = false;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnAbout);
            this.Controls.Add(this.metroTileAddCourse);
            this.Controls.Add(this.metroTabControl);
            this.Name = "FormMain";
            this.Text = "gestFormandos";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.metroTabControl.ResumeLayout(false);
            this.metroTabCursos.ResumeLayout(false);
            this.metroTabProfessors.ResumeLayout(false);
            this.splitContainerProfessorTable_Buttons.Panel1.ResumeLayout(false);
            this.splitContainerProfessorTable_Buttons.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerProfessorTable_Buttons)).EndInit();
            this.splitContainerProfessorTable_Buttons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProfessors)).EndInit();
            this.splitContainerProfessorAddEdit_Delete.Panel1.ResumeLayout(false);
            this.splitContainerProfessorAddEdit_Delete.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerProfessorAddEdit_Delete)).EndInit();
            this.splitContainerProfessorAddEdit_Delete.ResumeLayout(false);
            this.splitContainerProfessorAdd_Edit.Panel1.ResumeLayout(false);
            this.splitContainerProfessorAdd_Edit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerProfessorAdd_Edit)).EndInit();
            this.splitContainerProfessorAdd_Edit.ResumeLayout(false);
            this.metroTabUFCDs.ResumeLayout(false);
            this.splitContainerUFCDTable_Buttons.Panel1.ResumeLayout(false);
            this.splitContainerUFCDTable_Buttons.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerUFCDTable_Buttons)).EndInit();
            this.splitContainerUFCDTable_Buttons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUFCDs)).EndInit();
            this.splitContainerUFCDAddEdit_Delete.Panel1.ResumeLayout(false);
            this.splitContainerUFCDAddEdit_Delete.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerUFCDAddEdit_Delete)).EndInit();
            this.splitContainerUFCDAddEdit_Delete.ResumeLayout(false);
            this.splitContainerUFCDAdd_Edit.Panel1.ResumeLayout(false);
            this.splitContainerUFCDAdd_Edit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerUFCDAdd_Edit)).EndInit();
            this.splitContainerUFCDAdd_Edit.ResumeLayout(false);
            this.contextMenuStripCourse.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl;
        private MetroFramework.Controls.MetroTabPage metroTabCursos;
        private System.Windows.Forms.TableLayoutPanel tableLayoutCourses;
        private MetroFramework.Controls.MetroTabPage metroTabUFCDs;
        private MetroFramework.Controls.MetroTabPage metroTabProfessors;
        private System.Windows.Forms.SplitContainer splitContainerUFCDTable_Buttons;
        private System.Windows.Forms.DataGridView dataGridViewUFCDs;
        private System.Windows.Forms.SplitContainer splitContainerUFCDAddEdit_Delete;
        private System.Windows.Forms.SplitContainer splitContainerUFCDAdd_Edit;
        private MetroFramework.Controls.MetroTile metroTileAddUFCD;
        private MetroFramework.Controls.MetroTile metroTileEditUFCD;
        private MetroFramework.Controls.MetroTile metroTileDeleteUFCD;
        private MetroFramework.Controls.MetroTile metroTileAddCourse;
        private System.Windows.Forms.SplitContainer splitContainerProfessorTable_Buttons;
        private System.Windows.Forms.DataGridView dataGridViewProfessors;
        private System.Windows.Forms.SplitContainer splitContainerProfessorAddEdit_Delete;
        private System.Windows.Forms.SplitContainer splitContainerProfessorAdd_Edit;
        private MetroFramework.Controls.MetroTile metroTileAddProfessor;
        private MetroFramework.Controls.MetroTile metroTileEditProfessor;
        private MetroFramework.Controls.MetroTile metroTileDeleteProfessor;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripCourse;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemEditCourse;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRemoveCourse;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.ToolTip toolTip;
    }
}