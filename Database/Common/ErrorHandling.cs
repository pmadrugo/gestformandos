﻿namespace Database.Common
{
    using System;
    using System.IO;

    static class ErrorHandling
    {
        /// <summary>
        /// Writes exception error a log.txt file in the same folder as the application
        /// </summary>
        /// <param name="ex">Exception</param>
        public static void OnExceptionThrow (Exception ex)
        {
            string path = @"log.txt";

            try
            {
                if (!File.Exists(path))
                {
                    File.CreateText(path);
                }

                using (StreamWriter writer = new StreamWriter(path))
                {
                    writer.WriteLine($"Current Time: {DateTime.Now.ToString("en-GB")}");
                    writer.WriteLine($"Source: {ex.Source}");
                    writer.WriteLine($"ExMessage: {ex.Message}");
                    writer.WriteLine($"StackTrace: {ex.StackTrace}");
                    writer.WriteLine($"------------------------------");
                }

                return;
            }
            catch (Exception)
            {
                return;
            }
        }
    }
}
