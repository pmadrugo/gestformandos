﻿namespace Database.Common
{
    /// <summary>
    /// Interface to what functions must a table controller possess
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ITable<T>
    {
        int SaveToTextFile();
        int LoadFromTextFile();

        int Add(T newEntry);
        int Update(T originalEntry, T replaceWith);
        int Delete(T deleteEntry);

        uint GetNewID();

        bool HasRecordWithID(uint ID);
    }
}
