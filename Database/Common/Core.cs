﻿namespace Database.Common
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Security.Permissions;

    public static class Core<T>
    {
        public static string WorkingDirectory { get { return @".\Data\"; } }

        /// <summary>
        /// Writes the indicated table data to a text file with the indicated table name.
        /// </summary>
        /// <param name="tableName">Table name</param>
        /// <param name="data">List of strings containing table data</param>
        /// <returns>
        /// 0 if successful
        /// -1 if exception error
        /// </returns>
        public static int WriteToText(string tableName, List<string> data)
        {
            try
            {
                (new FileInfo(WorkingDirectory)).Directory.Create();

                string filePath = WorkingDirectory + tableName;
                string filePathBackup = filePath + ".bak";

                if (File.Exists(filePathBackup))
                {
                    File.Delete(filePathBackup);
                }

                using (FileStream currentFile = new FileStream(filePath, FileMode.OpenOrCreate))
                {
                    using (FileStream destinationFile = new FileStream(filePathBackup, FileMode.Create))
                    {
                        currentFile.CopyTo(destinationFile);

                        destinationFile.Close();
                    }
                    currentFile.Close();
                }

                using (StreamWriter sw = new StreamWriter(filePath, false))
                {
                    foreach (string textLine in data)
                    {
                        sw.WriteLine(textLine);
                    }
                    sw.Close();
                }
                return 0;
            }
            catch (Exception ex)
            {
                ErrorHandling.OnExceptionThrow(ex);
                return -1;
            }
        }

        /// <summary>
        /// Gets new ID from the indicated list/table
        /// </summary>
        /// <param name="DataList">Table/list to search on</param>
        /// <returns>Highest ID + 1 (uint)</returns>
        public static uint GetNewID(List<T> DataList)
        {
            List<T> dataList = DataList;

            uint highestID = 0;
            if (dataList != null)
            {
                foreach (IData items in dataList)
                {
                    if (items.ID > highestID)
                    {
                        highestID = items.ID;
                    }
                }
            }
            return highestID + 1;
        }

        /// <summary>
        /// Checks the indicated table/list if a value with the indicated ID is already inserted
        /// </summary>
        /// <param name="DataList">Table/list to search on</param>
        /// <param name="ID">ID to look for</param>
        /// <returns>
        /// True if it exists
        /// False if it does not
        /// </returns>
        public static bool HasRecordWithID(List<T> DataList, uint ID)
        {
            List<T> dataList = DataList;

            if (dataList != null)
            {
                foreach (IData item in dataList)
                {
                    if (item.ID == ID)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Copies values from one object to another as long as they're the same type
        /// </summary>
        /// <param name="source">Source object</param>
        /// <param name="destination">Destination object</param>
        public static void CopyValues(object source, object destination)
        {
            if (source.GetType() != destination.GetType())
            {
                return;
            }

            Type dataType = source.GetType();

            foreach (PropertyInfo property in dataType.GetProperties())
            {
                if (property.SetMethod != null)
                {
                    dataType.GetProperty(property.Name).SetValue(destination, property.GetValue(source));
                }
            }
        }
    }
}
