﻿namespace Database.Common
{
    /// <summary>
    /// Basic interface to simulate a table-like construct
    /// </summary>
    public interface IData
    {
        uint ID { get; set; }

        string BaseInfo { get; }
    }
}
