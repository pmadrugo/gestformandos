﻿namespace Database.Common
{
    public enum Gender
    {
        NA,
        MALE,
        FEMALE
    }

    public enum IDType
    {
        BILHETE_IDENTIDADE,
        CARTAO_CIDADAO,
        MILITAR,
        PASSAPORTE,
        TITULO_RESIDENCIA,
        OUTRO
    }
}
