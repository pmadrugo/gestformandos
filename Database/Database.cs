﻿using System.Collections.Generic;
using Database.TableControllers;
using Database.TableModels;

namespace Database
{
    public class Database
    {
        public static TClass Classes           { get; set; }
        public static TCourse Courses          { get; set; }
        public static TProfessor Professors    { get; set; }
        public static TUFCD UFCDs              { get; set; }
        public static TStudent Students        { get; set; }
        public static TEvaluation Evaluations  { get; set; }
        public static TProfessor_Class Professor_Class { get; set; }

        static Database()
        {
            Classes        = new TClass()
            {
                ClassList = new List<Class>()
            };

            Courses        = new TCourse()
            {
                CourseList = new List<Course>()
            };

            Professors     = new TProfessor()
            {
                ProfessorList = new List<Professor>()
            };

            UFCDs          = new TUFCD()
            {
                UFCDList = new List<UFCD>()
            };

            Students       = new TStudent()
            {
                StudentList = new List<Student>()
            };

            Evaluations    = new TEvaluation()
            {
                EvaluationList = new List<Evaluation>()
            };

            Professor_Class = new TProfessor_Class()
            {
                Professor_ClassList = new List<Professor_Class>()
            };
        }

        public void SaveAll()
        {
            Database.Classes.SaveToTextFile();
            Database.Courses.SaveToTextFile();
            Database.Professors.SaveToTextFile();
            Database.UFCDs.SaveToTextFile();
            Database.Students.SaveToTextFile();
            Database.Evaluations.SaveToTextFile();
            Database.Professor_Class.SaveToTextFile();
        }

        public void LoadAll()
        {
            Database.Evaluations.LoadFromTextFile();
            Database.Students.LoadFromTextFile();
            Database.Classes.LoadFromTextFile();
            Database.UFCDs.LoadFromTextFile();
            Database.Courses.LoadFromTextFile();
            Database.Professors.LoadFromTextFile();
            Database.Professor_Class.LoadFromTextFile();
        }

        /* Skeleton to what might be needed
        public int Delete_Integrity(object entryDelete)
        {
            switch (entryDelete)
            {
                case UFCD ufcd:
                {
                    
                } break;

                case Student student:
                {

                } break;

                case Professor professor:
                {

                } break;

                case Evaluation evaluation:
                {

                } break;

                case Course course:
                {

                } break;

                case Class team:
                {

                } break;
            }
        }*/
    }
}
