﻿namespace Database.TableControllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Common;
    using TableModels;

    public class TUFCD : ITable<UFCD>
    {
        public List<UFCD> UFCDList { get; set; }

        public TUFCD()
        {
            this.UFCDList = new List<UFCD>();
        }

        /// <summary>
        /// Adds a new UFCD entry
        /// </summary>
        /// <param name="newEntry">New UFCD entry</param>
        /// <returns>
        /// 0 if sucessful
        /// -1 if there's already an item with the same ID
        /// </returns>
        public int Add(UFCD newEntry)
        {
            if (HasRecordWithID(newEntry.ID))
            {
                return -1; // Já existe algo com esse ID
            }

            UFCDList.Add(newEntry);

            Database.UFCDs.SaveToTextFile();
            Database.Professors.SaveToTextFile();
            Database.Courses.SaveToTextFile();

            return 0; // Sucesso
        }

        /// <summary>
        /// Updates an existing entry with new values
        /// </summary>
        /// <param name="originalEntry">Entry to receive new values</param>
        /// <param name="replaceWith">Entry with new values</param>
        /// <returns>
        /// 0 if succesful
        /// -1 if new ID conflicts with already existing ones
        /// </returns>
        public int Update(UFCD originalEntry, UFCD replaceWith)
        {
            if (originalEntry.ID != replaceWith.ID)
            {
                if (HasRecordWithID(replaceWith.ID))
                {
                    return -1; // ID já se encontra em uso!
                }
            }

            Core<UFCD>.CopyValues(replaceWith, originalEntry);

            Database.UFCDs.SaveToTextFile();
            return 0;
        }

        /// <summary>
        /// Deletes an existing entry
        /// </summary>
        /// <param name="deleteEntry">Entry to remove</param>
        /// <returns>
        /// 0 if sucessful
        /// -1 entry not found
        /// -2 if entry has dependents
        /// </returns>
        public int Delete(UFCD deleteEntry)
        {
            if (deleteEntry.Evaluations.EvaluationList.Count > 0)
            {
                return -2; // Tem avaliações associados
            }

            UFCD entryToDelete = UFCDList.Find(c => c == deleteEntry);
            if (entryToDelete != null)
            {
                UFCDList.Remove(entryToDelete);

                Database.UFCDs.SaveToTextFile();
                Database.Professors.SaveToTextFile();
                Database.Courses.SaveToTextFile();

                return 0;
            }

            // Entrada não encontrada
            return -1;
        }

        /// <summary>
        /// Returns ID for a new entry
        /// </summary>
        /// <returns></returns>
        public uint GetNewID()
        {
            return Core<UFCD>.GetNewID(this.UFCDList);
        }

        /// <summary>
        /// Checks if there's an entry with the same ID
        /// </summary>
        /// <param name="ID">ID to check</param>
        /// <returns>
        /// True if it exists
        /// False if it does not
        /// </returns>
        public bool HasRecordWithID(uint ID)
        {
            return Core<UFCD>.HasRecordWithID(this.UFCDList, ID);
        }

        /// <summary>
        /// Saves table data to textFile
        /// </summary>
        /// <returns>
        /// 0 if succesful
        /// -1 if exception error
        /// </returns>
        public int SaveToTextFile()
        {
            List<string> dataToWrite = new List<string>();

            try
            {
                foreach (UFCD ufcd in UFCDList)
                {
                    dataToWrite.Add(
                        $"{ufcd.ID}|" +
                        $"{ufcd.Name}|" +
                        $"{ufcd.Duration}|" +
                        $"{ufcd.Credits}|" +
                        $"{ufcd.Description.Replace(Environment.NewLine, @"\r\n")}"
                    );
                }

                Core<UFCD>.WriteToText($"ufcds", dataToWrite);

                return 0;
            }
            catch (Exception ex)
            {
                ErrorHandling.OnExceptionThrow(ex);
                return -1;
            }
        }

        /// <summary>
        /// Loads all data from textfiles to Table
        /// </summary>
        /// <returns>
        /// 0 if sucessful
        /// -1 if exception
        /// </returns>
        public int LoadFromTextFile()
        {
            string path = Core<UFCD>.WorkingDirectory + @"ufcds";

            try
            {
                if (File.Exists(path))
                {
                    this.UFCDList.Clear();

                    using (StreamReader sr = new StreamReader(path))
                    {
                        while (!sr.EndOfStream)
                        {
                            string[] loadedEntry = sr.ReadLine().Split('|');

                            UFCD loadedUFCD = new UFCD()
                            {
                                ID = Convert.ToUInt32(loadedEntry[0]),
                                Name = loadedEntry[1],
                                Duration = Convert.ToDouble(loadedEntry[2]),
                                Credits = Convert.ToDouble(loadedEntry[3]),
                                Description = loadedEntry[4].Replace(@"\r\n", Environment.NewLine)
                            };

                            foreach (Evaluation evaluation in Database.Evaluations.EvaluationList.FindAll(evaluation => evaluation.IDUFCD == loadedUFCD.ID))
                            {
                                loadedUFCD.Evaluations.EvaluationList.Add(evaluation);
                            }

                            this.UFCDList.Add(loadedUFCD);
                        };

                        sr.Close();
                    }

                    return 0; // Success
                }

                return 1; // File doesn't exist
            }
            catch (Exception ex)
            {
                ErrorHandling.OnExceptionThrow(ex);
                return -1;
            }
        }
    }
}
