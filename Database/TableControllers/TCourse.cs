﻿namespace Database.TableControllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Common;
    using TableModels;

    public class TCourse : ITable<Course>
    {
        public List<Course> CourseList { get; set; }

        public TCourse()
        {
            this.CourseList = new List<Course>();
        }

        /// <summary>
        /// Adds a new Course entry
        /// </summary>
        /// <param name="newEntry">New course entry</param>
        /// <returns>
        /// 0 if sucessful
        /// -1 if there's already an item with the same ID
        /// </returns>
        public int Add(Course newEntry)
        {
            if (HasRecordWithID(newEntry.ID))
            {
                return -1; // Já existe algo com esse ID
            }

            CourseList.Add(newEntry);
            Database.Courses.SaveToTextFile();

            return 0; // Sucesso
        }

        /// <summary>
        /// Updates an existing entry with new values
        /// </summary>
        /// <param name="originalEntry">Entry to receive new values</param>
        /// <param name="replaceWith">Entry with new values</param>
        /// <returns>
        /// 0 if succesful
        /// -1 if new ID conflicts with already existing ones
        /// </returns>
        public int Update(Course originalEntry, Course replaceWith)
        {
            if (originalEntry.ID != replaceWith.ID)
            {
                if (HasRecordWithID(replaceWith.ID))
                {
                    return -1; // ID já se encontra em uso!
                }
            }

            Core<Course>.CopyValues(replaceWith, originalEntry);
            Database.Courses.SaveToTextFile();

            return 0;
        }

        /// <summary>
        /// Deletes an existing entry
        /// </summary>
        /// <param name="deleteEntry">Entry to remove</param>
        /// <returns>
        /// 0 if sucessful
        /// -1 entry not found
        /// -2 if entry has dependents
        /// </returns>
        public int Delete(Course deleteEntry)
        {
            if (deleteEntry.Classes.ClassList.Count > 0)
            {
                return -2; // Tem turmas associados
            }
            if (deleteEntry.UFCDs.UFCDList.Count > 0)
            {
                return -3; // Tem UFCDs associados
            }

            Course entryToDelete = CourseList.Find(c => c == deleteEntry);
            if (entryToDelete != null)
            {
                Database.Courses.CourseList.Remove(entryToDelete);
                Database.Courses.SaveToTextFile();

                return 0;
            }

            // Entrada não encontrada
            return -1;
        }

        /// <summary>
        /// Returns ID for a new entry
        /// </summary>
        /// <returns></returns>
        public uint GetNewID()
        {
            return Core<Course>.GetNewID(this.CourseList);
        }

        /// <summary>
        /// Checks if there's an entry with the same ID
        /// </summary>
        /// <param name="ID">ID to check</param>
        /// <returns>
        /// True if it exists
        /// False if it does not
        /// </returns>
        public bool HasRecordWithID(uint ID)
        {
            return Core<Course>.HasRecordWithID(this.CourseList, ID);
        }

        /// <summary>
        /// Saves table data to textFile
        /// </summary>
        /// <returns>
        /// 0 if succesful
        /// -1 if exception error
        /// </returns>
        public int SaveToTextFile()
        {
            List<string> dataToWrite = new List<string>();

            try
            {
                foreach (Course course in CourseList)
                {
                    string temp = $"{course.ID}|{course.Name}|{course.Description.Replace(Environment.NewLine, @"\r\n")}|";

                    foreach (UFCD ufcd in course.UFCDs.UFCDList)
                    {
                        temp += $"{ufcd.ID}§";
                    }
                    temp += $"|";

                    foreach (Class team in course.Classes.ClassList)
                    {
                        temp += $"{team.ID}§";
                    }
                    
                    dataToWrite.Add(temp);
                }

                Core<Course>.WriteToText($"courses", dataToWrite);

                return 0;
            }
            catch (Exception ex)
            {
                ErrorHandling.OnExceptionThrow(ex);
                return -1;
            }
        }

        /// <summary>
        /// Loads all data from textfiles to Table
        /// </summary>
        /// <returns>
        /// 0 if sucessful
        /// -1 if exception
        /// </returns>
        public int LoadFromTextFile()
        {
            string path = Core<Course>.WorkingDirectory + @"courses";

            try
            {
                if (File.Exists(path))
                {
                    this.CourseList.Clear();

                    using (StreamReader sr = new StreamReader(path))
                    {
                        while (!sr.EndOfStream)
                        {
                            string[] loadedEntry = sr.ReadLine().Split('|');

                            Course loadedCourse = new Course()
                            {
                                ID = Convert.ToUInt32(loadedEntry[0]),
                                Name = loadedEntry[1],
                                Description = loadedEntry[2].Replace(@"\r\n", Environment.NewLine)
                            };

                            if (loadedEntry[3].Length > 0)
                            {
                                foreach (string ID in loadedEntry[3].Remove(loadedEntry[3].Length - 1).Split('§'))
                                {
                                    uint ufcdID = Convert.ToUInt32(ID);

                                    loadedCourse.UFCDs.UFCDList.Add(Database.UFCDs.UFCDList.Find(ufcd => ufcd.ID == ufcdID));
                                }
                            }

                            if (loadedEntry[4].Length > 0)
                            {
                                foreach (string ID in loadedEntry[4].Remove(loadedEntry[4].Length - 1).Split('§'))
                                {
                                    uint teamID = Convert.ToUInt32(ID);

                                    loadedCourse.Classes.ClassList.Add(Database.Classes.ClassList.Find(team => team.ID == teamID));
                                }
                            }

                            this.CourseList.Add(loadedCourse);

                        };
                        sr.Close();
                    }

                    return 0; //Success
                }

                return 1; // File doesn't exist
            }
            catch (Exception ex)
            {
                ErrorHandling.OnExceptionThrow(ex);
                return -1;
            }
        }
    }
}
