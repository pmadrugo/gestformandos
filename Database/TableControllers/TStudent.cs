﻿using System;
using System.Collections.Generic;
using System.IO;
using Database.Common;
using Database.TableModels;

namespace Database.TableControllers
{
    public class TStudent : ITable<Student>
    {
        public List<Student> StudentList { get; set; }

        public TStudent()
        {
            this.StudentList = new List<Student>();
        }

        /// <summary>
        /// Adds a new Student entry
        /// </summary>
        /// <param name="newEntry">New student entry</param>
        /// <returns>
        /// 0 if sucessful
        /// -1 if there's already an item with the same ID
        /// </returns>
        public int Add(Student newEntry)
        {
            if (Database.Students.StudentList.Find(student => student.NIF == newEntry.NIF) != null)
            {
                return -1; // Já existe algo com esse NIF
            }
            if (Database.Students.StudentList.Find(student => student.ID == newEntry.ID) == null)
            {
                Database.Students.StudentList.Add(newEntry);
            }

            StudentList.Add(newEntry);

            Database.Classes.SaveToTextFile();
            Database.Students.SaveToTextFile();

            return 0; // Sucesso
        }

        /// <summary>
        /// Updates an existing entry with new values
        /// </summary>
        /// <param name="originalEntry">Entry to receive new values</param>
        /// <param name="replaceWith">Entry with new values</param>
        /// <returns>
        /// 0 if succesful
        /// -1 if new ID conflicts with already existing ones
        /// </returns>
        public int Update(Student originalEntry, Student replaceWith)
        {
            if (originalEntry.ID != replaceWith.ID)
            {
                if (HasRecordWithID(replaceWith.ID))
                {
                    return -1; // ID já se encontra em uso!
                }
            }

            Core<Student>.CopyValues(replaceWith, originalEntry);
            Database.Students.SaveToTextFile();

            return 0;
        }

        /// <summary>
        /// Deletes an existing entry
        /// </summary>
        /// <param name="deleteEntry">Entry to remove</param>
        /// <returns>
        /// 0 if sucessful
        /// -1 entry not found
        /// -2 if entry has dependents
        /// </returns>
        public int Delete(Student deleteEntry)
        {
            Student entryToDelete;
            if (this.StudentList != Database.Students.StudentList)
            {
                entryToDelete = StudentList.Find(c => c == deleteEntry);
                if (entryToDelete != null)
                {
                    StudentList.Remove(entryToDelete);
                }
                Database.Classes.SaveToTextFile();
                Database.Students.SaveToTextFile();

                return 0;
            }

            if (deleteEntry.Evaluations.EvaluationList.Count > 0)
            {
                return -2; // Tem avaliações associadas
            }

            entryToDelete = StudentList.Find(c => c == deleteEntry);
            if (entryToDelete != null)
            {
                StudentList.Remove(entryToDelete);
                
                if(Database.Classes.ClassList.FindAll(team => team.Students.StudentList.Contains(entryToDelete)).Count <= 0)
                {
                    if (Database.Students.StudentList.Find(student => student == deleteEntry) != null)
                    {
                        Database.Students.StudentList.Remove(deleteEntry);
                    }
                }

                Database.Classes.SaveToTextFile();
                Database.Students.SaveToTextFile();

                return 0;
            }

            // Entrada não encontrada
            return -1;
        }

        /// <summary>
        /// Returns ID for a new entry
        /// </summary>
        /// <returns></returns>
        public uint GetNewID()
        {
            return Core<Student>.GetNewID(this.StudentList);
        }

        /// <summary>
        /// Checks if there's an entry with the same ID
        /// </summary>
        /// <param name="ID">ID to check</param>
        /// <returns>
        /// True if it exists
        /// False if it does not
        /// </returns>
        public bool HasRecordWithID(uint ID)
        {
            return Core<Student>.HasRecordWithID(this.StudentList, ID);
        }

        /// <summary>
        /// Saves table data to textFile
        /// </summary>
        /// <returns>
        /// 0 if succesful
        /// -1 if exception error
        /// </returns>
        public int SaveToTextFile()
        {
            List<string> dataToWrite = new List<string>();

            try
            {
                foreach (Student student in StudentList)
                {
                    dataToWrite.Add( 
                        $"{student.ID}|" +
                        $"{student.Name}|" +
                        $"{Convert.ToUInt32(student.Gender)}|" +
                        $"{student.BirthDate}|" +
                        $"{Convert.ToUInt32(student.ID_Document_Type)}|" +
                        $"{student.ID_Document_Number}|" +
                        $"{student.ID_Document_EmissionDate}|" +
                        $"{student.ID_Document_ExpirationDate}|" +
                        $"{student.NIF}|" +
                        $"{student.Nationality}|" +
                        $"{student.Citizenship}|" +
                        $"{student.Birthplace}"
                    );
                }

                Core<Student>.WriteToText($"students", dataToWrite);

                return 0;
            }
            catch (Exception ex)
            {
                ErrorHandling.OnExceptionThrow(ex);
                return -1;
            }
        }

        /// <summary>
        /// Loads all data from textfiles to Table
        /// </summary>
        /// <returns>
        /// 0 if sucessful
        /// -1 if exception
        /// </returns>
        public int LoadFromTextFile()
        {
            string path = Core<Student>.WorkingDirectory + @"students";

            try
            {
                if (File.Exists(path))
                {
                    this.StudentList.Clear();

                    using (StreamReader sr = new StreamReader(path))
                    {
                        while (!sr.EndOfStream)
                        {
                            string[] loadedEntry = sr.ReadLine().Split('|');

                            Student loadedStudent = new Student()
                            {
                                ID = Convert.ToUInt32(loadedEntry[0]),
                                Name = loadedEntry[1],
                                Gender = (Gender)(Convert.ToUInt32(loadedEntry[2])),
                                BirthDate = Convert.ToDateTime(loadedEntry[3]),
                                ID_Document_Type = (IDType)(Convert.ToUInt32(loadedEntry[4])),
                                ID_Document_Number = loadedEntry[5],
                                ID_Document_EmissionDate = Convert.ToDateTime(loadedEntry[6]),
                                ID_Document_ExpirationDate = Convert.ToDateTime(loadedEntry[7]),
                                NIF = Convert.ToUInt32(loadedEntry[8]),
                                Nationality = loadedEntry[9],
                                Citizenship = loadedEntry[10],
                                Birthplace = loadedEntry[11]
                            };

                            foreach (Evaluation evaluation in Database.Evaluations.EvaluationList.FindAll(evaluation => evaluation.IDStudent == loadedStudent.ID))
                            {
                                loadedStudent.Evaluations.EvaluationList.Add(evaluation);
                            }

                            this.StudentList.Add(loadedStudent);
                        };
                        sr.Close();
                    }

                    return 0; // Success
                }

                return 1; // File doesn't exist
            }
            catch (Exception ex)
            {
                ErrorHandling.OnExceptionThrow(ex);
                return -1;
            }
        }
    }
}
