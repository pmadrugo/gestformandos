﻿namespace Database.TableControllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Common;
    using TableModels;

    public class TProfessor : ITable<Professor>
    {
        public List<Professor> ProfessorList  { get; set; }

        public TProfessor()
        {
            this.ProfessorList = new List<Professor>();
        }

        /// <summary>
        /// Adds a new Professor entry
        /// </summary>
        /// <param name="newEntry">New professor entry</param>
        /// <returns>
        /// 0 if sucessful
        /// -1 if there's already an item with the same ID
        /// </returns>
        public int Add(Professor newEntry)
        {
            if (Database.Professors.ProfessorList.Find(professor => professor.NIF == newEntry.NIF) != null)
            {
                return -1; // Já existe algo com esse ID
            }

            ProfessorList.Add(newEntry);
            Database.Professors.SaveToTextFile();

            return 0; // Sucesso
        }

        /// <summary>
        /// Updates an existing entry with new values
        /// </summary>
        /// <param name="originalEntry">Entry to receive new values</param>
        /// <param name="replaceWith">Entry with new values</param>
        /// <returns>
        /// 0 if succesful
        /// -1 if new ID conflicts with already existing ones
        /// </returns>
        public int Update(Professor originalEntry, Professor replaceWith)
        {
            if (originalEntry.ID != replaceWith.ID)
            {
                if (HasRecordWithID(replaceWith.ID))
                {
                    return -1; // ID já se encontra em uso!
                }
            }

            Core<Professor>.CopyValues(replaceWith, originalEntry);
            Database.Professors.SaveToTextFile();

            return 0;
        }

        /// <summary>
        /// Deletes an existing entry
        /// </summary>
        /// <param name="deleteEntry">Entry to remove</param>
        /// <returns>
        /// 0 if sucessful
        /// -1 entry not found
        /// -2 if entry has dependents
        /// </returns>
        public int Delete(Professor deleteEntry)
        {
            if (deleteEntry.UFCDs.UFCDList.Count > 0)
            {
                return -2; // Tem UFCDs associadas
            }
            if (Database.Professor_Class.Professor_ClassList.Find(pclass => pclass.Professor == deleteEntry) != null)
            {
                return -3; // Tem turmas associadas
            }

            Professor entryToDelete = ProfessorList.Find(c => c == deleteEntry);
            if (entryToDelete != null)
            {
                List<Professor_Class> pClassToDelete = Database.Professor_Class.Professor_ClassList.FindAll(pclass => pclass.Professor == entryToDelete);

                foreach (Professor_Class pClass in pClassToDelete)
                {
                    Database.Professor_Class.Professor_ClassList.Remove(pClass);
                }

                ProfessorList.Remove(entryToDelete);
                Database.Professors.SaveToTextFile();

                return 0;
            }

            // Entrada não encontrada
            return -1;
        }

        /// <summary>
        /// Returns ID for a new entry
        /// </summary>
        /// <returns></returns>
        public uint GetNewID()
        {
            return Core<Professor>.GetNewID(this.ProfessorList);
        }

        /// <summary>
        /// Checks if there's an entry with the same ID
        /// </summary>
        /// <param name="ID">ID to check</param>
        /// <returns>
        /// True if it exists
        /// False if it does not
        /// </returns>
        public bool HasRecordWithID(uint ID)
        {
            return Core<Professor>.HasRecordWithID(this.ProfessorList, ID);
        }

        /// <summary>
        /// Saves table data to textFile
        /// </summary>
        /// <returns>
        /// 0 if succesful
        /// -1 if exception error
        /// </returns>
        public int SaveToTextFile()
        {
            List<string> dataToWrite = new List<string>();

            try
            {
                foreach (Professor professor in ProfessorList)
                {
                    string temp =   $"{professor.ID}|" +
                                    $"{professor.Name}|" +
                                    $"{Convert.ToInt32(professor.Gender)}|" +
                                    $"{professor.BirthDate}|" +
                                    $"{Convert.ToInt32(professor.ID_Document_Type)}|" +
                                    $"{professor.ID_Document_Number}|" +
                                    $"{professor.ID_Document_EmissionDate}|" +
                                    $"{professor.ID_Document_ExpirationDate}|" +
                                    $"{professor.NIF}|" +
                                    $"{professor.Nationality}|" +
                                    $"{professor.Citizenship}|" +
                                    $"{professor.Birthplace}|";

                    foreach (UFCD ufcd in professor.UFCDs.UFCDList)
                    {
                        temp += $"{ufcd.ID}§";
                    }

                    dataToWrite.Add(temp);
                }

                Core<Professor>.WriteToText($"professors", dataToWrite);

                return 0;
            }
            catch (Exception ex)
            {
                ErrorHandling.OnExceptionThrow(ex);
                return -1;
            }
        }

        /// <summary>
        /// Loads all data from textfiles to Table
        /// </summary>
        /// <returns>
        /// 0 if sucessful
        /// -1 if exception
        /// </returns>
        public int LoadFromTextFile()
        {
            string path = Core<Professor>.WorkingDirectory + @"professors";

            try
            {
                if (File.Exists(path))
                {
                    this.ProfessorList.Clear();

                    using (StreamReader sr = new StreamReader(path))
                    {
                        while (!sr.EndOfStream)
                        {
                            string[] loadedEntry = sr.ReadLine().Split('|');

                            Professor loadedProfessor = new Professor()
                            {
                                ID = Convert.ToUInt32(loadedEntry[0]),
                                Name = loadedEntry[1],
                                Gender = (Gender)(Convert.ToUInt32(loadedEntry[2])),
                                BirthDate = Convert.ToDateTime(loadedEntry[3]),
                                ID_Document_Type = (IDType)(Convert.ToUInt32(loadedEntry[4])),
                                ID_Document_Number = loadedEntry[5],
                                ID_Document_EmissionDate = Convert.ToDateTime(loadedEntry[6]),
                                ID_Document_ExpirationDate = Convert.ToDateTime(loadedEntry[7]),
                                NIF = Convert.ToUInt32(loadedEntry[8]),
                                Nationality = loadedEntry[9],
                                Citizenship = loadedEntry[10],
                                Birthplace = loadedEntry[11]
                            };

                            if (loadedEntry[12].Length > 0)
                            {
                                foreach (string ID in loadedEntry[12].Remove(loadedEntry[12].Length - 1).Split('§'))
                                {
                                    uint ufcdID = Convert.ToUInt32(ID);

                                    loadedProfessor.UFCDs.UFCDList.Add(Database.UFCDs.UFCDList.Find(ufcd => ufcd.ID == ufcdID));
                                }
                            }

                            this.ProfessorList.Add(loadedProfessor);
                        };
                        
                        sr.Close();
                    }

                    return 0; // Success
                }

                return 1; // File doesn't exist
            }
            catch (Exception ex)
            {
                ErrorHandling.OnExceptionThrow(ex);
                return -1;
            }
        }
    }
}
