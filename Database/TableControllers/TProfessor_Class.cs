﻿namespace Database.TableControllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Common;
    using TableModels;

    public class TProfessor_Class : ITable<Professor_Class>
    {
        public List<Professor_Class> Professor_ClassList { get; set; }

        public TProfessor_Class()
        {
            this.Professor_ClassList = new List<Professor_Class>();
        }

        /// <summary>
        /// Adds a new Professor_Class association entry
        /// </summary>
        /// <param name="newEntry">New Professor_Class association entry</param>
        /// <returns>
        /// 0 if sucessful
        /// -1 if there's already an item with the same ID
        /// </returns>
        public int Add(Professor_Class newEntry)
        {
            if (Database.Professor_Class.HasRecordWithID(newEntry.ID))
            {
                return -1; // Já existe algo com esse ID adicionado
            }

            Database.Professor_Class.Professor_ClassList.Add(newEntry);

            Database.Professor_Class.SaveToTextFile();

            return 0; // Success
        }

        /// <summary>
        /// Updates an existing entry with new values
        /// </summary>
        /// <param name="originalEntry">Entry to receive new values</param>
        /// <param name="replaceWith">Entry with new values</param>
        /// <returns>
        /// 0 if succesful
        /// -1 if new ID conflicts with already existing ones
        /// </returns>
        public int Update(Professor_Class originalEntry, Professor_Class replaceWith)
        {
            if (originalEntry.ID != replaceWith.ID)
            {
                if (HasRecordWithID(replaceWith.ID))
                {
                    return -1; // ID já se encontra em uso!
                }
            }

            Core<Professor_Class>.CopyValues(replaceWith, originalEntry);
            Database.Professor_Class.SaveToTextFile();

            return 0;
        }

        /// <summary>
        /// Deletes an existing entry
        /// </summary>
        /// <param name="deleteEntry">Entry to remove</param>
        /// <returns>
        /// 0 if sucessful
        /// -1 entry not found
        /// </returns>
        public int Delete(Professor_Class deleteEntry)
        {
            Professor_Class entryToDelete = Professor_ClassList.Find(pclass => pclass == deleteEntry);
            if (entryToDelete != null)
            {
                Professor_ClassList.Remove(entryToDelete);

                Database.Professor_Class.SaveToTextFile();

                return 0;
            }
            // Entrada não encontrada
            return -1;
        }

        /// <summary>
        /// Returns ID for a new entry
        /// </summary>
        /// <returns></returns>
        public uint GetNewID()
        {
            return Core<Professor_Class>.GetNewID(this.Professor_ClassList);
        }

        /// <summary>
        /// Checks if there's an entry with the same ID
        /// </summary>
        /// <param name="ID">ID to check</param>
        /// <returns>
        /// True if it exists
        /// False if it does not
        /// </returns>
        public bool HasRecordWithID(uint ID)
        {
            return Core<Professor_Class>.HasRecordWithID(this.Professor_ClassList, ID);
        }

        /// <summary>
        /// Saves table data to textFile
        /// </summary>
        /// <returns>
        /// 0 if succesful
        /// -1 if exception error
        /// </returns>
        public int SaveToTextFile()
        {
            List<string> dataToWrite = new List<string>();

            try
            {
                foreach (Professor_Class pclass in Professor_ClassList)
                {
                    dataToWrite.Add($"{pclass.ID}|{pclass.Professor.ID}|{pclass.Team.ID}|{pclass.UFCD.ID}");
                }

                Core<Professor_Class>.WriteToText($"professor_class", dataToWrite);

                return 0;
            }
            catch (Exception ex)
            {
                ErrorHandling.OnExceptionThrow(ex);
                return -1;
            }
        }

        /// <summary>
        /// Loads all data from textfiles to Table
        /// </summary>
        /// <returns>
        /// 0 if sucessful
        /// -1 if exception
        /// </returns>
        public int LoadFromTextFile()
        {
            string path = Core<Professor_Class>.WorkingDirectory + @"professor_class";

            try
            {
                if (File.Exists(path))
                {
                    this.Professor_ClassList.Clear();

                    using (StreamReader sr = new StreamReader(path))
                    {
                        while (!sr.EndOfStream)
                        {
                            string[] loadedEntry = sr.ReadLine().Split('|');

                            Professor_Class loadedPClass = new Professor_Class()
                            {
                                ID = Convert.ToUInt32(loadedEntry[0])
                            };

                            uint professorID = Convert.ToUInt32(loadedEntry[1]);
                            loadedPClass.Professor = Database.Professors.ProfessorList.Find(professor => professor.ID == professorID);

                            uint teamID = Convert.ToUInt32(loadedEntry[2]);
                            loadedPClass.Team = Database.Classes.ClassList.Find(team => team.ID == teamID);

                            uint ufcdID = Convert.ToUInt32(loadedEntry[3]);
                            loadedPClass.UFCD = Database.UFCDs.UFCDList.Find(ufcd => ufcd.ID == ufcdID);

                            Database.Professor_Class.Professor_ClassList.Add(loadedPClass);

                        };
                        sr.Close();
                    }

                    return 0; // Success
                }
                return 1; // File doesn't exist
            }
            catch (Exception ex)
            {
                ErrorHandling.OnExceptionThrow(ex);
                return -1;
            }
        }

        /// <summary>
        /// Get assigned professor towards a team regarding certain UFCD
        /// </summary>
        /// <param name="Team">Class</param>
        /// <param name="ufcd">UFCD</param>
        /// <returns>Professor</returns>
        public Professor_Class GetAssignedProfessorClass (Class Team, UFCD ufcd)
        {
            return this.Professor_ClassList.Find(pclass => ((pclass.Team == Team) && (pclass.UFCD == ufcd)));
        }

        /// <summary>
        /// Get all professors that can teach this UFCD
        /// </summary>
        /// <param name="ufcd">UFCD</param>
        /// <returns>List of Professors</returns>
        public List<Professor> GetAvailableProfessors (UFCD ufcd)
        {
            List<Professor> availableProfessors = new List<Professor>();
            foreach (Professor professor in Database.Professors.ProfessorList)
            {
                if (professor.UFCDs.UFCDList.Contains(ufcd))
                {
                    availableProfessors.Add(professor);
                }
            }
            return availableProfessors;
        }
    }
}
