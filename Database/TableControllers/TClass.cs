﻿namespace Database.TableControllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Common;
    using TableModels;

    public class TClass : ITable<Class>
    {
        public List<Class> ClassList { get; set; }

        public TClass()
        {
            this.ClassList = new List<Class>();
        }

        /// <summary>
        /// Adds a new Class entry
        /// </summary>
        /// <param name="newEntry">New class entry</param>
        /// <returns>
        /// 0 if sucessful
        /// -1 if there's already an item with the same ID
        /// </returns>
        public int Add(Class newEntry)
        {
            if (Database.Classes.HasRecordWithID(newEntry.ID))
            {
                return -1; // Já existe algo com esse ID adicionado
            }
            if (Database.Classes.ClassList.Find(team => team.ID == newEntry.ID) == null)
            {
                Database.Classes.ClassList.Add(newEntry);
            }

            ClassList.Add(newEntry);
            Database.Classes.SaveToTextFile();
            Database.Courses.SaveToTextFile();

            return 0; // Sucesso
        }

        /// <summary>
        /// Updates an existing entry with new values
        /// </summary>
        /// <param name="originalEntry">Entry to receive new values</param>
        /// <param name="replaceWith">Entry with new values</param>
        /// <returns>
        /// 0 if succesful
        /// -1 if new ID conflicts with already existing ones
        /// </returns>
        public int Update(Class originalEntry, Class replaceWith)
        {
            if (originalEntry.ID != replaceWith.ID)
            {
                if (HasRecordWithID(replaceWith.ID))
                {
                    return -1; // ID já se encontra em uso!
                }
            }

            Core<Class>.CopyValues(replaceWith, originalEntry);
            Database.Classes.SaveToTextFile();

            return 0;
        }

        /// <summary>
        /// Deletes an existing entry
        /// </summary>
        /// <param name="deleteEntry">Entry to remove</param>
        /// <returns>
        /// 0 if sucessful
        /// -1 entry not found
        /// -2 if entry has dependents
        /// </returns>
        public int Delete(Class deleteEntry)
        {
            if (deleteEntry.Students.StudentList.Count > 0)
            {
                return -2; // Tem estudantes associadas
            }

            Class entryToDelete = ClassList.Find(c => c == deleteEntry);
            if (entryToDelete != null)
            {
                ClassList.Remove(entryToDelete);

                if (Database.Courses.CourseList.FindAll(course => course.Classes.ClassList.Contains(entryToDelete)).Count <= 0)
                {
                    if (Database.Classes.ClassList.Find(team => team == deleteEntry) != null)
                    {
                        Database.Classes.ClassList.Remove(deleteEntry);
                    }
                }

                Database.Classes.SaveToTextFile();
                Database.Courses.SaveToTextFile();

                return 0;
            }

            // Entrada não encontrada
            return -1;
        }

        /// <summary>
        /// Returns ID for a new entry
        /// </summary>
        /// <returns></returns>
        public uint GetNewID()
        {
            return Core<Class>.GetNewID(this.ClassList);
        }

        /// <summary>
        /// Checks if there's an entry with the same ID
        /// </summary>
        /// <param name="ID">ID to check</param>
        /// <returns>
        /// True if it exists
        /// False if it does not
        /// </returns>
        public bool HasRecordWithID(uint ID)
        {
            return Core<Class>.HasRecordWithID(this.ClassList, ID);
        }

        /// <summary>
        /// Saves table data to textFile
        /// </summary>
        /// <returns>
        /// 0 if succesful
        /// -1 if exception error
        /// </returns>
        public int SaveToTextFile()
        {
            List<string> dataToWrite = new List<string>();

            try
            {
                foreach (Class team in ClassList)
                {
                    string temp = $"{team.ID}|{team.Name}|{team.StartDate}|{team.EndDate}|";

                    foreach (Student student in team.Students.StudentList)
                    {
                        temp += $"{student.ID}§";
                    }

                    dataToWrite.Add(temp);
                }

                Core<Class>.WriteToText($"classes", dataToWrite);

                return 0;
            }
            catch (Exception ex)
            {
                ErrorHandling.OnExceptionThrow(ex);
                return -1;
            }
        }

        /// <summary>
        /// Returns classes that are not yet associated with a course
        /// </summary>
        /// <returns>
        /// List<Class> of items that are not yet associated, null if it possesses none
        /// </returns>
        public List<Class> GetAvailableClassList()
        {
            List<Class> availableTeams = new List<Class>();

            foreach (Class team in Database.Classes.ClassList)
            {
                bool available = true;
                foreach (Course course in Database.Courses.CourseList)
                {
                    if (course.Classes.ClassList.Contains(team))
                    {
                        available = false;
                        break;
                    }
                }
                if (available)
                {
                    availableTeams.Add(team);
                }
            }
            return availableTeams;
        }

        /// <summary>
        /// Loads all data from textfiles to Table
        /// </summary>
        /// <returns>
        /// 0 if sucessful
        /// -1 if exception
        /// </returns>
        public int LoadFromTextFile()
        {
            string path = Core<Class>.WorkingDirectory + @"classes";

            try
            {
                if (File.Exists(path))
                {
                    this.ClassList.Clear();

                    using (StreamReader sr = new StreamReader(path))
                    {
                        while (!sr.EndOfStream)
                        {
                            string[] loadedEntry = sr.ReadLine().Split('|');

                            Class loadedTeam = new Class()
                            {
                                ID = Convert.ToUInt32(loadedEntry[0]),
                                Name = loadedEntry[1],
                                StartDate = Convert.ToDateTime(loadedEntry[2]),
                                EndDate = Convert.ToDateTime(loadedEntry[3])
                            };

                            if (loadedEntry[4].Length > 0)
                            {
                                foreach (string ID in loadedEntry[4].Remove(loadedEntry[4].Length - 1).Split('§'))
                                {
                                    uint studentID = Convert.ToUInt32(ID);

                                    loadedTeam.Students.StudentList.Add(Database.Students.StudentList.Find(student => student.ID == studentID));
                                }
                            }

                            this.ClassList.Add(loadedTeam);
                        };
                        sr.Close();
                    }

                    return 0; // Success
                }

                return 1; // File doesn't exist
            }
            catch (Exception ex)
            {
                ErrorHandling.OnExceptionThrow(ex);
                return -1;
            }
        }
    }
}
