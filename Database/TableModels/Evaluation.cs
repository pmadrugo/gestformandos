﻿namespace Database.TableModels
{
    using Common;

    public class Evaluation : IData
    {
        #region Attributes
        private double _Grade;
        private uint _IDStudent;
        private uint _IDUFCD;
        private uint _ID;
        #endregion

        #region Properties
        public double Grade
        {
            get { return _Grade; }
            set {
                if (value >= 0 && value <= 20)
                {
                    _Grade = value;
                }
            }
        }
        public uint IDStudent
        {
            get { return _IDStudent; }
            set
            {
                if (value > 0)
                {
                    _IDStudent = value;
                }
            }
        }
        public uint IDUFCD
        {
            get { return _IDUFCD; }
            set
            {
                if (value > 0)
                {
                    _IDUFCD = value;
                }
            }
        }
        public uint ID
        {
            get { return _ID; }
            set
            {
                if (value > 0)
                {
                    _ID = value;
                }
            }
        }
        public string Description { get; set; }

        public string BaseInfo { get { return $"";  } }

        #endregion
    }
}
