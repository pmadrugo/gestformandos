﻿namespace Database.TableModels
{
    using Common;
    using TableControllers;

    public class Course : IData
    {
        #region Attributes
        private uint _ID;
        #endregion

        #region Properties
        public uint ID
        {
            get { return _ID; }
            set
            {
                if (value >= 1)
                {
                    _ID = value;
                }
            }
        }

        public string Name { get; set; }
        public string BaseInfo { get { return $"{ID} - {Name}"; } }
        public string Description { get; set; }

        public TClass Classes { get; set; }
        public TUFCD UFCDs { get; set; }

        public Course()
        {
            this.Classes = new TClass();
            this.UFCDs = new TUFCD();
        }

        public override string ToString()
        {
            return BaseInfo;
        }
        #endregion

    }
}
