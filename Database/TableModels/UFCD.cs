﻿namespace Database.TableModels
{
    using System.Collections.Generic;
    using Common;
    using TableControllers;

    public class UFCD : IData
    {
        #region Attributes
        private uint _ID;
        #endregion

        #region Properties
        public string BaseInfo { get { return $"{ID} - {Name}"; } }

        public uint ID
        {
            get { return _ID; }
            set
            {
                if (value >= 1)
                {
                    _ID = value;
                }
            }
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public double Duration { get; set; }
        public double Credits { get; set; }

        public TEvaluation Evaluations { get; set; }

        #endregion

        public UFCD()
        {
            this.Evaluations = new TEvaluation();
        }

        public override string ToString()
        {
            return BaseInfo;
        }
    }
}
