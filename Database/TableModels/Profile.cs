﻿namespace Database.TableModels
{
    using System;
    using Common;

    public class Profile : IData
    {
        #region Attributes
        private uint _ID;
        #endregion

        #region Properties
        public uint ID
        {
            get { return _ID; }
            set
            {
                if (value >= 1)
                {
                    _ID = value;
                }
            }
        }
        public string Name { get; set; }
        public string BaseInfo { get { return $"{ID} - {Name}"; } }
        public Gender Gender { get; set; }
        public DateTime BirthDate { get; set; }

        // Documento de Identificação
        public IDType ID_Document_Type { get; set; }
        public string ID_Document_Number { get; set; }
        public DateTime ID_Document_EmissionDate { get; set; }
        public DateTime ID_Document_ExpirationDate { get; set; }

        public uint NIF { get; set; }
        public string Nationality { get; set; }
        public string Citizenship { get; set; }
        public string Birthplace { get; set; }
        #endregion
    }
}
