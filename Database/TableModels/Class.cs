﻿namespace Database.TableModels
{
    using System;
    using Common;
    using TableControllers;

    public class Class : IData
    {
        #region Attributes
        private uint _ID;
        #endregion

        #region Properties
        public string BaseInfo { get { return $"{ID} - {Name}"; } }

        public uint ID
        {
            get { return _ID; }
            set
            {
                if (value >= 1)
                {
                    _ID = value;
                }
            }
        }

        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public TStudent Students { get; set; }

        public Class()
        {
            this.Students = new TStudent();
        }

        public override string ToString()
        {
            return BaseInfo;
        }
        #endregion
    }
}
