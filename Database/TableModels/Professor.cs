﻿namespace Database.TableModels
{
    using TableControllers;

    public class Professor : Profile
    {
        public TUFCD UFCDs { get; set; }

        public Professor()
        {
            this.UFCDs = new TUFCD();
        }

        public override string ToString()
        {
            return BaseInfo;
        }
    }
}
