﻿namespace Database.TableModels
{
    using Common;

    public class Professor_Class : IData
    {
        public uint ID { get; set; }
        public Professor Professor { get; set; }
        public Class Team { get; set; }
        public UFCD UFCD { get; set; }

        public string BaseInfo { get { return $""; } }
    }
}
