﻿namespace Database.TableModels
{
    using System.Collections.Generic;
    using TableControllers;

    public class Student : Profile
    {
        public TEvaluation Evaluations { get; set; }

        public Student()
        {
            this.Evaluations = new TEvaluation();
        }

        public override string ToString()
        {
            return BaseInfo;
        }
    }
}
